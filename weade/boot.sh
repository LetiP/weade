#!/bin/bash
source venv/bin/activate
flask --app weade db upgrade
exec gunicorn -b :5000 --access-logfile - --error-logfile - weade:app