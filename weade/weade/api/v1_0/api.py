""" Creates routes for a RESTful API for the WEADE resources and services """

from flask import Blueprint, jsonify, abort, request, make_response
import weade.prep_associations as preps
import weade.functions as fct
import pandas as pd
import numpy as np


api = Blueprint('api', __name__)


@api.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@api.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'Bad Request'}), 400)


@api.route('/categorySets', methods=['GET'])
def categorySets():
    """ Returns list of available category sets. """
    cat_sets = preps.list_available_categories()
    return jsonify(cat_sets)


@api.route('/categorySets/<string:set_name>', methods=['GET'])
def getSet(set_name):
    """ Returns json export of selected category set. """
    _, cat_set = preps.retrieve_categories(set_name)
    return cat_set.to_json()


@api.route('/species', methods=['GET'])
def species():
    """ Returns list of available species. """
    specs = preps.list_available_species()
    return jsonify(specs)


@api.route('/run/<string:atype>', methods=['POST'])
def run(atype):
    """ Runs analysis on categories with provided data. """
    # checks for correct request format and information
    if atype not in ['fisher', 'mwu']:
        abort(404)
    if not request.json:
        abort(400)
    if not 'categorySet' in request.json:
        abort(400)
    if not 'species' in request.json:
        abort(400)
    if not 'ontologies' in request.json:
        abort(400)
    if atype == 'fisher' and not 'alternative' in request.json:
        abort(400)
    if not 'dataset' in request.json:
        abort(400)

    genes = request.json['dataset']
    compl_bg, _, _ = preps.read_species(request.json['species'])
    onto = request.json['ontologies']
    cat_terms, _ = preps.retrieve_categories(request.json['categorySet'])
    if len(onto) != 3:
        cat_terms = cat_terms[cat_terms['Ontology'].isin(onto)]
    if atype == 'fisher':
        alternative = request.json['alternative']
        sig = {'input_type': 'sig'}
        genes = pd.DataFrame(genes, columns=['Gene_ID'])
    else:
        alternative = None
        sig = {'input_type': 'cm'}
        genes = pd.DataFrame(genes, columns=['Gene_ID', 'measure'])

    test_res, _ = fct.run_ana_cats(
        sig, genes, compl_bg, cat_terms, alternative)

    return test_res.to_json()


@api.route('/run/<string:atype>/terms', methods=['POST'])
def runTerms(atype):
    """ Runs analysis on Terms. """
    # checks for correct request format and information
    if atype not in ['fisher', 'mwu']:
        abort(404)
    if not request.json:
        abort(400)
    if not 'categorySet' in request.json:
        abort(400)
    if not 'category' in request.json:
        abort(400)
    if not 'species' in request.json:
        abort(400)
    if not 'ontologies' in request.json:
        abort(400)
    if atype == 'fisher' and not 'alternative' in request.json:
        abort(400)
    if not 'dataset' in request.json:
        abort(400)

    n_worker = 2

    genes = request.json['dataset']
    background, _, _ = preps.read_species(request.json['species'])
    if atype == 'fisher':
        genes = pd.DataFrame(genes, columns=['Gene_ID'])
        n_genes_anno = pd.unique(background['Gene_ID']).shape[0]
    else:
        genes = pd.DataFrame(genes, columns=['Gene_ID', 'measure'])

    cat = request.json['category']
    onto = request.json['ontologies']
    cat_terms, _ = preps.retrieve_categories(request.json['categorySet'])

    if len(onto) != 3:
        cat_terms = cat_terms[cat_terms['Ontology'].isin(onto)]

    cat_terms = cat_terms[cat_terms['Category'] == cat][['GO_ID', 'Name']]
    go_terms = pd.unique(cat_terms['GO_ID'])
    bgfg = pd.merge(background, genes, how='inner')['GO_ID']
    go_terms = np.array(list(set(go_terms).intersection(set(bgfg.values))))
    if go_terms.shape[0] == 0:
        return '', 204
    background = background[background['GO_ID'].isin(go_terms)]
    n_genes_in_sample = genes.shape[0]

    if atype == 'fisher':
        alternative = request.json['alternative']
        test_res = fct.run_fisher_multi_process(
            go_terms, genes, background, n_genes_anno,
            n_genes_in_sample, alternative, n_worker
        )
    else:
        alternative = None
        test_res = fct.run_mwu_multi_process(
            genes, go_terms, background, n_genes_in_sample, n_worker
        )

    test_res = pd.merge(cat_terms.drop_duplicates(), test_res,
                        how='inner', on='GO_ID')
    return test_res.to_json()
