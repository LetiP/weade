""" Generate output like html tables and plots. """

import numpy as np
import pandas as pd
import plotly.graph_objs as go
from plotly.offline import plot

from scipy.cluster.hierarchy import linkage, leaves_list


def calc_z_score(x, m, sd):
    """ Calculates the z-score with given mean and standard deviation """
    return (x - m)/sd


def calc_z_score_list(samples):
    """ Calculates z-score for each entry in a list """
    m = np.mean(samples)
    sd = np.std(samples)
    return [calc_z_score(x, m, sd) for x in samples]


def plot_to_div(fig):
    """
    Generates a div containing the plot.
    plotly.js has to be present on the html where the plot is to be shown.
    """
    div = plot(fig, show_link=False, output_type="div", include_plotlyjs=False)
    return div


def df_to_table(test_res, table_id, cat_or_term=None):
    """ Transform any pandas dataframe to HTML table."""
    header = test_res.columns.values.tolist()
    if table_id == 'historyTable':
        header = header[1:]
    tb = '<table id="{}" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">'.format(
        table_id)
    tb += "<thead><tr><th>" + \
        "</th><th>".join(header) + "</th></tr></thead><tbody><tr>"
    tb_list = ["<td>" + "</td><td>".join(change_first_elem(
        row, table_id, cat_or_term)) + "</tr>" for _, row in test_res.iterrows()]
    tb += ''.join(tb_list) + "</tbody></table>"
    return tb


def change_first_elem(row, table_id, cat_or_term=None):
    """ Change first element of row in order to make row clickable. """
    row_data = [str(x) for x in row]
    if table_id == 'historyTable':
        row_data[1] = '<span data-toggle="popover" data-placement="left" data-value="{}">'.format(row_data[0]) + \
                      row_data[1] + '</span>'
        row_data = row_data[1:]
    elif table_id in ['catSetTable', 'droppedSymbolsTable']:
        pass
    else:
        row_data[0] = '<span data-toggle="popover" data-placement="left">' + \
            row_data[0] + '</span>'
    if table_id == 'genesTable':
        name_to_list = row_data[2].split(', ')
        if cat_or_term == 'Category':
            if len(name_to_list) <= 4:
                row_data[2] = '<span data-value="{}">'.format(
                    row_data[2]) + ', '.join(name_to_list) + '</span>'
            else:
                row_data[2] = '<span data-value="{}">'.format(
                    row_data[2]) + ', '.join(name_to_list[:4]) + ', ...</span> <span style="display:none">{}</span>'.format(row_data[2])
        else:
            goid_list = row_data[3].split(', ')
            if len(name_to_list) <= 4:
                row_data[2] = '<span data-value="{}">'.format(
                    add_quickGo(name_to_list, goid_list)) + ', '.join(name_to_list) + '</span>'
            else:
                row_data[2] = '<span data-value="{}">'.format(
                    add_quickGo(name_to_list, goid_list)) + ', '.join(name_to_list[:4]) + ', ...</span>'

    return row_data


def add_quickGo(name_to_list, goid_list):
    ''' Add the quickGo link to genes Table popover in Term view. '''
    new = []
    for g, l in zip(goid_list, name_to_list):
        new.append(
            "<a href='https://www.ebi.ac.uk/QuickGO/term/{}' target='_blank'>{}</a>".format(g, l))
    return ', '.join(new)


def enrichment_res_plot(test_res, fisher=False, analysis_name=""):
    """ Plot the Enrichment Result with Plotly. """

    colors = ['#7f0000', '#990000', '#b30000', '#c51810', '#d7301f', '#e34a34',
              '#ef6548', '#f67950', '#fc8d59', '#fca46e', '#fdbb84', '#fdd49e', '#fee8c8']

    bins = [0,
            0.00000000000000001,
            0.00000000000001,
            0.00000000001,
            0.00000001,
            0.0000001,
            0.000001,
            0.00001,
            0.0001,
            0.001,
            0.01,
            0.05,
            1, 2]

    def hex_to_rgba(h, alpha):
        h = h.lstrip('#')
        col = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4)) + (alpha,)
        col = 'rgba' + str(col)
        return col

    if np.unique(test_res['p-Value']).shape[0] != 1:
        index = np.digitize(test_res['p-Value'], bins) - 1
        legend_title = 'p-Value range'
    else:  # if we only have one p-value
        index = np.digitize(test_res['p-Value'], bins) - 2
        legend_title = ''
    # prepare the colors to generate something like a p-Value heatmap
    rgba_colors = [hex_to_rgba(x, 0.7) for x in colors]

    # format p-vals
    p_vals = test_res['p-Value'].round(decimals=3)
    p_vals = [0 if x == 0 else x for x in p_vals]
    p_vals = np.array(['p=' + str(x) for x in p_vals])

    test_res.loc[test_res['n. Category'] == 0, 'n. Category'] = 1

    ratio_vals = test_res['n. Sample Category'] / \
        test_res['n. Category'] * 100
    ratios = np.array([str(x) + '/' + str(y) for x, y in
                       zip(test_res['n. Sample Category'], test_res['n. Category'])])
    data = []
    annotations = []
    anno_pos = 0
    un_ind = np.unique(index)[::-1]
    # create as many traces as colors, in order to later generate legend
    for i in un_ind:
        ind = np.argwhere(index == i).flatten()
        # split between plot for fisher and mwu
        if fisher:
            x = ratio_vals.iloc[ind]
        else:
            x = test_res['delta Rank'].iloc[ind]
        trace = go.Bar(
            y=test_res['Category'].iloc[ind],
            x=x,
            text=p_vals[ind],
            textfont=dict(
                color='#000000'
            ),
            textposition='auto',
            orientation='h',
            name='({}, {}]'.format(bins[i], bins[i + 1]),
            marker=dict(
                color=rgba_colors[i],
                line=dict(
                    color=colors[i],
                    width=3)
            ),
            hoverinfo='none',
        )
        data.append(trace)
        if fisher:
            cur_ratios = ratios[ind]
            for j in range(len(trace.x)):
                annotations.append(dict(x=trace.x[j],
                                        y=anno_pos, text=cur_ratios[j],
                                        xanchor='left',
                                        yanchor='auto',
                                        showarrow=False,))
                anno_pos += 1
    annotations.append(dict(
        x=1.045,  # 1.13
        y=1.06,
        align="right",
        valign="top",
        text=legend_title,
        font=dict(
            size=14
        ),
        showarrow=False,
        xref="paper",
        yref="paper",
        xanchor="left",
        yanchor="top"
    ))
    # split between plot for fisher and mwu
    if fisher:
        x_label = '% of genes in category in sample'
        x_range = [0, 100]
    else:
        x_label = 'Delta Rank'
        x_range = None
    layout = go.Layout(
        title=analysis_name,
        barmode='stack',
        autosize=False,
        width=800,
        height=600,
        xaxis=dict(
            title=x_label,
            range=x_range),
        yaxis=dict(
            title='Category'),
        margin=go.layout.Margin(
            l=300,
        ),
        annotations=annotations,
    )

    fig = go.Figure(data=data, layout=layout)
    return plot_to_div(fig)


def makeHeatmap(names, results, measure="p", clustering=[], z_score='none', cutoff=None):
    """Plot heatmap"""
    data = pd.DataFrame(columns=names, index=results[0]["Category"])
    
    colors = ['#6b0000', '#7f0000', '#990000', '#b30000', '#c51810', '#d7301f', '#e34a34',
              '#ef6548', '#f67950', '#fc8d59', '#fca46e', '#fdbb84', '#fdd49e', '#fee8c8']
    if measure == "p":
        for i, name in enumerate(names):
            data[name] = np.array(results[i]["p-Value"])
    elif measure == "tf":
        for i, name in enumerate(names):
            data[name] = results[i]["n. Sample Category"].values / \
                results[i]["n. Category"].values
        if z_score == 'columns':
            for i, row in enumerate(data.values):
                data.loc[data.index[i]] = calc_z_score_list(row)
        elif z_score == 'rows':
            for _, name in enumerate(names):
                data[name] = calc_z_score_list(data[name].values)
    elif measure == "dr":
        for i, name in enumerate(names):
            data[name] = results[i]["delta Rank"].values
        if z_score == 'columns':
            for i, row in enumerate(data.values):
                data.loc[data.index[i]] = calc_z_score_list(row)
        elif z_score == 'rows':
            for _, name in enumerate(names):
                data[name] = calc_z_score_list(data[name].values)

    # cluster rows
    if "clustRow" in clustering:
        Z = linkage(data.values, "ward")
        ll = leaves_list(Z)
        labels = [data.index[i] for i in ll]
        data = data.reindex(labels)
    else:
        labels = data.index

    # cluster columns
    if "clustCol" in clustering:
        Z = linkage(data.values.transpose(), "ward")
        ll = leaves_list(Z)
        names = [names[i] for i in ll]
        data = data[names]

    annotations = makeHeatmapAnnotations(data.values.tolist())

    trace = go.Heatmap(
        x=[str(n) for n in names],
        y=[str(l) for l in labels],
        z=data.values.tolist()
    )
    if measure == 'p':
        rem = (cutoff) % 12
        if rem != 0:
            max = cutoff + (12-rem)
        else:
            max = cutoff
        bins = list(range(-2, max, max//12))
        bins = [10**x for x in bins]
        bins = [0.05] + bins
        bins = [bins[i//2] for i in range(len(bins)*2)]
        bins.insert(0, 1)
        bins.append(0)
        colors = [colors[i//2] for i in range(len(colors)*2)]
        color_scale = [[bins[i], colors[-i-1]] for i in range(len(bins))]
        color_scale.reverse()
        trace.colorscale = color_scale
        trace.colorbar = dict(
            title='p-Value range',
            tickmode='array',
            tickvals=bins
        )
    elif measure == 'tf':
        if z_score == 'none':
            trace.colorscale = 'YlOrRd'
            trace.colorbar = dict(title='term frequency')
        else:
            trace.colorscale = 'RdYlBu'
            trace.colorbar = dict(title='z-score')
    else:
        trace.colorscale = 'RdYlBu'
        if z_score == 'none':
            trace.colorbar = dict(title='delta rank')
        else:
            trace.colorbar = dict(title='z-score')

    data = [trace]

    layout = go.Layout(
        yaxis=dict(
            type="category"
        ),
        xaxis=dict(
            type="category"
        ),
        margin=go.layout.Margin(l=250, t=20),
        annotations=annotations
    )

    fig = go.Figure(data=data, layout=layout)
    return plot_to_div(fig)


def makeHeatmapAnnotations(data):
    anno = []
    for i, row in enumerate(data):
        for j, elem in enumerate(row):
            anno_elem = {
                'font': {'color': '#000000'},
                'showarrow': False,
                'text': '{:.1e}'.format(elem),
                'x': j,
                'xref': 'x1',
                'y': i,
                'yref': 'y1'
            }
            anno.append(anno_elem)
    return(anno)


def make3dHeatmap(names, results, measure, clustering=[], z_score='none', cutoff=None):
    colors = ['#6b0000', '#7f0000', '#990000', '#b30000', '#c51810', '#d7301f', '#e34a34',
              '#ef6548', '#f67950', '#fc8d59', '#fca46e', '#fdbb84', '#fdd49e', '#fee8c8']
    colors.reverse()
    rem = (cutoff) % 12
    if rem != 0:
        max = cutoff + (12-rem)
    else:
        max = cutoff
    bins = list(range(-2, max, max//12))
    bins = [10**x for x in bins]
    bins = [0.05] + bins
    data = pd.DataFrame(columns=names, index=results[0]["Category"])
    p = pd.DataFrame(columns=names, index=results[0]["Category"])
    if measure == "tf":
        for i, name in enumerate(names):
            data[name] = results[i]["n. Sample Category"].values / \
                results[i]["n. Category"].values
        if z_score == 'columns':
            for i, row in enumerate(data.values):
                data.loc[data.index[i]] = calc_z_score_list(row)
        elif z_score == 'rows':
            for _, name in enumerate(names):
                data[name] = calc_z_score_list(data[name].values)
    elif measure == "dr":
        for i, name in enumerate(names):
            data[name] = results[i]["delta Rank"].values
        if z_score == 'columns':
            for i, row in enumerate(data.values):
                data.loc[data.index[i]] = calc_z_score_list(row)
        elif z_score == 'rows':
            for _, name in enumerate(names):
                data[name] = calc_z_score_list(data[name].values)

    for i, name in enumerate(names):
        p[name] = np.array(results[i]["p-Value"])

    # cluster rows
    if "clustRow" in clustering:
        Z = linkage(data.values, "ward")
        ll = leaves_list(Z)
        labels = [data.index[i] for i in ll]
        data = data.reindex(labels)
        p = p.reindex(labels)
    else:
        labels = data.index

    # cluster columns
    if "clustCol" in clustering:
        Z = linkage(data.values.transpose(), "ward")
        ll = leaves_list(Z)
        names = [names[i] for i in ll]
        data = data[names]
        p = p[names]

    data = data.values.tolist()
    p = p.values.tolist()
    p_colors = np.digitize(p, bins)

    plotData = []
    for i, r in enumerate(data):
        row = []
        for j, e in enumerate(r):
            row.append([e, colors[p_colors[i][j]], [
                       names[j], labels[i], p[i][j]]])
        plotData.append(row)
    return plotData
