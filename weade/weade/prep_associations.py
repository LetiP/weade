# from weade.mysqlfun import MysqlData

import time
import weade.godb as godb
import pandas as pd
from biomart import BiomartServer
import numpy as np
import igraph
from os.path import expanduser
import os
from subprocess import call
from joblib import Memory
import random
from scipy.stats import rankdata
import sys
from weade.functions import calc_pathway_enrichment
from weade.models import Species, Catset, Interaction, Pathway


CACHE_PATH = expanduser("~/.python_cache")
call("mkdir -p {}".format(CACHE_PATH), shell=True)
MEMORY = Memory(cachedir=CACHE_PATH)

# # delete
# from mysqlfun import MysqlData


def search_matches(typed, clicked_col):
    ''' Search for matches to show in dropdown in category table. '''
    series = godb.PARSED_OBO[clicked_col]
    mask = series.str.contains(typed, case=False)
    return series[mask].head(15).tolist()


def search_value(value, clicked_col):
    """ Get the corresponding goid/name. """
    compl = {'GO_ID': 'Name', 'Name': 'GO_ID'}
    df = godb.PARSED_OBO
    df = df.loc[df[clicked_col] == value][[compl[clicked_col], 'Ontology']]
    return df.values.tolist()[0]


def retrieve_categories(catname='General', reg_types=['is_a', 'part_of']):
    """ Load category table from mysql and compute all offsprings. """
    if isinstance(catname, str):
        # sql = MysqlData()
        # sql.runQuery('SELECT * FROM db_goapp.tbl_catterms WHERE ' +
        #              'catset_id = (SELECT catset_id FROM tbl_catset ' +
        #              'WHERE catset_name = "{}");'.format(catname))

        catset = Catset.query.filter_by(name=catname).first()  # type: Catset
        # convert sql data into pandas DataFrame
        df = pd.DataFrame(
            [[t.id, t.goid, t.goterm, t.cat, t.catset_id] for t in catset.terms.all()],
            columns=['id', 'GO_ID', 'Name', 'Category', 'set_id'])
    else:
        df = catname
        # get ontology
        df = pd.merge(godb.PARSED_OBO, df, how='inner',
                      on='GO_ID')[['GO_ID', 'Ontology', 'Name', 'Category']]

    t1 = time.time()

    offsprings = godb.get_offspring(relations=reg_types)
    off_goids = pd.merge(offsprings, df, how='right',
                         left_on='Parent', right_on='GO_ID')
    parents = off_goids[['Parent', 'Name', 'Category']].drop_duplicates()
    parents = parents.rename(columns={'Parent': 'GO_ID'})
    off_goids = off_goids[['Offspring', 'Name', 'Category']]
    off_goids = off_goids.rename(columns={'Offspring': 'GO_ID'})
    # add parents too#
    off_goids = pd.concat([off_goids, parents])
    # bring the data into needed format
    res = pd.merge(godb.PARSED_OBO, off_goids, how='inner',
                   on=['GO_ID'])[['GO_ID', 'Ontology', 'Name_x', 'Category']].drop_duplicates()
    res = res.rename(columns={'Name_x': 'Name'})
    if isinstance(catname, str):
        # get ontology to the Category Set table
        df = pd.merge(df, res, on=['GO_ID', 'Name', 'Category'])[
            ['GO_ID', 'Ontology', 'Name', 'Category']].drop_duplicates()
    print("Computation of ALL categories offsprings took:", time.time() - t1)
    return res, df


def list_available_categories(user=None):
    # sql = MysqlData()
    # sql.runQuery(
    #     'SELECT catset_name FROM db_goapp.tbl_catset WHERE catset_owner = 1;')
    categories = Catset.query.filter_by(owner=1).all()
    if user is not None:
        raise NotImplementedError

    return [cat.name for cat in categories]


def list_available_species():
    # sql = MysqlData()
    # sql.runQuery('SELECT species_name FROM db_goapp.tbl_species;')

    # return [x[0] for x in sql.data]

    species = Species.query.all()
    return [sp.name for sp in species]


def species_identifiers_link(species):
    # sql = MysqlData()
    # sql.runQuery(
    #     'SELECT species_gID, species_identifier FROM db_goapp.tbl_species' +
    #     ' WHERE species_name="{}";'.format(species))
    # return sql.data[0]

    sp = Species.query.filter_by(name=species).first()
    return [sp.g_id, sp.identifier]


@MEMORY.cache(verbose=0)
def get_identifier_mapping(dataset, identifier):
    server = BiomartServer("http://www.ensembl.org/biomart")
    mart = server.datasets[dataset]
    r = mart.search(
        {'attributes': ['entrezgene_id', identifier, 'external_gene_name']})

    lines = [line.decode('utf-8').split('\t') for line in r.iter_lines()]

    # remove incomplete lines
    df = pd.DataFrame(lines, columns=['entrezgene', 'Gene_ID', 'Symbol'])
    df['entrezgene'].replace('', np.nan, inplace=True)
    df.dropna(subset=['entrezgene'], inplace=True)
    df['entrezgene'] = df['entrezgene'].astype(int)
    return df


@MEMORY.cache(verbose=0)
def get_any_ident_mapping(dataset, ident1, ident2):
    server = BiomartServer("http://www.ensembl.org/biomart")
    mart = server.datasets[dataset]
    r = mart.search(
        {'attributes': [ident1, ident2]}
    )

    lines = [line.decode('utf-8').split('\t') for line in r.iter_lines()]

    df = pd.DataFrame(lines, columns=[ident1, ident2])
    df[ident1].replace('', np.nan, inplace=True)
    df.dropna(subset=[ident1], inplace=True)
    return df


@MEMORY.cache(verbose=0)
def load_interactions(species, taxid):
    dataset, identifier = species_identifiers_link(species)
    symbol_geneid = get_identifier_mapping(dataset, identifier)

    # sql = MysqlData()
    # sql.runQuery('SELECT interaction_a, interaction_b, interaction_type FROM db_goapp.tbl_interaction' +
    #              ' WHERE interaction_taxid_a={} AND interaction_taxid_b={};'.format(taxid, taxid))
    inter = Interaction.query.filter_by(taxid_a=taxid, taxid_b=taxid)
    data = [[i.a, i.b, i.itype] for i in inter]
    data = pd.DataFrame(
        data, columns=['interactor_a', 'interactor_b', 'inter_type'])
    data['interactor_a'] = data['interactor_a'].astype(int)
    data['interactor_b'] = data['interactor_b'].astype(int)
    # replace integer id with Fbgn id.
    data = pd.merge(data, symbol_geneid, how='left',
                    left_on='interactor_a',
                    right_on='entrezgene')[['Gene_ID',
                                            'interactor_b',
                                            'inter_type']].dropna(axis=0,
                                                                  how='any').rename(index=str,
                                                                                    columns={'Gene_ID': 'interactor_a'})

    data = pd.merge(data, symbol_geneid, how='left',
                    left_on='interactor_b',
                    right_on='entrezgene')[['interactor_a',
                                            'Gene_ID',
                                            'inter_type']].dropna(axis=0,
                                                                  how='any').rename(index=str,
                                                                                    columns={'Gene_ID': 'interactor_b'})
    return data, symbol_geneid


@MEMORY.cache(verbose=0)
def load_pathways(species):
    # sql = MysqlData()
    # q = 'SELECT species_gID, species_identifier, species_pID FROM db_goapp.tbl_species WHERE species_name = "{}";'.format(
    #     species)
    spec = Species.query.filter_by(name=species).first()  # type: Species
    # species_ID = sql.data[0][0]
    # gene_ID = sql.data[0][1]
    # pathway_gene_ID = sql.data[0][2]
    sp_name = species.split(" (")[0]
    # sql = MysqlData()
    # sql.runQuery('SELECT pathway_gene, pathway_Rid, pathway_link, pathway_name ' +
    #              'FROM db_goapp.tbl_pathway WHERE pathway_species = "{}"'.format(sp_name))
    # sql.data = pd.DataFrame(
    #     sql.data, columns=['gene', 'pathway', 'link', 'name']
    # )
    pathways = Pathway.query.filter_by(species=sp_name).all()  # type: list[Pathway]
    pathway_data = [[p.gene, p.Rid, p.link, p.name] for p in pathways]
    pathway_data = pd.DataFrame(
        pathway_data, columns=['gene', 'pathway', 'link', 'name']
    )
    if spec.p_id != spec.identifier:
        id_mapping = get_any_ident_mapping(spec.g_id, spec.identifier, spec.p_id)
        pathway_data = pd.merge(pathway_data, id_mapping, how='left', left_on='gene', right_on=spec.p_id)[[
            spec.g_id, 'pathway', 'link', 'name'
        ]].dropna(axis=0, how='any').rename(index=str, columns={spec.identifier: 'gene'})

    return pathway_data


def build_inter_graph(interactions):
    """ Build the interaction graph for igraph. """
    a = interactions['interactor_a'].tolist()
    b = interactions['interactor_b'].tolist()

    G = igraph.Graph()
    G.add_vertices(list(set(interactions['interactor_a'].unique().tolist() +
                            interactions['interactor_b'].unique().tolist())))
    G.add_edges(zip(a, b))
    return G


def build_GO_graph(reg_types, directed=False):
    """ Build the go ontology graph (3 trees). """
    offsprings = godb.get_children(relations=reg_types)

    # create graph
    a = offsprings['Child'].tolist()
    b = offsprings['Parent'].tolist()

    G = igraph.Graph(directed=directed)
    G.add_vertices(list(set(offsprings['Child'].unique().tolist() +
                            offsprings['Parent'].unique().tolist())))
    G.add_edges(zip(a, b))
    if directed:
        G.degree(mode="out")
        return G, offsprings
    else:
        return G


def build_onto_tree(go_id, session):
    """ Build the ontology graph and sent it to visjs. """
    edge_color = {'is_a': '#e95420',
                  'part_of': '#377eb8',
                  'has_part': '#4daf4a',
                  'regulates': '#984ea3',
                  'positively_regulates': '#e41a1c',
                  'negatively_regulates': 'black',
                  'occurs_in': '#a65628'}
    goG, offsprings = build_GO_graph(session['reg_types'], directed=True)

    # s = set(goG.subcomponent(go_id, mode="out"))
    # t = set(goG.subcomponent("GO:0005575", mode="in"))
    # vert = s.intersection(t)

    vert = set(goG.subcomponent(go_id, mode="out"))
    levels = []
    for node in vert:
        levels.append(len(set(goG.subcomponent(node, mode="out"))))
    levels = rankdata(levels, method='dense').astype(int).tolist()

    edgees = [x.tuple for x in goG.es if x.source in vert and x.target in vert]

    # convert GO_ID to go name
    vert = pd.DataFrame([goG.vs[x]['name'] for x in vert], columns=['GO_ID'])
    go_name = godb.PARSED_OBO[['GO_ID', 'Name']]
    vert = pd.merge(vert, go_name, how='inner', on='GO_ID')

    # build graph in visjs format
    nodes, edges = [], []
    for i, row in vert.iterrows():
        nodes.append(
            {"id": row['GO_ID'], "label": row['Name'], "level": levels[i]})

    for edge in edgees:
        fom = goG.vs[edge[0]]['name']
        to = goG.vs[edge[1]]['name']
        edge_type = offsprings.loc[(offsprings['Child'] == fom) & (
            offsprings['Parent'] == to)]['Relation'].tolist()[0]
        edges.append(
            {"from": fom, "to": to, 'arrows': 'to', 'label': edge_type,
             "color": {
                 "color": edge_color[edge_type]
             }})

    return nodes, edges


def shortest_path(G, nodes, connectivity=2):
    conn_nodes = []
    path_lens = G.shortest_paths(nodes, nodes)
    max_j = 0
    for i, x in enumerate(nodes):
        for j, y in enumerate(nodes):
            if j > max_j - 1:
                break
            if path_lens[i][j] < connectivity:
                path = G.get_shortest_paths(x, y)
                for v in path:
                    conn_nodes.append([G.vs[x]['name'] for x in v])
        max_j += 1
    return conn_nodes


def compute_go_dist(vert, conn_nodes, session, approx, sample_size=25):
    """ Compute the go distances on interaction network. """
    goG = build_GO_graph(session['reg_types'])
    gene_to_term = session['direct_goterms']
    gene_to_term = gene_to_term.loc[gene_to_term['Gene_ID'].isin(vert)]
    gene_to_term = gene_to_term.loc[gene_to_term['GO_ID'].isin(
        [x['name'] for x in goG.vs])]

    edge_weights = {}
    for i, gene in enumerate(conn_nodes):
        gene1 = gene[0]
        gene2 = gene[1]
        terms1 = gene_to_term.loc[gene_to_term['Gene_ID']
                                  == gene1]['GO_ID']
        terms2 = gene_to_term.loc[gene_to_term['Gene_ID']
                                  == gene2]['GO_ID']

        if not approx:
            path_lens = np.array(goG.shortest_paths(terms1, terms2))
        else:
            if len(terms1) > sample_size:
                terms1 = random.sample(terms1.tolist(), k=sample_size)
            if len(terms2) > sample_size:
                terms2 = random.sample(terms2.tolist(), k=sample_size)
            path_lens = np.array(goG.shortest_paths(
                terms1, terms2), dtype=float)

        path_lens[path_lens == float('Inf')] = np.nan
        edge_weights[(gene1, gene2)] = np.nanmean(
            np.nanmin(path_lens, axis=1)) * 95
    return edge_weights


def visualise_graph(interactions, symbol_geneid, vert, edge_weights, genes, active_ana, species, taxid):
    """ Make the interaction network ready for visjs. """
    # create graph to be visualised
    edge_color = {'direct interaction': '#e95420',
                  'physical association': '#377eb8',
                  'colocalization': '#4daf4a',
                  'suppressive genetic interaction defined by inequality': '#984ea3',
                  'association': '#e41a1c',
                  'additive genetic interaction defined by inequality': 'black',
                  'synthetic genetic interaction defined by inequality': '#a65628'}
    nodes = []

    vert = pd.DataFrame(vert, columns=['Gene_ID'])
    vert = pd.merge(vert, symbol_geneid, how='left',
                    on='Gene_ID')[['Gene_ID', 'Symbol']]

    sub_nodes = genes['Gene_ID'].tolist()
    ana_names = genes['Analysis'].tolist()
    for _, node in vert.iterrows():
        idx = node['Gene_ID']
        if idx in sub_nodes:
            pos = sub_nodes.index(idx)
            try:
                nodes.append(
                    {"id": idx, "label": node['Symbol'],
                        "color": {"background": active_ana[ana_names[pos]]}
                     }
                )
            except KeyError as e:
                print("KeyError in prep_associations.visualise_graph")
                print(node, active_ana, ana_names, pos)
                print("species:", species, taxid)
        else:
            nodes.append({"id": idx, "label": node['Symbol'],
                          "borderWidth": 1.3,
                          "color": {
                          "background": "#dbd2cb"}
                          })
    interactions = pd.merge(vert, interactions, how='left', left_on='Gene_ID',
                            right_on='interactor_a')[['interactor_a', 'interactor_b', 'inter_type']]
    interactions = pd.merge(vert, interactions, how='left', left_on='Gene_ID',
                            right_on='interactor_b')[['interactor_a', 'interactor_b',
                                                      'inter_type']].dropna(axis=0, how='any').drop_duplicates()
    edges = []
    appended = {}
    for _, edge in interactions.iterrows():
        inter_type = edge['inter_type'].rstrip(")").split("(")[-1]
        fom = edge['interactor_a']
        to = edge['interactor_b']
        # it is None if no go dists active
        if edge_weights:
            if fom == to:
                length = 0.0001
            else:
                if (fom, to) in edge_weights:
                    length = edge_weights[(fom, to)]
                elif (to, fom) in edge_weights:
                    length = edge_weights[(to, fom)]
                else:
                    length = 95
        else:
            length = 95
        if to in sub_nodes and fom in sub_nodes:
            dashes = False
        else:
            dashes = [5, 2]
        if (to, fom, inter_type) not in appended:
            appended[(fom, to, inter_type)] = True
            edges.append({"from": fom,
                          "to": to,
                          "length": length,
                          "label": inter_type,
                          "dashes": dashes,
                          "color": {
                              "color": edge_color[inter_type]
                          }})
    return nodes, edges


def read_species(species):
    """ Read species by name from .txt-files."""
    # get species data filepath from SQL
    # sql = MysqlData()
    # sql.runQuery(
    #     'SELECT species_dataset, species_link FROM db_goapp.tbl_species WHERE species_name="{}";'
    #     .format(species))
    spec = Species.query.filter_by(name=species).first()  # type: Species
    try:
        bg_path = spec.dataset
        species_link = spec.link
    except IndexError as e:
        print("IndexError in prep_associations.read_species")
        print("species:", species)
    d_path = os.path.join(sys.path[0], 'weade/ga/direct' + bg_path[5:])
    bg_path = os.path.join(sys.path[0], "weade/" + bg_path)

    # read txt files
    complete_backgr = pd.read_csv(bg_path, sep='\t', names=[
        'Gene_ID', 'GO_ID', 'Symbol']).drop_duplicates().dropna(how='any')
    direct_goterms = pd.read_csv(d_path, sep='\t', names=[
        'Gene_ID', 'GO_ID', 'Symbol']).dropna(how='any')

    return complete_backgr, direct_goterms, species_link


def computePairWiseInter(to_compare, session):
    """ Compute the pairwise interactions dataframe. """
    history = session['history']

    genes_1 = history[to_compare[0]].foreground
    genes_2 = history[to_compare[1]].foreground
    bg = history[to_compare[0]].background
    ana_1 = history[to_compare[0]].name
    ana_2 = history[to_compare[1]].name

    species = history[to_compare[0]].species
    if session['chosen_species'] != species:
        if species in session['custom_species'].keys():
            session['complete_backgr'], session['direct_goterms'], session['species_link'] = session['custom_species'][species]
        else:

            session['complete_backgr'], session['direct_goterms'], session['species_link'] = read_species(
                species)
        session['chosen_species'] = species
        session['current_analysis_name'] = history[to_compare[0]].name
        session['input_type'] = history[to_compare[0]].input_type
        session['alternative'] = history[to_compare[0]].alternative

    sp_link = session['species_link']
    taxid = session['taxids'][species]

    interactions, symbol_geneid = load_interactions(species, taxid)
    # take only the ligand-receptor interactions reqTerms <- c("GO:0005102", "GO:0004872")
    gene_to_term = session['complete_backgr']
    gene_to_term = gene_to_term[gene_to_term['GO_ID'].isin(
        ["GO:0005102", "GO:0004872"])]
    interactions = pd.merge(interactions, gene_to_term, how='inner',
                            left_on='interactor_a', right_on='Gene_ID')[['interactor_a', 'interactor_b', 'inter_type']]
    interactions = pd.merge(interactions, gene_to_term, how='inner',
                            left_on='interactor_b', right_on='Gene_ID')[['interactor_a', 'interactor_b', 'inter_type']]
    # compute the a -> b direction
    to_right = pd.merge(genes_1, interactions, how='inner', left_on='Gene_ID',
                        right_on='interactor_a')[['interactor_a', 'interactor_b', 'inter_type']]
    to_right = pd.merge(to_right, genes_2, how='inner', left_on='interactor_b',
                        right_on='Gene_ID')[['interactor_a', 'interactor_b', 'inter_type']].drop_duplicates()
    # compute the b -> a direction
    to_left = pd.merge(genes_1, interactions, how='inner', left_on='Gene_ID',
                       right_on='interactor_b')[['interactor_a', 'interactor_b', 'inter_type']]
    to_left = pd.merge(to_left, genes_2, how='inner', left_on='interactor_a',
                       right_on='Gene_ID')[['interactor_a', 'interactor_b', 'inter_type']].drop_duplicates()
    to_left = to_left.rename(columns={'interactor_a': 'interactor_b',
                                      'interactor_b': 'interactor_a'})
    # insert beautiful arrows
    to_right['Direction'] = "<span class='fa fa-long-arrow-right'></span>"
    to_left['Direction'] = "<span class='fa fa-long-arrow-left'></span>"
    # collapse <->
    interactions = pd.concat([to_right, to_left])
    interactions = interactions.groupby(['interactor_a', 'interactor_b', 'inter_type'], as_index=False).agg(
        lambda x: x if x.size == 1 else "<span class='fa fa-arrows-h'></span>")

    pw_data = load_pathways(species)
    n_genes_anno = pd.unique(bg['Gene_ID']).shape[0]
    all_genes = pd.concat([genes_1, genes_2])
    all_genes = all_genes.drop_duplicates()
    interactions = calc_pathway_enrichment(
        interactions, pw_data, species, all_genes, n_genes_anno)

    # include link to FlyBase

    def symbol_link_a(row):
        return '<a href="{}{}" target="_blank">{}</a>'.format(sp_link, row['interactor_a'], row['Symbol'])

    def symbol_link_b(row):
        return '<a href="{}{}" target="_blank">{}</a>'.format(sp_link, row['interactor_b'], row['Symbol'])

    interactions = pd.merge(interactions, symbol_geneid, how='inner', left_on='interactor_a',
                            right_on='Gene_ID')[['interactor_a', 'Symbol',
                                                 'Direction', 'interactor_b', 'inter_type', 'n_enriched', 'enriched_pathways']]
    interactions['interactor_a'] = interactions.apply(symbol_link_a, axis=1)
    interactions = interactions[['interactor_a',
                                 'Direction', 'interactor_b', 'inter_type', 'n_enriched', 'enriched_pathways']]

    interactions = pd.merge(interactions, symbol_geneid, how='inner', left_on='interactor_b',
                            right_on='Gene_ID')[['interactor_a', 'Symbol',
                                                 'Direction', 'interactor_b', 'inter_type', 'n_enriched', 'enriched_pathways']]
    interactions['interactor_b'] = interactions.apply(symbol_link_b, axis=1)
    interactions = interactions[['interactor_a',
                                 'Direction', 'interactor_b', 'inter_type', 'n_enriched', 'enriched_pathways']]
    # interactions = interactions.sort_values(by='n_enriched')

    # rename the interaction table to be displayed in html
    return interactions.rename(columns={'interactor_a': ana_1, 'interactor_b': ana_2,
                                        'inter_type': 'Interaction Type',
                                        'enriched_pathways': 'Enriched Pathway (p-value)',
                                        'n_enriched': 'n. sign. enriched pathways'})
