""" History for reproducing previous analyses """


class History_item:
    """ Storing all information for running an analysis"""

    def __init__(self, fg, bg, species, ontos, input_type, name, alternative, result):
        self.foreground = fg
        self.background = bg
        self.species = species
        self.ontologies = ontos
        self.input_type = input_type
        self.name = name
        self.alternative = alternative
        self.result = result

    def to_json(self):
        """ Prepare for saving as json """
        j = {
            'foreground': self.foreground.to_json(),
            'background': self.background.to_json(),
            'species': self.species,
            'ontologies': self.ontologies,
            'input_type': self.input_type,
            'name': self.input_type,
            'alternative': self.alternative,
            'result': self.result.to_json()
        }
        return j
