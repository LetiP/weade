$(document).ready(function () {

  // get Categories to fill in dropdown
  $.ajax({
    type: "GET",
    url: "/getCategories",
    success: function (response) {
      $(".loading-overlay").fadeOut("fast");

      var select = document.getElementById("cats");
      var response = JSON.parse(response);
      var catChoices = response["catChoices"];
      catChoices = catChoices.map(function (x) {
        return { item: x };
      });

      catSel = $("#cats").selectize({
        delimiter: ",",
        persist: false,
        labelField: "item",
        valueField: "item",
        options: catChoices
      });

      // custom categories
      customBgCat(
        "#userCatUpload",
        "/customCats",
        "/customCatsName",
        "#userCatUpload_progress",
        "#clearUserCats",
        "#cat-browse-info",
        catSel
      );

      updateCatSetTable(response);

      // set the species in files tab
      $.ajax({
        type: "GET",
        url: "/getSpecies",
        success: function (response) {
          var select = document.getElementById("species");
          var response = JSON.parse(response);
          var species = response["species"];
          species = species.map(function (x) {
            return { item: x };
          });

          specSelect = $("#selectspec").selectize({
            delimiter: ",",
            persist: false,
            labelField: "item",
            valueField: "item",
            options: species
          });
          // custom background
          customBgCat(
            "#bg",
            "/customBackground",
            "/customSpeciesName",
            "#bg_progress",
            "#clearBg",
            "#bg-browse-info",
            specSelect
          );
          readSpecies();
        }
      });
    }
  });

  swal({
    title: "",
    text: "WEADE does not store your uploaded data permanently. Your data is only stored during the session to perform the analyses and deleted upon reloading or closing the browser tab or window. Your uploaded data is stored on the server in a way that it is not accessible for us or any third party. The application requires temporary cookies to function. These cookies expire at the end of the session. We do not use any permanent cookies. By clicking OK you agree to our terms of use.",
    type: "warning"
  });

  // toggle advanced category relations
  $("#adv-cat-opt").click(function () {
    $("#adv-cat").slideDown();
    $("#adv-cat-opt").hide();
    $("#hide-adv-cat-opt").fadeIn("fast");
  });

  $("#hide-adv-cat-opt").click(function () {
    $("#adv-cat").slideUp();
    $("#hide-adv-cat-opt").hide();
    $("#adv-cat-opt").fadeIn("fast");
  });

  // toggle advanced category relations
  $("#adv-rel-opt").click(function () {
    $("#cat-relations").slideDown();
    $("#adv-rel-opt").hide();
    $("#hide-rel-cat-opt").fadeIn("fast");
  });

  $("#hide-rel-cat-opt").click(function () {
    $("#cat-relations").slideUp();
    $("#hide-rel-cat-opt").hide();
    $("#adv-rel-opt").fadeIn("fast");
  });

  // toggle advanced species relations
  $("#adv-sp-opt").click(function () {
    $("#addSp").slideDown();
    $("#adv-sp-opt").hide();
    $("#hide-adv-sp-opt").fadeIn("fast");
  });

  $("#hide-adv-sp-opt").click(function () {
    $("#addSp").slideUp();
    $("#hide-adv-sp-opt").hide();
    $("#adv-sp-opt").fadeIn("fast");
  });
  $("#clearBg").click(function () {
    var sel = specSelect[0].selectize;
    sel.enable();
    readSpecies();
    $("#bg_progress").fadeOut();
    $("#clearBg").fadeOut();
    $("#bg-browse-info").attr("placeholder", "No file selected");
  });
  $("#clearUserCats").click(function () {
    var sel = catSel[0].selectize;
    sel.enable();
    useCatSet();
    $("#userCatUpload_progress").fadeOut();
    $("#clearUserCats").fadeOut();
    $("#cat-browse-info").attr("placeholder", "No file selected");
  });

  // use cat set and switch to further point in workflow - files
  $("#applyRels").click(function (event) {
    useCatSet();
  });
  // use cat set and switch to further point in workflow - files
  $("#cats").on("change", function () {
    useCatSet();
  });

  // clear collection on species change
  $("#selectspec").on("change", function () {
    collection.clear().draw(false);
    $("#showNet").attr("disabled", true);
    readSpecies();
  });

  // Input type mutually exclusive switches
  mutuallyExclSwitch("user-data-type", 2);
  // check if the input type matches the file
  sendSignifContStatus();
  $("#type :checkbox").change(function (event) {
    sendSignifContStatus();
  });

  // Heat map measure mutually exclusive switches
  mutuallyExclSwitch("heatMeasure", 3);
  mutuallyExclSwitch("heatIsolation", 2);
  mutuallyExclSwitch("heatColumns", 2);
  mutuallyExclSwitch("heatScale", 3);
  mutuallyExclSwitch("heatChoice", 2);
  mutuallyExclSwitch("3dheatMeasure", 2);
  $("#heatColumns :checkbox").change(function (event) {
    $("#showHeatmapSorting").prop("disabled", !$("#customCol").prop("checked"));
  });

  // Trigger Apply Sets on enter
  $("#setsName").keypress(function (e) {
    if (e.which == 13) {
      $("#applysets").trigger("click");
      return false; //<---- Add this line
    }
  });

  $("#useExampleData").on("click", function () {
    resetUpload();
    var fileNames = ["dmel_gro_FBgn.txt", "dmel_repro_FBgn.txt"];
    if (
      specSelect[0].selectize.getValue() != "Drosophila melanogaster (FBgn)"
    ) {
      specSelect[0].selectize.setValue("Drosophila melanogaster (FBgn)");
    }
    $("#sig").prop("checked", true);
    $("#cm").prop("checked", false);
    $("#useSymbol").prop("checked", false);

    $.ajax({
      type: "POST",
      url: "/useExampleData",
      success: function () {
        $("#filenames").html("");
        for (var i = 1; i <= fileNames.length; i++) {
          var html =
            '<div class="form-group shiny-input-container">' +
            '<label for="name_' +
            i +
            '">Name for <em><span style="font-weight:normal"> ' +
            fileNames[i - 1] +
            "</span></em></label>" +
            '<input id="name_' +
            i +
            '" type="text" class="form-control shiny-bound-input shinyjs-resettable" value="" data-shinyjs-resettable-id="name_' +
            i +
            '1" data-shinyjs-resettable-type="Text" data-shinyjs-resettable-value="">' +
            "</div>";
          $(html)
            .hide()
            .appendTo("#filenames")
            .slideDown();
        }
        $("#browse-info").attr("placeholder", fileNames.length + " files");
        $("#fg_progress").hide();
        showVenn(fileNames.length, fileNames);
      }
    });
  });

  // send information on selected bg and fg
  $("#applysets").click(function (event) {
    if ($("#setsName").val() == "") {
      swal(
        {
          title: "Missing analysis name.",
          text: "Please name your analysis:",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          inputPlaceholder: "Write something"
        },
        function (inputValue) {
          if (inputValue === false) return false;
          if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false;
          }
          $("#setsName").val(inputValue);
          swal("", "Nice!", "success");
          setTimeout(function () {
            swal.close();
            applySets();
          }, 800);
        }
      );
    } else {
      applySets();
    }
  });

  // START THE ANALYSIS
  $("#start").click(function () {
    $(".loading-overlay").fadeIn("fast");
    $("#start").attr("disabled", true);
    // send status of ontologies
    var toPython = new Object();
    toPython.checkedOnto = new Array();
    var ontoCheckboxes = $(".material-switch-input.onto:checkbox:checked");
    for (var i = 0; i < ontoCheckboxes.length; i++) {
      toPython.checkedOnto.push(ontoCheckboxes[i].value);
    }
    // send alternative of Fisher test
    var alternative = $(
      "#alternative .material-switch-input:checkbox:checked"
    )[0].value;
    toPython.alternative = alternative;
    $.ajax({
      type: "POST",
      url: "/startAnalysisCategories",
      data: JSON.stringify(toPython, null, "\t"),
      contentType: "application/json;charset=UTF-8",
      success: function (response) {
        successStartAnaCat(response);
      },
      complete: function () {
        completeStartAnaCat();
      }
    });
  });

  // Return to Category view after having run analysis on terms.
  $("#backToCatsBtn").click(function () {
    $(this).fadeOut("slow");
    $.ajax({
      type: "POST",
      url: "/backToCats",
      success: function (response) {
        var response = JSON.parse(response);
        // Add name of analysis to bread crumbs field
        // Draw Enrichment Result plot
        $("#plot").html(response["plot"]);

        // Draw Enrichment Result table
        $("#output").html(response["enrich_table"]);
        $("#enrichTable").DataTable({
          scrollY: 600,
          scrollX: "100%",
          scrollCollapse: true,
          deferRender: true,
          scroller: true,
          select: true,
          dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
          buttons: ["copy", "csv", "excel", "pdf", "print"]
        });
        $("#collect").hide();

        $(".loading-overlay").fadeOut("fast");
        setUpRowTableClick("enrichTable", "popover-buttons");
        // make plotly plot react on hover by emphasising traces
        setUpPlotlyExt();
        $("#genes").html(response["genes_table"]);
        var table = $("#genesTable").DataTable({
          scrollY: 600,
          scrollX: "100%",
          scrollCollapse: true,
          deferRender: true,
          scroller: true,
          select: true,
          dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
          buttons: ["copy", "csv", "excel", "pdf", "print"]
        });
        // Make the GO_ID column invisible
        table.column(3).visible(false);
        setUpRowTableClick("genesTable", "popover-genes");
        $("#breadcrumbs").text(response["analysis_name"]);
      }
    });
  });

  // clear whole history
  $("#clearHistory").on("click", function () {
    deleteFromHistory(false);
    historyTable.clear().draw(false);
  });

  // clear selection from history
  $("#remove_history").on("click", function () {
    deleteFromHistory(true);
    historyTable
      .rows(".selected")
      .remove()
      .draw(false);
  });

  // initialise collection datatable
  collection = $("#collection_table").DataTable({
    scrollY: 600,
    scrollX: "100%",
    scrollCollapse: true,
    deferRender: true,
    scroller: true,
    select: true,
    dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
    buttons: ["copy", "csv", "excel", "pdf", "print"]
  });

  // clear whole collection table
  $("#clearColl").on("click", function () {
    collection.clear().draw(false);
    $("#showNet").attr("disabled", true);
  });

  // clear only selected content
  $("#clearCollSel").on("click", function () {
    collection
      .rows(".selected")
      .remove()
      .draw(false);
    if (!collection.data().any()) {
      $("#showNet").attr("disabled", true);
    }
  });

  $("#selectInteraction").selectize({
    valueField: "item",
    create: true
  });

  // show network
  $("#showNet").on("click", function () {
    $("#network-box").fadeIn();
    computeNet(true);
  });

  $("#moreConnectNodes").on("click", function () {
    var val = parseInt($("#numConnectNodes").html());
    val += 1;
    $("#numConnectNodes").html(val);
    computeNet();
  });
  $("#lessConnectNodes").on("click", function () {
    var val = parseInt($("#numConnectNodes").html());
    val -= 1;
    $("#numConnectNodes").html(val);
    computeNet();
  });
  $("#interTypes input[type=checkbox]").change(function () {
    computeNet();
  });
  $("#useLayout").change(function () {
    drawNet(nodes, edges);
  });
  // make mutually exclusive switches but allow for both to be off
  $("#goDistSwitches .material-switch .material-switch-input").click(
    function () {
      var checkedState = $(this).prop("checked");
      var switchers = $(this)
        .parent(".material-switch")
        .parent(".switch")
        .parent("#goDistSwitches")
        .children(".switch")
        .children(".material-switch");
      if (checkedState) {
        switchers
          .children(".material-switch-input")
          .prop("checked", !checkedState);
        $(this).prop("checked", checkedState);
      }
    }
  );
  $("#goDistSwitches input[type=checkbox]").change(function () {
    computeNet();
  });

  $("#network").on("click", function (event) {
    var sel = search[0].selectize;
    sel.blur(); // close dropdown also on canvas click

    // if selected, write in selectize
    var selNodes = network.getSelectedNodes();
    sel.setValue(selNodes);
  });

  // close network
  $("#closenet").on("click", function () {
    $("#network-box").fadeOut("fast");
  });
  // close interactions
  $("#closeInteractions").on("click", function () {
    $("#interactions-overlay").fadeOut("fast");
  });
  // open where are my genes
  $("#where_my_genes").on("click", function (event) {
    $("#where-my-genes-overlay").fadeIn("fast");
  });
  // close where are my genes
  $("#closeWhereGenes").on("click", function () {
    $("#where-my-genes-overlay").fadeOut("fast");
  });
  // close ontotree
  $("#closeontotree").on("click", function () {
    $("#ontotree-overlay").fadeOut("fast");
  });

  $("#clearCatTable").on("click", function () {
    catSetTable.clear().draw(false);
    $("#apply_cats").attr("disabled", true);
    $("#editInCat").attr("disabled", true);
    $("#removeFromCat").attr("disabled", true);
    $("#termContextCatSet").attr("disabled", true);
  });

  $("#apply_cats_edit").on("click", function () {
    applyCatsEdit();
  });

  // display contact information on click on footer
  $("body").on("click", ".fa-chevron-down.footerToggle", function () {
    $(".contact").slideDown();
    $(this).removeClass("fa-chevron-down");
    $(this).addClass("fa-chevron-up");
  });
  $("body").on("click", ".fa-chevron-up.footerToggle", function () {
    $(".contact").slideUp();
    $(this).removeClass("fa-chevron-up");
    $(this).addClass("fa-chevron-down");
  });
});

// display warning before leaving or reloading page
window.onbeforeunload = function (event) {
  event.returnValue = "You're about to leave the site, your data will be lost.";
};

/**
 * Find the switches in a group that are active.
 * @param {str} divId
 */
function findActiveSwitches(divId) {
  var activeInter = new Array();
  var interSwitches = $(divId + " .material-switch .material-switch-input");
  for (var i = 0; i < interSwitches.length; i++) {
    if (interSwitches.eq(i).prop("checked")) {
      var val = interSwitches.eq(i).prop("value");
      activeInter.push(val);
    }
  }
  return activeInter;
}

function readSpecies() {
  var toPython = new Object();
  toPython.chosenSpecies = $("#selectspec").children()[0].value;

  $(".loading-overlay").fadeIn("fast");
  $.ajax({
    type: "POST",
    url: "/readSpecies",
    async: false,
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      speciesLink = response["speciesLink"];
      $(".loading-overlay").fadeOut("fast");
    }
  });
}

function updateCatSetTable(response) {
  $("#selCatSet").html(response["catSetTable"]);
  catSetTable = $("#catSetTable").DataTable({
    scrollY: 550,
    scrollX: "100%",
    scrollCollapse: true,
    deferRender: true,
    scroller: true,
    order: [[3, "asc"]],
    select: "single",
    altEditor: true,
    dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
    buttons: ["copy", "csv", "excel", "pdf", "print"]
  });

  // implement deselect on click outside table except buttons.
  $(document).on("click", "#body", function (event) {
    if (
      !$(event.target).closest("tbody").length &&
      !$(event.target).closest(".btn").length &&
      !$(event.target).closest(".modal").length
    ) {
      catSetTable.rows().deselect();
    }
  });
  editTable(catSetTable, "#catSetTable");

  $("#termContextCatSet").off("click"); // unbind clicks from previous datatables

  catSetTable.on("select", function () {
    $("#termContextCatSet").prop("disabled", false);
    $("#editInCat").prop("disabled", false);
    $("#removeFromCat").prop("disabled", false);
  });

  catSetTable.on("deselect", function () {
    $("#termContextCatSet").prop("disabled", true);
    $("#editInCat").prop("disabled", true);
    $("#removeFromCat").prop("disabled", true);
  });

  // show Ontology tree
  $("#termContextCatSet").on("click", function () {
    $("#ontotree-overlay").fadeIn("fast");
    drawOntoTree("catSetTable");
    catSetTable.rows().deselect();
  });
}

function applySets() {
  $(".loading-overlay").fadeIn("fast");
  var toPython = new Object();
  toPython.foreground = canvasArea.foreground;
  toPython.background = canvasArea.background;
  var anaName = $("#setsName").val();
  if (!isNaN(anaName)) {
    toPython.analysisName = "A." + anaName;
  } else {
    toPython.analysisName = anaName;
  }
  toPython.useGeneSymbol = $("#useSymbol").prop("checked");
  // send chosen species by user in dropdown
  $("#breadcrumbs").text(toPython.analysisName);
  toPython.chosenSpecies = $("#selectspec").children()[0].value;
  var applyStatus;
  $.ajax({
    type: "POST",
    url: "/applySets",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      if (response !== undefined) {
        applyStatus = JSON.parse(response);
      } else {
        applyStatus = {};
      }
      if ("error" in applyStatus) {
        swal({
          title: "Sets not applied!",
          text: applyStatus["error"],
          type: "error"
        });
      } else {
        $("#useSymbol").prop("checked", false);
        // switch to next tab, RUN
        var nextId = $("#sidetabs-content")
          .children(".tab-pane")
          .next()
          .next()
          .attr("id");
        $("[href=\\#" + nextId + "]").tab("show");
        $("#start").removeAttr("disabled");
      }
      $(".loading-overlay").fadeOut("fast");
    },
    complete: function () {
      if (!("error" in applyStatus)) {
        applySetsComplete();
      }
    }
  });
}
function applySetsComplete(dropped = false) {
  var dropped = dropped;
  $.ajax({
    type: "GET",
    url: "/getPrelimGenesTable",
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      $("#genes").html(response["genes_table"]);
      $("#pGenesTable").DataTable({
        scrollY: 600,
        scrollX: "100%",
        scrollCollapse: true,
        deferRender: true,
        scroller: true,
        select: true,
        dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
        buttons: ["copy", "csv", "excel", "pdf", "print"]
      });
      setUpRowTableClick("pGenesTable", "popover-pGenes");
    },
    complete: function () {
      if (!dropped) {
        $.ajax({
          type: "GET",
          url: "/whereAreMyGenes",
          success: function (response) {
            var response = JSON.parse(response);
            if (response["whereGenes"] != "empty") {
              $("#whereGenes").html(response["whereGenes"]);
              $("#whereGenesTable").DataTable({
                scrollY: 600,
                scrollX: "100%",
                scrollCollapse: true,
                deferRender: true,
                scroller: true,
                select: true,
                dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
                buttons: ["copy", "csv", "excel", "pdf", "print"]
              });
              setUpRowTableClick("whereGenesTable", "popover-pGenes");
            } else {
              $("#whereGenes").html(
                "All genes are to be found in the analysis."
              );
            }
            if (response["droppedSymbols"] != "empty") {
              $("#droppedSymbols").html(response["droppedSymbols"]);
              var table = $("#droppedSymbolsTable").DataTable({
                scrollY: 600,
                scrollX: "100%",
                scrollCollapse: true,
                deferRender: true,
                scroller: true,
                select: true,
                dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
                buttons: ["copy", "csv", "excel", "pdf", "print"]
              });
              editDroppedSymbols(table, "#droppedSymbolsTable");
            } else {
              $("#droppedSymbols").html(
                "All genes Symbols have been matched. :)"
              );
            }
          }
        });
      }
    }
  });
}
function useCatSet() {
  var chosenCategory = document
    .getElementById("cats")
    .childNodes[0].getAttribute("value");
  var checkBoxes = $("#cat-relations").children();
  var checkedRels = new Array();
  checkBoxes.each(function () {
    if (
      $(this)
        .children()
        .children()
        .is(":checked")
    ) {
      checkedRels.push(
        $(this)
          .children()
          .children()
          .val()
      );
    }
  });

  var toPython = new Object();
  toPython.chosenCategory = chosenCategory;
  toPython.regTypes = checkedRels;
  $.ajax({
    type: "POST",
    url: "/useCategory",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      if ("catSetTable" in response) {
        updateCatSetTable(response);
      }
    }
  });
}

/**
 * Gets active or inactive state of all tabs.
 * Returns an object with a bool for all tabs.
 < */
function getActiveTabs() {
  var tabs = {
    spTab: false,
    fileTab: false,
    runTab: false,
    userTab: false,
    vennTab: false,
    plotTab: false,
    resTab: false,
    geneTab: false,
    heatTab: false,
    histTab: false,
    colTab: false
  };
  var tabID = $('*[data-value="Categories"]');
  tabs.spTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Files"]');
  tabs.fileTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Run"]');
  tabs.runTab = tabID.hasClass("active");
  var tabID = $('*[data-value="User"]');
  tabs.userTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Venn"]');
  tabs.vennTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Plot"]');
  tabs.plotTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Enrichment results"]');
  tabs.resTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Genes"]');
  tabs.geneTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Heat map"]');
  tabs.heatTab = tabID.hasClass("active");
  var tabID = $('*[data-value="History"]');
  tabs.histTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Collection"]');
  tabs.colTab = tabID.hasClass("active");
  var tabID = $('*[data-value="Category set"]');
  tabs.colTab = tabID.hasClass("active");
  return tabs;
}

/**
 * Makes mutually exclusive switches.
 * @param {*} divId Parent div that contains an enumeration of switches.
 * @param {*} numSwitches If we have 2 switches we want one always to be checked.
 */
function mutuallyExclSwitch(divId, numSwitches) {
  var ident = "#" + divId;
  $(ident + " .material-switch .material-switch-input").click(function () {
    var checkedState = $(this).prop("checked");
    var switchers = $(this)
      .parent(".material-switch")
      .parent(".switch")
      .parent(ident)
      .children(".switch")
      .children(".material-switch");

    if (numSwitches != 2) {
      switchers
        .children(".material-switch-input:checked")
        .prop("checked", false);
      $(this).prop("checked", true);
    } else {
      switchers
        .children(".material-switch-input")
        .prop("checked", !checkedState);
      $(this).prop("checked", checkedState);
    }
  });
}

/**
 * Send switch status to python of significant genes or continous measure
 * in order to know what a good uploaded file is.
 */
function sendSignifContStatus() {
  if ($("#sig").is(":checked")) {
    inputType = "sig";
    $("#fg").prop("multiple", true);
  } else {
    inputType = "cm";
    $("#fg").prop("multiple", false);
  }
  var toPython = new Object();
  toPython.inputType = inputType;
  $.ajax({
    type: "POST",
    url: "/inputType",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8"
  });
  // if Fisher test, then render options
  if (inputType == "sig") {
    $("#fisher-alt").show();
    mutuallyExclSwitch("alternative", 2);
  } else {
    $("#fisher-alt").hide();
  }
}

function successStartAnaCat(response) {
  var response = JSON.parse(response);
  // Add name of analysis to bread crumbs field
  // Draw Enrichment Result plot
  $("#plot").html(response["plot"]);
  // Move to tab plot
  var nextId = $("#maintabs-content")
    .children(".tab-pane")
    .next()
    .attr("id");
  $("[href=\\#" + nextId + "]").tab("show");

  // Draw Enrichment Result table

  $("#output").html(response["enrich_table"]);
  $("#enrichTable").DataTable({
    scrollY: 600,
    scrollX: "100%",
    scrollCollapse: true,
    deferRender: true,
    scroller: true,
    select: true,
    dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
    buttons: ["copy", "csv", "excel", "pdf", "print"]
  });

  $(".loading-overlay").fadeOut("fast");
  setUpRowTableClick("enrichTable", "popover-buttons");
  // make plotly plot react on hover by emphasising traces
  setUpPlotlyExt();
}

function completeStartAnaCat() {
  // Render the genes table while user looks at plot.
  var toPython = new Object();
  toPython.where = "Category";
  $.ajax({
    type: "POST",
    url: "/getGenesTable",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      // Render the history table
      reloadHistory();

      var response = JSON.parse(response);
      $("#genes").html(response["genes_table"]);
      var table = $("#genesTable").DataTable({
        scrollY: 600,
        scrollX: "100%",
        scrollCollapse: true,
        deferRender: true,
        scroller: true,
        select: true,
        dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
        buttons: ["copy", "csv", "excel", "pdf", "print"]
      });
      // Make the GO_ID column invisible
      table.column(3).visible(false);
      setUpRowTableClick("genesTable", "popover-genes");
    }
  });
}

/**
 * Loads and reloads the history
 */
function reloadHistory() {
  $.ajax({
    type: "POST",
    url: "/reloadHistory",
    success: function (response) {
      var response = JSON.parse(response);
      // Draw history table
      $("#history").html(response["history_table"]);
      historyTable = $("#historyTable").DataTable({
        paging: false,
        select: true,
        dom: '<"top"f>rt<"bottom"ilp><"clear">'
      });
      $("#history_message").remove();
      setUpRowTableClick("historyTable", "popover-history");
    }
  });
}

function findPairwiseInter() {
  var species = new Array();
  var toCompare = new Array();
  var rows = $("#historyTable")
    .children("tbody")
    .children("tr.selected");
  for (var i = 0; i < rows.length; i++) {
    species.push(
      rows
        .eq(i)
        .children("td")
        .eq(1)
        .html()
    );
    toCompare.push(
      rows
        .eq(i)
        .children("td")
        .eq(0)
        .children("span")
        .data("value")
    );
  }
  if (species[0] != species[1]) {
    swal({
      title: "Error!",
      text:
        "Both analyses must be of the same species for pairwise interactions.",
      type: "error",
      confirmButtonClass: "btn-default btn-primary",
      confirmButtonText: "OK"
    });
  } else {
    $(".loading-overlay").fadeIn("fast");
    $("#interactions-overlay").fadeIn("fast");
    var toPython = new Object();
    toPython.toCompare = toCompare;
    $.ajax({
      type: "POST",
      url: "/findPairWiseInter",
      data: JSON.stringify(toPython, null, "\t"),
      contentType: "application/json;charset=UTF-8",
      success: function (response) {
        var response = JSON.parse(response);
        $("#interactions").html(response["interactions"]);
        $("#interactionsTable").DataTable({
          scrollY: 600,
          scrollX: "100%",
          scrollCollapse: true,
          deferRender: true,
          scroller: true,
          select: true,
          columns: [null, { width: "20px" }, null, null, null, null],
          dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
          buttons: ["copy", "csv", "excel", "pdf", "print"]
        });
        $(".loading-overlay").fadeOut("fast");
      }
    });
  }
}

function deleteFromHistory(selected = false) {
  closePopover("historyTable", "popover-history-multipl");
  var toDelete = new Array();
  if (selected) {
    var rows = $("#historyTable")
      .children("tbody")
      .children("tr.selected");
  } else {
    var rows = $("#historyTable")
      .children("tbody")
      .children("tr");
  }
  for (var i = 0; i < rows.length; i++) {
    toDelete.push(
      rows
        .eq(i)
        .children("td")
        .eq(0)
        .children("span")
        .data("value")
    );
  }

  var toPython = new Object();
  toPython.toDelete = toDelete;
  $.ajax({
    type: "POST",
    url: "/deleteFromHistory",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8"
  });
}

function reloadHeatmap() {
  if ($("#p").is(":checked")) {
    var inputType = "p";
  } else if ($("#tf").is(":checked")) {
    var inputType = "tf";
  } else {
    var inputType = "dr";
  }
  var toPython = new Object();
  toPython.inputType = inputType;
  if ($("#iso").is(":checked")) {
    var iso = "iso";
  } else {
    var iso = "all";
  }
  toPython.iso = iso;
  toPython.checkedClust = new Array();
  var clustCheckboxes = $(".material-switch-input.clust:checkbox:checked");
  for (var i = 0; i < clustCheckboxes.length; i++) {
    toPython.checkedClust.push(clustCheckboxes[i].value);
  }
  toPython.cutoff = $("#heatCutoff").val();

  if ($("#n").is(":checked")) {
    var scaling = "none";
  } else if ($("#r").is(":checked")) {
    var scaling = "rows";
  } else {
    var scaling = "columns";
  }
  toPython.scaling = scaling;
  $.ajax({
    type: "POST",
    url: "/reloadHeatmap",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      // Draw heatmap
      $("#heatmap").html(response["plot"]);
      $("#heatmap_message").remove();
    }
  });
}

function addGeneToColl(tableId, pop_id, fromWhere) {
  $(document).off("click", "#body"); // unbind clicks from previous datatables
  $("#genecollect-overlay").fadeIn();
  $("#genecollName").select();
  var table = $("#" + tableId).DataTable();
  $(document).bind("keypress.key13", function (event) {
    // allow confirm on enter too
    if (event.which == 13) {
      $("#confirmgenecoll").click();
    }
  });
  $("#confirmgenecoll").on("click", function () {
    $("#collection_message").hide();
    var selGenes = $("#" + tableId + " tr.selected");
    var collData1, collData2;
    var analysisName = $("#setsName").val() + " > " + $("#genecollName").val();
    if (tableId != "enrichTable") {
      for (var i = 0; i < selGenes.length; i++) {
        var row = selGenes.eq(i).children();
        collData1 = row
          .eq(0)
          .children()
          .html();
        collData2 = row.eq(1).html();
        collection.row.add([collData1, collData2, analysisName]).draw(false);
        $("#showNet").attr("disabled", false);
      }
    } else {
      var toPython = new Object();
      toPython.goids = new Array();
      for (var i = 0; i < selGenes.length; i++) {
        var row = selGenes.eq(i).children();
        collData1 = row
          .eq(0)
          .children()
          .html();
        toPython.goids.push(collData1);
      }
      $.ajax({
        type: "POST",
        url: "/getGenesForTerm",
        data: JSON.stringify(toPython, null, "\t"),
        contentType: "application/json;charset=UTF-8",
        success: function (response) {
          var response = JSON.parse(response);
          var genes = response["genes"];
          var symbols = response["symbols"];
          for (var i = 0; i < genes.length; i++) {
            collection.row
              .add([genes[i], symbols[i], analysisName])
              .draw(false);
            $("#showNet").attr("disabled", false);
          }
        }
      });
    }
    closePopover(tableId, pop_id);
    table.rows().deselect();
    $("#genecollect-overlay").fadeOut();
  });
  $("#cancelgenecoll").on("click", function () {
    closePopover(tableId, pop_id);
    table.rows().deselect();
    $("#genecollect-overlay").fadeOut();
  });
}

$(document).on("click", "a[data-value='Heat map']", function () {
  if ($("#3d").is(":checked")) {
    make3dHeatmap();
    $("#heatScale").fadeIn("fast", function () {
      $("#heatCutSlider").fadeIn("fast");
    });
    $("#hmM").fadeOut("fast", function () {
      $("#3dhmM").fadeIn("fast");
    });
  } else {
    reloadHeatmap();
    if ($("#p").is(":checked")) {
      $("#heatScale").fadeOut("fast", function () {
        $("#heatCutSlider").fadeIn("fast");
      });
    } else {
      $("#heatCutSlider").fadeOut("fast", function () {
        $("#heatScale").fadeIn("fast");
      });
    }
    $("#3dhmM").fadeOut("fast", function () {
      $("#hmM").fadeIn("fast");
    });
  }
});

$(document).on("change", "#heatMeasure input[type=checkbox]", function () {
  reloadHeatmap();
  if ($("#p").is(":checked")) {
    $("#heatScale").fadeOut("fast", function () {
      $("#heatCutSlider").fadeIn("fast");
    });
  } else {
    $("#heatCutSlider").fadeOut("fast", function () {
      $("#heatScale").fadeIn("fast");
    });
  }
});

$(document).on("change", "#3dheatMeasure input[type=checkbox]", function () {
  make3dHeatmap();
});

$(document).on("change", "#heatChoice input[type=checkbox]", function () {
  if ($("#3d").is(":checked")) {
    make3dHeatmap();
    $("#hmM").fadeOut("fast", function () {
      $("#3dhmM").fadeIn("fast");
    });
    $("#heatScale").fadeIn("fast", function () {
      $("#heatCutSlider").fadeIn("fast");
    });
  } else {
    reloadHeatmap();
    $("#3dhmM").fadeOut("fast", function () {
      $("#hmM").fadeIn("fast");
    });
    if ($("#p").is(":checked")) {
      $("#heatScale").fadeOut("fast", function () {
        $("#heatCutSlider").fadeIn("fast");
      });
    } else {
      $("#heatCutSlider").fadeOut("fast", function () {
        $("#heatScale").fadeIn("fast");
      });
    }
  }
});

$(document).on("change", "#heatCutSlider input[type=range]", function () {
  if ($("#3d").is(":checked")) {
    make3dHeatmap();
  } else {
    reloadHeatmap();
  }
});

$(document).on("change", "#heatScale input[type=checkbox]", function () {
  if ($("#3d").is(":checked")) {
    make3dHeatmap();
  } else {
    reloadHeatmap();
  }
});

$(document).on("change", "#heatIsolation input[type=checkbox]", function () {
  if ($("#3d").is(":checked")) {
    make3dHeatmap();
  } else {
    reloadHeatmap();
  }
});

$(document).on("change", "#heatDendro input[type=checkbox]", function () {
  if ($("#3d").is(":checked")) {
    make3dHeatmap();
  } else {
    reloadHeatmap();
  }
});

$(document).on("click", "#showHeatmapSorting", function () {
  if ($("#p").is(":checked")) {
    var inputType = "p";
  } else if ($("#tf").is(":checked")) {
    var inputType = "tf";
  } else {
    var inputType = "dr";
  }
  var toPython = new Object();
  toPython.inputType = inputType;
  if ($("#iso").is(":checked")) {
    var iso = "iso";
  } else {
    var iso = "all";
  }
  toPython.iso = iso;

  $.ajax({
    type: "POST",
    url: "/getNamesForHeatmapSorting",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      $("body").append(response["overlay"]);
      $("#heatSortingOverlay").fadeIn("fast");
      $("#heatSortBox").sortable({
        axis: "y",
        revert: true,
        scroll: false,
        placeholder: "sortable-placeholder",
        cursor: "move"
      });
    }
  });
});

$(document).on("click", "#applySorting", function () {
  var sortedElements = $(".heat-sort-info")
    .map(function () {
      return $(this).data("value");
    })
    .get();
  var toPython = new Object();
  toPython.sorting = sortedElements;

  if ($("#iso").is(":checked")) {
    var sortUrl = "/sortHeatmap/iso";
  } else {
    var sortUrl = "/sortHeatmap";
  }

  $.ajax({
    type: "POST",
    url: sortUrl,
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      if ($("#3d").is(":checked")) {
        make3dHeatmap();
      } else {
        reloadHeatmap();
      }
      $("#heatSortingOverlay").fadeOut("fast", function () {
        $(this).remove();
      });
    }
  });
});

$(document).on("click", "#cancelSorting", function () {
  $("#heatSortingOverlay").fadeOut("fast", function () {
    $(this).remove();
  });
});

// make dataframe table header look fine
$(document).on("click", "a[data-value='Enrichment results']", function () {
  $("#enrichTable")
    .DataTable()
    .columns.adjust();
});

$(document).on("click", "a[data-value='Genes']", function () {
  $("#genesTable")
    .DataTable()
    .columns.adjust();
  $("#pGenesTable")
    .DataTable()
    .columns.adjust();
});

$(document).on("click", "a[data-value='Collection']", function () {
  $("#collection_table")
    .DataTable()
    .columns.adjust();
});

$(document).on("click", "a[data-value='Category set']", function () {
  $("#catSetTable")
    .DataTable()
    .columns.adjust();
});

$(document).on("click", "#showRegister", function () {
  $("#register-overlay").fadeIn("slow");
});

$(document).on("click", "#cancelreg", function () {
  $("#register-overlay").fadeOut("fast");
});

$(document).on("click", "#showLogin", function () {
  $("#login-overlay").fadeIn("slow");
});

$(document).on("click", "#cancel", function () {
  $("#login-overlay").fadeOut("fast");
});

// $(document).ready(function(){
//   $('#regPassword1').keyup(function(ev) {
//     console.log('?');
//   });
//   $('#regPassword1').blur(function() {
//     console.log('focus left');
//   });
// });

var regPassLength = 3;
var regPassCheck1 = false;
var regPassCheck2 = false;

$(document).on("keyup", "#regPassword1", function (ev) {
  if ($("#regPassword1").val().length >= regPassLength) {
    $("#pass1Message").text("Okay");
    $("#pass1Message").css({ color: "#4daf4a" });
    $("#pass1Message").fadeIn("fast");
    $("#pass1NoCheck").fadeOut("fast", function () {
      $("#pass1Check").fadeIn("fast");
    });
    regPassCheck1 = true;
  } else {
    $("#pass1Message").text("Too short");
    $("#pass1Message").css({ color: "#e41a1c" });
    $("#pass1Message").fadeIn("fast");
    $("#pass1Check").fadeOut("fast", function () {
      $("#pass1NoCheck").fadeIn("fast");
    });
    regPassCheck1 = false;
  }
});

$(document).on("keyup", "#regPassword2", function (ev) {
  if ($("#regPassword1").val() == $("#regPassword2").val()) {
    $("#pass2Message").text("Okay");
    $("#pass2Message").css({ color: "#4daf4a" });
    $("#pass2Message").fadeIn("fast");
    $("#pass2NoCheck").fadeOut("fast", function () {
      $("#pass2Check").fadeIn("fast");
    });
    regPassCheck2 = true;
  } else {
    $("#pass2Message").text("Passwords not matching");
    $("#pass2Message").css({ color: "#e41a1c" });
    $("#pass2Message").fadeIn("fast");
    $("#pass2Check").fadeOut("fast", function () {
      $("#pass2NoCheck").fadeIn("fast");
    });
    regPassCheck2 = false;
  }
});

$(document).on("click", "#signup", function () {
  if (regPassCheck1 && regPassCheck2) {
    var pass = $("#regPassword1").val();
    var toPython = new Object();
    toPython.pass = pass;
    toPython.name = $("#regUsername").val();
    toPython.mail = $("#regMail").val();
    $.ajax({
      type: "POST",
      url: "/registerUser",
      data: JSON.stringify(toPython, null, "\t"),
      contentType: "application/json;charset=UTF-8",
      success: function (response) {
        var response = JSON.parse(response);
        if (response["result"] == "error") {
          swal({
            title: "Error!",
            text: response["message"],
            type: "error",
            confirmButtonClass: "btn-default btn-primary",
            confirmButtonText: "OK"
          });
        } else {
          swal("", response["message"], "success");
          $("#register-overlay").fadeOut("fast");
          $("#showRegister").fadeOut("fast");
          $("#showLogin").fadeOut("fast");
          $("#logout").fadeIn("fast");
          $("#greetingName").text(response["user"]);
          $("#userFunctions").fadeIn("fast");
        }
      }
    });
  }
});

$(document).on("click", "#verify", function () {
  var toPython = new Object();
  toPython.pass = $("#password").val();
  toPython.name = $("#username").val();
  $.ajax({
    type: "POST",
    url: "/loginUser",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      if (response["result"] == "error") {
        swal({
          title: "Error!",
          text: response["message"],
          type: "error",
          confirmButtonClass: "btn-default btn-primary",
          confirmButtonText: "OK"
        });
      } else {
        swal("", response["message"], "success");
        $("#login-overlay").fadeOut("fast");
        $("#showRegister").fadeOut("fast");
        $("#showLogin").fadeOut("fast");
        $("#logout").fadeIn("fast");
        $("#greetingName").text(response["user"]);
        $("#userFunctions").fadeIn("fast");
      }
    }
  });
});

$(document).on("click", "#logout", function () {
  $.ajax({
    type: "POST",
    url: "/logoutUser"
  });
  $("#showRegister").fadeIn("fast");
  $("#showLogin").fadeIn("fast");
  $("#logout").fadeOut("fast");
  $("#greetingName").text("");
  $("#userFunctions").fadeOut("fast");
});

$(document).on("click", "#saveSession", function () {
  $.ajax({
    type: "POST",
    url: "/saveSession",
    success: function (response) { }
  });
});

$(document).ready(function () {
  $(".horizontal-controls").scroll(function () {
    if ($(this).scrollLeft() > 0) {
      if (!$(this).hasClass("right")) {
        $(this).addClass("right");
      }
    } else {
      $(this).removeClass("right");
    }
    if ($(this).scrollLeft() < $(this)[0].scrollWidth - $(this).width()) {
      if (!$(this).hasClass("left")) {
        $(this).addClass("left");
      }
    } else {
      $(this).removeClass("left");
    }
  });
});
