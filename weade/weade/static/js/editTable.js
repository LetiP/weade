var opts = new Array();
var currCatOpts = new Array();

function editTable(table, tableId) {
  $(tableId + " tbody").on("click", "td", function (event) {
    if (!$(tableId).hasClass("editing")) {
      // find out which column was clicked on
      var idx = table.cell(this).index().column;
      var clickedColumn = $(table.column(idx).header()).text();

      if (clickedColumn == "Category") {
        editCat = true;
        var currCatOpts = new Array();
        var listCatColumn = $.unique(
          catSetTable
            .column(3)
            .data()
            .draw("page")
        );
        $.each($(listCatColumn), function (i, value) {
          currCatOpts.push({ name: value });
        });
        opts = currCatOpts;
      } else {
        editCat = false;
        opts = new Array();
      }

      if (clickedColumn != "Ontology") {
        $(tableId).addClass("editing");
        thisLocation = $(this);
        thisText = thisLocation.text();

        thisLocation.empty().append(
          $("<select></select>", {
            id: "editing",
            class: "changeLocation"
          })
        );
        selectizeIt(
          table,
          tableId,
          "#editing",
          thisLocation,
          thisText,
          clickedColumn,
          opts
        );
        editSel[0].selectize.focus();
      }
    } else if ($(event.target).is("td")) {
      $(tableId).removeClass("editing");
      thisLocation.text(thisText);
    }
  });
}

function selectizeIt(
  table,
  tableId,
  selId,
  thisLocation,
  oldText,
  clickedColumn,
  opts
) {
  editSel = $(selId).selectize({
    persist: false,
    maxItems: "1",
    valueField: "name",
    labelField: "name",
    searchField: ["name"],
    placeholder: oldText,
    options: opts,
    create: editCat,
    onChange: function (value) {
      $(tableId).removeClass("editing");
      // thisLocation.text(value);
      var row = table.row(thisLocation.closest("tr"));
      var oldData = row.data();

      if (clickedColumn != "Category") {
        // look up the corresponding go_id/name
        var toPython = new Object();
        toPython.value = value;
        toPython.clickedColumn = clickedColumn;
        $.ajax({
          type: "POST",
          url: "/getCorrespondingGoidNameOnto",
          data: JSON.stringify(toPython, null, "\t"),
          contentType: "application/json;charset=UTF-8",
          success: function (response) {
            var response = JSON.parse(response);
            var sugg = response["suggestion"];
            var selector = thisLocation.parent().children();
            if (clickedColumn == "GO_ID") {
              row
                .data([value, response["onto"], sugg, oldData[3]])
                .draw("page");
            } else if (clickedColumn == "Name") {
              row
                .data([sugg, response["onto"], value, oldData[3]])
                .draw("page");
            }
          }
        });
      } else {
        oldData[3] = value;
        row.data(oldData).draw("page"); // new Data actually
      }
      $("#apply_cats_edit").prop("disabled", false);
    },
    load: function (typed, callback) {
      if (clickedColumn == "GO_ID" || clickedColumn == "Name") {
        var toPython = new Object();
        toPython.typed = typed;
        toPython.clickedColumn = clickedColumn;
        $.ajax({
          type: "POST",
          url: "/getTableEditInfos",
          data: JSON.stringify(toPython, null, "\t"),
          contentType: "application/json;charset=UTF-8",
          success: function (response) {
            var response = JSON.parse(response);

            // bring in the reuired json format
            var result = new Array();
            $.each($(response["suggestion"]), function (i, value) {
              result.push({ name: value });
            });
            callback(result);
          }
        });
      }
    }
  });
}

function editDroppedSymbols(table, tableId) {
  $(tableId + " tbody").on("click", "td", function (event) {
    if (!$(tableId).hasClass("editing")) {
      $(tableId).addClass("editing");
      thisLocation = $(this);
      thisText = thisLocation.text();

      thisLocation.empty().append(
        $("<select></select>", {
          id: "editing",
          class: "changeLocation"
        })
      );
      selectizeDropped(table, tableId, "#editing", thisLocation, thisText);
      editSel[0].selectize.focus();
    } else if ($(event.target).is("td")) {
      $(tableId).removeClass("editing");
      thisLocation.text(thisText);
    }
  });
}

function selectizeDropped(table, tableId, selId, thisLocation, oldText) {
  editSel = $(selId).selectize({
    persist: false,
    maxItems: "1",
    valueField: "name",
    labelField: "name",
    searchField: ["name"],
    placeholder: oldText,
    onChange: function (value) {
      $(tableId).removeClass("editing");
      var row = table.row(thisLocation.closest("tr"));

      // if changed, then add symbol to custom
      var toPython = new Object();
      toPython.value = value;
      var oldData = row.data();
      if (oldData.length == 2) {
        toPython.measure = oldData[1];
      }
      $.ajax({
        type: "POST",
        url: "/addDroppedRecovered",
        data: JSON.stringify(toPython, null, "\t"),
        contentType: "application/json;charset=UTF-8",
        success: function () {
          row.remove().draw(false);
          applySetsComplete(true);
          swal({
            title: "Success!",
            text:
              "The gene symbol " +
              value +
              " has been added to the applied set.",
            type: "success",
            confirmButtonClass: "btn-default btn-primary",
            confirmButtonText: "OK"
          });
        }
      });
    },
    load: function (typed, callback) {
      var toPython = new Object();
      toPython.typed = typed;
      $.ajax({
        type: "POST",
        url: "/getDroppedInfo",
        data: JSON.stringify(toPython, null, "\t"),
        contentType: "application/json;charset=UTF-8",
        success: function (response) {
          var response = JSON.parse(response);
          // bring in the required json format
          var result = new Array();
          $.each($(response["suggestion"]), function (i, value) {
            result.push({ name: value });
          });
          callback(result);
        }
      });
    }
  });
}
