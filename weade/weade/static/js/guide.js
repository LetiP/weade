var reviewer = false;

var tipTexts = [
    "The history tab shows the analyses that you already performed.",
    "You can rerun a previous analysis from the history tab.",
    "Remove an analysis from the history to remove it permanently from the heat map.",
    "You can look at a subset of your analyses in the heat map by selecting them in the history tab and clicking on 'Isolate in heat map'.",
    "Switch between the isolated analyses and all analyses by toggling the corresponding radio buttons in the side bar.",
    "New to the app? Take the guided tour or have a look at the hints (top right corner).",
    "You can filter the gene table by searching for a gene or term and download the result in text format.",
    "Useful tips are displayed while you wait for the app to load.",
    "You can compare analyses from different data sets in the heat map.",
    "It is even possible to compare analyses from different species in the heat map.",
    "You can rearrange the columns in the heat map, by moving the entries in the history and then selecting custom heat map sorting.",
    "Scaling the colors across columns or rows in the heat map might help, if very high values make it hard to see signatures.",
    "Adjust the cut off for the p-Value in the heat map, to focus on differences in the lower or higher p-Value ranges.",
    "You can download a subset of your genes by selecting segments in the Venn diagram and hitting the apply button and then the download button.",
    "The run tab contains the options for the heat map.",
    // "You can upload your own set of categories in the User tab.",
    "You can save your own set of categories for later use.",
    "Your saved category sets will appear in the Species tab alongside the general category sets.",
    "You can find the complete documentation under the following address: http://beta-weade.cos.uni-heidelberg.de/documentation"
];


var hintTexts = {
    species1: "Choose one of the available species here. The necessary gene identifier is given in parenthesis after the species name. It is possible to change the species later on, for a different data set and compare the results.",
    species2: "Choose the set of categories that you would like to analyse and click 'Use this category set'. Changing to a different category will reset the heat map.",
    species3: "If the app does not include your species yet, you can click here, to upload a custom background file.",
    files1: "If you have a list of significant genes, a fisher test will be performed to determine enrichment. If your data includes a continuous measure, like fold change or RPKM, a Mann-Whitney U-test can be can be used. You can upload the entire dataset in that case, not a thresholded subset.",
    files2: "Here you can upload up to four files at a time. If you selected continuous measure, they should include two comma separated columns: The first containing the gene identifier, the second containing the measurement. Otherwise, a simple list of gene identifiers, each in a new line is needed.",
    run1: "Here you can choose which of the three gene ontologies should be included in your analysis.",
    run2: "In SEA, you can choose to calculate either the enrichment (overrepresentation) or depletion (underrepresentation) of the category.",
    venn1: "Toggle these buttons to display and select either the foreground or background in the Venn diagram. The foreground will be displayed in green, the background in red.",
    venn2: "Choose a name for the analysis. This name will be displayed in the heat map, in the history, the collection and the interaction views.",
    venn3: "In this diagram you can see and select intersections between your uploaded data sets. You can select multiple segments by clicking on them sequentially. Click on an already selected segment to remove the selection.",
    venn4: "Upon pressing the apply sets button, the app will calculate the genes in your selection that also have GO annotations. After this, you can run the analysis by clicking the 'start analysis' button in the Run tab.",
    plot1: "This plot shows the results of the enrichment analysis. If you used a list of significant genes, the length of the bars is the normalized percentage of genes contributing to a category and present in your set. If you selected continuos measure, the length of the bars corresponds to the delta rank of the MWU test. The color of the plot shows the p-Value.",
    res1: "This is the tabular summary of the enrichment analysis. Click on the categories or terms to get access to further actions.",
    gene1: "This table shows you all genes in this analysis and their annotated category or GO term.",
    heat1: "With this button you can download the heat map data as a text file, e.g. to construct a heat map yourself with your preferred tool.",
    heat2: "Here, a heat map of the enrichment results of your analyses is displayed.",
    heat3: "You can switch the measure that is used to create the colors for the heat map. Term-frequency is only available for SEA, whereas delta rank is only available for GSEA results.",
    heat4: "By adjusting the cutoff, you can focus on differences in either the lower or higher p-Value ranges",
    heat5: "You can choose to display the z-score, to normalize the colors across colors or columns to either observe changes across samples or across categories.",
    heat6: "You can switch between including all analyses and a subset in the heat map. Choose the analyses that you want to isolate in the History tab.",
    hist1: "Here you see the analyses you have performed.",
    hist2: "Select one or multiple entries in the table above and click this button to remove them from the history and thereby also from the heat map.",
    hist3: "Select two or more entries from the table above and click this button to show a heat map containing only these analyses.",
    hist4: "Select one entry in the table above and click this button, to rerun the analysis. Make sure, the correct data sets are still loaded."
};

$(document).ready(function () {
    "use strict";
    // quick tip for loading animation
    // initial tip
    var tipText = tipTexts[getRandomInt(0, tipTexts.length)];
    $("#quickTip").text(tipText)
    // change tip every 7.5 seconds
    var tipInter = setInterval(function () {
        if($("#quickTip").is(":visible")) {
            $("#quickTip").fadeOut(250, function () {
                var tipText = tipTexts[getRandomInt(0, tipTexts.length)];
                $("#quickTip").text(tipText)
                $("#quickTip").fadeIn(250);
            });
        }
    }, 7500);

    // $("#revButton").click(function () {
    //     reviewer = true;
    //     $(".revTip").remove();
    //     var tabID = $('*[data-value="User"]');
    //     tabID.click();
    //     var popup = document.createElement('div');
    //     popup.className = "revTip";
    //     popup.innerText = "Log in here to access the data.";
    //     popup.style.width = "230px";
    //     $("#showLogin").append(popup);
    //     $("#showLogin").click(function () {
    //         if (reviewer) {
    //             var crLogin = document.createElement('div');
    //             crLogin.className = "revTip";
    //             crLogin.innerText = "damid17";
    //             crLogin.style.left = "50%"
    //             crLogin.style.bottom = "86%"
    //             crLogin.style.width = "80px"
    //             var crPwd = document.createElement('div');
    //             crPwd.className = "revTip";
    //             crPwd.innerText = "run";
    //             crPwd.style.left = "50%"
    //             crPwd.style.bottom = "49%"
    //             crPwd.style.width = "80px"
    //             $(".revTip").remove();
    //             setTimeout(function () {
    //                 $(".login-form").append(crLogin);
    //                 $(".login-form").append(crPwd);
    //             }, 200);
    //             $("#verify").click(function () {
    //                 crLogin.remove();
    //                 crPwd.remove();
    //                 var popup = document.createElement('div');
    //                 popup.className = "revTip";
    //                 popup.innerText = "Now choose the data sets here.";
    //                 popup.style.left = "50%"
    //                 $(".revTip").remove();
    //                 setTimeout(function () {
    //                     $("#userFunctions").append(popup);
    //                 }, 500);
    //                 $("#userFunctions").click(function () {
    //                     $(".revTip").remove();
    //                 });
    //             });
    //         }
    //     });
    // });

    // $("body").on("click", "#useUserSets", function () {
    //     reviewer = true;
    // });

    var hintsShown = false;

    // on click of the hint button, show relevant hints
    $("#hintButton").click(function () {
        if(hintsShown) {
            introJs().hideHints();
            introJs().removeHints();
            $('*[data-hint]').removeAttr("data-hint");
            hintsShown = false;
            $(this).removeClass("clicked");
        } else {
            // side tabs
            if(getActiveTabs().spTab) {
                $("#catSets").attr("data-hint", hintTexts.species2);
                introJs().addHints();
                introJs().showHints();
            } else if(getActiveTabs().fileTab) {
                $("#species").attr("data-hint", hintTexts.species1);
                $("#advSpOptions").attr("data-hint", hintTexts.species3);
                $("#type").attr("data-hint", hintTexts.files1);
                $("#fileInputBox").attr("data-hint", hintTexts.files2);
            } else if(getActiveTabs().runTab) {
                $("#ont").attr("data-hint", hintTexts.run1);
                if ($("#alternative").is(":visible")) {
                    $("#alternative").attr("data-hint", hintTexts.run2);
                }
            }
            // main tabs
            if(getActiveTabs().vennTab) {
                if($("#venntabdiv").is(":visible")) {
                    $("#bgfg").attr("data-hint", hintTexts.venn1);
                    $("#setNameInput").attr("data-hint", hintTexts.venn2);
                    $("#Venn").attr("data-hint", hintTexts.venn3);
                    $("#applysets").attr("data-hint", hintTexts.venn4)
                }
            } else if(getActiveTabs().plotTab) {
                if($("#plot").find('img').length !== 0) {
                    $("#plot").attr("data-hint", hintTexts.plot1);
                }
            } else if(getActiveTabs().resTab) {
                $("#enrichTable").attr("data-hint", hintTexts.res1);
            } else if(getActiveTabs().geneTab) {
                $("#genes").attr("data-hint", hintTexts.gene1);
            } else if(getActiveTabs().heatTab) {
                $("#downloadheat").attr("data-hint", hintTexts.heat1);
                $("#heat").attr("data-hint", hintTexts.heat2);
                if(getActiveTabs().runTab) {
                    $("#heatMeasure").attr("data-hint", hintTexts.heat3);
                    $("#heatIsoDiv").attr("data-hint", hintTexts.heat6);
                    if($("#heatCutSlider").find("div.form-group").is(":visible")) {
                        $("#heatCutSlider").attr("data-hint", hintTexts.heat4);
                    }
                    if($("#heatScale").is(":visible")) {
                        $("#heatScale").attr("data-hint", hintTexts.heat5);
                    }
                }
            } else if(getActiveTabs().histTab) {
                $("#history").attr("data-hint", hintTexts.hist1);
                $("#remove_history").attr("data-hint", hintTexts.hist2);
                $("#isolate_heatmap").attr("data-hint", hintTexts.hist3);
                $("#rerun_history").attr("data-hint", hintTexts.hist4);
            }
            introJs().removeHints();
            introJs().refresh();
            introJs().addHints();
            introJs().showHints();
            hintsShown = true;
            $(this).addClass("clicked");
        }
    });

    introJs.fn.onhintclose(function() {
        if($('.introjs-hints').find('a.introjs-hint:not(.introjs-hidehint)').length == 0) {
            $("#hintButton").removeClass("clicked");
            $('*[data-hint]').removeAttr("data-hint");
            introJs().removeHints();
            hintsShown = false;
        }
    });    

    // remove hints on tab change
    $('*[data-toggle="tab"]').click(function() {
        introJs().hideHints();
        $('*[data-hint]').removeAttr("data-hint");
        introJs().removeHints();
        introJs().refresh();
    });

    // variable to track whether the guide is active
    var tourStarted = false;

    // I use the introJs().goToStepNumber() function, because
    // the goToStep() function didn't seem to work as expected.
    // The caveat is, that the goToStepNumber() function seems
    // to exit the guide, therefore it has to be started again
    // and tourStarted needs to be set to true again.

    // a click on the start button starts the guide
    $("#startButton").click(function () {
        if (reviewer) {
            var tabID = $('*[data-value="Venn"]');
            tabID.click();
            introJs().goToStepNumber(6).start();
            tourStarted = true;
            // catchNextButton();
            // on click of apply sets button repeat until
            // process is complete and Run tab is shown
            $("#applysets").click(function () {
                if (tourStarted) {
                    var cont = false;
                    var inter = setInterval(function () {
                        // check if Run tab is active
                        cont = getActiveTabs().runTab;
                        if (cont) {
                            // stop repeat
                            clearInterval(inter);
                            // move to next step
                            introJs().goToStepNumber(7).start();
                            tourStarted = true;
                            // catchNextButton();
                        }
                    }, 500);
                }
            });
        } else {
            var tabID = $('*[data-value="Categories"]');
            tabID.click();
            $(this).addClass("clicked");
            var tabID = $('*[data-value="Venn"]');
            tabID.click();
            introJs().start();
            tourStarted = true;
            var nExists = $("#applysets").is(":visible");
            if (nExists) {
                var tabID = $('*[data-value="Files"]');
                tabID.click();
                introJs().goToStepNumber(5).start();
                tourStarted = true;
                $("#setsName").val('Example Analysis');
            }
        }
    });

    // on exit of the guide, set tourStarted to false
    introJs.fn.onexit(function () {
        tourStarted = false;
        $("#startButton").removeClass("clicked");
    });

    $("#useExampleData").click(function () {
        var inter = setInterval(function () {
            if (tourStarted) {
                var nExists = $("#applysets").is(":visible");
                if (nExists) {
                    // move to next step
                    introJs().goToStepNumber(5).start();
                    tourStarted = true;
                    $("#setsName").val('Example Analysis');
                    clearInterval(inter);
                }
            }
        }, 500);
    });

    $("#applysets").click(function () {
        var tabID = $('*[data-value="Run"]');
        tabID.click();
        setTimeout(function () {
            if (tourStarted) {
                // move to next step
                introJs().goToStepNumber(6).start();
                tourStarted = true;
            }
        }, 500);
    });

    // on click move to last step
    $("#start").click(function () {
        if (tourStarted) {
            introJs().goToStepNumber(8).start();
            tourStarted = true;
        }
    });

    $(document).on('click', ".introjs-nextbutton", function () {
        var activeBullet = $(".introjs-bullets")
            .find(".active")
            .data("stepnumber");
        if (activeBullet === 1) {
            var tabID = $('*[data-value="Files"]');
            tabID.click();
            introJs().goToStepNumber(2).start();
            tourStarted = true;
        } else if (activeBullet === 4) {
            var nExists = $("#applysets").is(":visible");
            if (!nExists) {
                introJs().goToStepNumber(4).start();
                tourStarted = true;
            } else {
                introJs().goToStepNumber(5).start();
                tourStarted = true;
                $("#setsName").val('Example Analysis');
            }
        } else if (activeBullet === 5) {
            if ($("#setsName").val().length != 0) {
                var tabID = $('*[data-value="Run"]');
                tabID.click();
                $("#applysets").click();
                tourStarted = true;
            } else {
                introJs().goToStepNumber(5).start();
                tourStarted = true;
            }
        } else if (activeBullet === 7 && !$('#start').is(":disabled")) {
            $("#start").click();
        } else if (activeBullet === 10) {
            var tabID = $('*[data-value="Enrichment results"]');
            tabID.click();
            setTimeout(function() {
                introJs().goToStepNumber(11).start();
            }, 200);
        } else if (activeBullet === 11) {
            var tabID = $('*[data-value="Genes"]');
            tabID.click();
            setTimeout(function() {
                introJs().goToStepNumber(12).start();
            }, 200);
        } else if (activeBullet === 12) {
            var tabID = $('*[data-value="Heat map"]');
            tabID.click();
            setTimeout(function() {
                introJs().goToStepNumber(13).start();
            }, 200);
        } else if (activeBullet === 13) {
            var tabID = $('*[data-value="History"]');
            tabID.click();
            setTimeout(function() {
                introJs().goToStepNumber(14).start();
            }, 200);
        } else if (activeBullet === 14) {
            var tabID = $('*[data-value="Collection"]');
            tabID.click();
            setTimeout(function() {
                introJs().goToStepNumber(15).start();
            }, 200);
        } else if (activeBullet === 15) {
            var tabID = $('*[data-value="Category set"]');
            tabID.click();
            setTimeout(function() {
                introJs().goToStepNumber(16).start();
            }, 200);
        }
    });

    /**
   * Gets active or inactive state of all tabs.
   * Returns an object with a bool for all tabs.
   * @return {Object}
   */
    function getActiveTabs() {
        var tabs = {
            spTab: false,
            fileTab: false,
            runTab: false,
            userTab: false,
            vennTab: false,
            plotTab: false,
            resTab: false,
            geneTab: false,
            heatTab: false,
            histTab: false,
            colTab: false
        };
        var tabID = $('*[data-value="Categories"]');
        tabs.spTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Files"]');
        tabs.fileTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Run"]');
        tabs.runTab = tabID.hasClass("active");
        var tabID = $('*[data-value="User"]');
        tabs.userTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Venn"]');
        tabs.vennTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Plot"]');
        tabs.plotTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Enrichment results"]');
        tabs.resTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Genes"]');
        tabs.geneTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Heat map"]');
        tabs.heatTab = tabID.hasClass("active");
        var tabID = $('*[data-value="History"]');
        tabs.histTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Collection"]');
        tabs.colTab = tabID.hasClass("active");
        var tabID = $('*[data-value="Category set"]');
        tabs.colTab = tabID.hasClass("active");
        return tabs;
    }

    /**
     * Returns a random integer between min (inclusive) and max (exclusive)
     * @param {Number} min
     * @param {Number} max
     * @return {Number}
     */
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

});
