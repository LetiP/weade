var myPlot = {
  init: function() {
    this.plot = document.getElementsByClassName("plotly-graph-div")[0];
    this.d3 = Plotly.d3;
    this.numPerTrace = new Array();

    for (var i = 0; i < this.plot.data.length; i++) {
      this.numPerTrace[i] = this.plot.data[i].x.length;
    }
  }
};

function setUpPlotlyExt() {
  myPlot.init();
  plotToLegend();
  // legendToPlot();
}

function plotToLegend() {
  myPlot.plot
    .on("plotly_hover", function(data) {
      traceNum = data.points[0].curveNumber;
      emphLegend(traceNum, "bold");
    })
    .on("plotly_unhover", function(data) {
      traceNum = data.points[0].curveNumber;
      emphLegend(traceNum);
    });
}

function legendToPlot() {
  myPlot.d3
    .select("g.legend")
    .selectAll(".traces")
    .each(function() {
      var item = myPlot.d3.select(this);
      item.on("mouseover", function(d) {
        var trace = d[0].trace;
        var traceNum = trace.index;
        var vis = trace.visible;
        emphAnno(traceNum, "black", "14");
        emphLegend(traceNum, "bold");
      });
      item.on("mouseout", function(d) {
        var trace = d[0].trace;
        var traceNum = trace.index;
        var vis = trace.visible;
        emphAnno(traceNum, 'auto', 'auto');
        emphLegend(traceNum);
      });
    });
}

function emphAnno(
  traceNum,
  what1 = "auto",
  what2 = "auto",
  plotPos = null
) {
    for (var i = 0; i < myPlot.numPerTrace[traceNum]; i++) {
      var bla = traceNum + i;
      var anno = "annotations[" + bla + "].font.color";
      Plotly.relayout(myPlot.plot, anno, what1);
      var anno = "annotations[" + bla + "].font.size";
      Plotly.relayout(myPlot.plot, anno, what2);
    }
}

function emphLegend(traceNum, what = "normal") {
  $("g.traces")
    .children("text")
    .eq(myPlot.plot.data.length - traceNum - 1)
    .css("font-weight", what);
}
