function drawOntoTree(tableid) {
  selected = $("#" + tableid + " tr.selected");
  if (tableid == 'catSetTable') {
    var selectedId = selected
      .children()
      .eq(0)
      .html();
  } else {
    var selectedId = selected
      .children()
      .eq(0)
      .children()
      .html();
  }

  var toPython = new Object();
  toPython.goid = selectedId;
  $.ajax({
    type: "POST",
    url: "/showOntoTree",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      nodes = response["nodes"];
      edges = response["edges"];
      var hideLabel = function (values, id, selected, hovering) {
        values.vadjust = 0;
      };
      var selectNode = function (values, id, selected, hovering) {
        values.borderWidth = 2;
        values.shadow = true;
        values.size = 15;
      };
      var selectEdge = function (values, id, selected, hovering) {
        values.inheritsColor = "both";
        values.shadow = true;
        values.width = 2.8;
        values.toArrowScale = 0.5;
      };
      var nodes = new vis.DataSet(nodes);
      var edges = new vis.DataSet(edges);
      // create a network
      var container = document.getElementById("ontotree");
      var data = {
        nodes: nodes,
        edges: edges
      };
      var options = {
        nodes: {
          shape: "box",
          margin: 10,
          widthConstraint: 80,
          labelHighlightBold: false,
          color: {
            border: "#e95420",
            background: "white"
          },
          chosen: {
            node: selectNode
          }
        },
        physics: {
          enabled: false
        },
        layout: {
          hierarchical: {
            direction: "UD",
            sortMethod: "directed",
            nodeSpacing: 160,
            levelSeparation: 100,
            blockShifting: false
          },
          randomSeed: 2
        },
        edges: {
          font: { size: 11, vadjust: 110000000000000000000 },
          chosen: {
            label: hideLabel,
            edge: selectEdge
          }
        }
      };
      network = new vis.Network(container, data, options);
      // double click to open quickGo
      network.on("doubleClick", function (properties) {
        if (!properties.nodes.length) return;
        window.open(
          "https://www.ebi.ac.uk/QuickGO/term/" + properties.nodes[0],
          "_blank"
        );
      });
    }
  });
}
