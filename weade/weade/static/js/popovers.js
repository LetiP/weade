/**
 * Click row in table and popup buttons.
 * @param {*} tableId
 */
function setUpRowTableClick(tableId, pop_id) {
  var pop_id = pop_id;
  rebindPopovers(pop_id);

  $("#" + tableId + " tbody").on("click", "tr", function (event) {
    var table = $("#" + tableId).DataTable();
    var zis = this;
    // multiple select if shift or ctrl is pressed
    if (
      (event.shiftKey || event.ctrlKey || event.metaKey) &&
      pop_id != "popover-buttons"
    ) {
      // see which rows where selected. Need to execute after DataTable Selector
      // so timeout.
      setTimeout(function () {
        var howManySel = table.rows(".selected")[0].length;
        if (tableId == "historyTable") {
          if (howManySel == 2) {
            $("#isolate_heatmap").show();
            $("#find_inter").show();
            showPopover(tableId, "popover-history-multipl", zis);
          } else if (howManySel > 2) {
            $("#isolate_heatmap").show();
            $("#find_inter").hide();
            showPopover(tableId, "popover-history-multipl", zis);
          }
        } else {
          if (howManySel < 2) {
            $("#geneCollectSelect").html("Add this gene to collection");
          } else {
            $("#geneCollectSelect").html("Add genes to collection");
          }
          showPopover(tableId, "popover-genes-add-to-collection", zis);
        }
      }, 100);
    } else {
      // read from second column of gene table to display the right popover
      if (tableId == "genesTable") {
        var span = $(this)
          .children()
          .eq(2)
          .children("span")
          .data("value");

        var cellElems = span.split(", ");
        var popTable = writeTable(cellElems, "popGeneTable");
        $("#popover-genes .pop-content").html(popTable);
      }
      showPopover(tableId, pop_id, this);
    }
  });
}

/**
 * Show a popover.
 * @param {string} tableId
 * @param {string} pop_id
 * @param {*} zis The 'this' before the timeout for genesTable shift selection.
 */
function showPopover(tableId, pop_id, zis) {
  $("#confirmgenecoll").off("click"); // unbind clicks from previous datatables
  $("#cancelgenecoll").off("click");

  rebindPopovers(pop_id);
  if (
    $(zis).hasClass("selected") &&
    pop_id != "popover-genes-add-to-collection" &&
    pop_id != "popover-history-multipl"
  ) {
    closePopover(tableId, pop_id);
  } else {
    // change button color of bottom button
    $("#geneCollect").addClass("top-pop");
    $("#collect").addClass("top-pop");
    $("#remove_history").addClass("top-pop");
    if ($(".popover").is(":visible")) {
      rebindPopovers(pop_id);
    }
    var span = $(zis)
      .children()
      .eq(0)
      .children("span");
    span.popover("show");
    clickedCat = span.text();

    // dim background
    var wh = $(window).height();
    $(".pop-overlay")
      .height(wh)
      .css("overflow", "hidden");
    $(".pop-overlay").fadeTo("fast", 0.5);
    $("#" + tableId).css({
      position: "relative",
      zIndex: 1002,
      boxShadow: "0 0 15px rgba(0, 0, 0, 0.6)"
    });
  }
  // check if click is outside the table in order to close the popover
  $(document).on("click", "#body", function (event) {
    var table = $("#" + tableId).DataTable();
    if (
      !$(event.target).closest("tbody").length &&
      !$(event.target).closest(".pop").length &&
      !$(event.target).closest("#geneCollect").length &&
      !$(event.target).closest("#collect").length
    ) {
      table.rows().deselect();
      if ($(".popover").is(":visible")) {
        closePopover(tableId, pop_id);
      }
    } else if ($(event.target).closest("#termLookUp").length) {
      // go to QuickGO page of clicked term
      closePopover(tableId, pop_id);
      window.open("https://www.ebi.ac.uk/QuickGO/term/" + clickedCat, "_blank");
    } else if ($(event.target).closest("#geneLookUp").length) {
      // go to FlyBase page of clicked gene
      closePopover(tableId, pop_id);
      window.open(speciesLink + clickedCat, "_blank");
    } else if ($(event.target).closest("#termContext").length) {
      $("#ontotree-overlay").fadeIn("fast");
      closePopover(tableId, pop_id);
      drawOntoTree(tableId);
      table.rows().deselect();
    } else if ($(event.target).closest("#find_inter").length) {
      closePopover(tableId, pop_id);
      findPairwiseInter();
      table.rows().deselect();
    } else if (
      $(event.target).closest("#geneCollectSelect").length ||
      $(event.target).closest("#geneCollect").length ||
      $(event.target).closest("#collect").length ||
      $(event.target).closest("#collTermGenes").length
    ) {
      addGeneToColl(tableId, pop_id);
    } else if ($(event.target).closest("#showGeneBtn").length) {
      closePopover(tableId, pop_id);
      // switch to genes tab
      var nextId = $("#maintabs-content")
        .children(".tab-pane")
        .next()
        .next()
        .next()
        .attr("id");
      $("[href=\\#" + nextId + "]").tab("show");
      var table = $("#genesTable").DataTable();
      table.search(clickedCat).draw();
    } else if ($(event.target).closest("#isolate_heatmap").length) {
      var selected = $("#history tr.selected");
      var toPython = new Object();
      toPython.selected = new Array();
      for (var i = 0; i < selected.length; i++) {
        toPython.selected.push(
          $(selected[i])
            .children()
            .first()
            .children()
            .first()
            .data("value")
        );
      }
      closePopover(tableId, pop_id);
      table.rows().deselect();
      $.ajax({
        type: "POST",
        url: "/isolateInHeatmap",
        data: JSON.stringify(toPython, null, "\t"),
        contentType: "application/json;charset=UTF-8",
        success: function () {
          closePopover("historyTable", "popover-history-multipl");
        }
      });
    } else if ($(event.target).closest("#rerun_history").length) {
      closePopover("historyTable", "popover-content");
      var rows = $("#historyTable")
        .children("tbody")
        .children("tr.selected");

      var toPython = new Object();
      toPython.toRerun = rows
        .eq(0)
        .children("td")
        .eq(0)
        .children("span")
        .data("value");
      $.ajax({
        type: "POST",
        url: "/rerunFromHistory",
        data: JSON.stringify(toPython, null, "\t"),
        contentType: "application/json;charset=UTF-8",
        success: function (response) {
          successStartAnaCat(response);
          speciesLink = response["speciesLink"];
        },
        complete: function () {
          completeStartAnaCat();
        }
      });
    } else if ($(event.target).closest("#rerunCatBtn").length) {
      // if click on rerunCatBtn #TODO: same code as on Cat here
      closePopover(tableId, pop_id);
      $(".pop-overlay").fadeOut("fast");
      $(".loading-overlay").fadeIn("fast");

      // send status of ontologies
      var toPython = new Object();
      toPython.checkedOnto = new Array();
      var ontoCheckboxes = $(".material-switch-input.onto:checkbox:checked");
      for (var i = 0; i < ontoCheckboxes.length; i++) {
        toPython.checkedOnto.push(ontoCheckboxes[i].value);
      }
      toPython.clickedCat = clickedCat;
      toPython.alternative = alternative;
      $.ajax({
        type: "POST",
        url: "/startAnalysisTerms",
        data: JSON.stringify(toPython, null, "\t"),
        contentType: "application/json;charset=UTF-8",
        success: function (response) {
          var response = JSON.parse(response);
          // Draw Enrichment Result table
          $("#output").html(response["enrich_table"]);
          $("#enrichTable").DataTable({
            scrollY: 600,
            scrollX: "100%",
            scrollCollapse: true,
            deferRender: true,
            scroller: true,
            select: true,
            dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
            buttons: ["copy", "csv", "excel", "pdf", "print"]
          });
          $(".loading-overlay").fadeOut("fast");
          $("#collect").fadeIn("fast");
          setUpRowTableClick("enrichTable", "popover-buttons-term");
          $("#breadcrumbs").text(
            $("#breadcrumbs").text() + " > " + clickedCat
          );
          $("#backToCatsBtn").fadeIn("fast");

          // prevent user to click on old genes table when the new one is not there yet.
          // if left unhandled => deadlock
          $("#genesTable tbody").off("click", "tr");
        },
        complete: function () {
          // Render the genes table while user looks at plot.
          var toPython = new Object();
          toPython.where = "Name";
          $.ajax({
            type: "POST",
            url: "/getGenesTable",
            data: JSON.stringify(toPython, null, "\t"),
            contentType: "application/json;charset=UTF-8",
            success: function (response) {
              var response = JSON.parse(response);
              $("#genes").html(response["genes_table"]);
              var table = $("#genesTable").DataTable({
                scrollY: 600,
                scrollX: "100%",
                scrollCollapse: true,
                deferRender: true,
                scroller: true,
                select: true,
                dom: 'B<"top"f>rt<"bottom"ilp><"clear">',
                buttons: ["copy", "csv", "excel", "pdf", "print"]
              });
              // Make the GO_ID column invisible
              table.column(3).visible(false);
              setUpRowTableClick("genesTable", "popover-genes");
            }
          });
        }
      });
    }
  });
}

/**
 * Destroy and bind popovers to avoid click while hiding or showing.
 * @param {string} pop_id
 */
function rebindPopovers(pop_id) {
  $('[data-toggle="popover"]').popover("dispose");
  $('[data-toggle="popover"]').popover({
    container: "body",
    html: true,
    content: function () {
      return $("#" + pop_id).html();
    }
  });
  $(document).off("click", "#body"); // unbind clicks from previous datatables
  $(document).unbind("keypress.key13");
}

/**
 * Close the current open popover on table.
 * @param {string} tableId
 * @param {string} pop_id
 */
function closePopover(tableId, pop_id) {
  rebindPopovers(pop_id);
  $(".pop-overlay").fadeOut("fast");
  $("#" + tableId).css({
    boxShadow: "none"
  });
  // change button color of bottom button
  $("#geneCollect").removeClass("top-pop");
  $("#collect").removeClass("top-pop");
  $("#remove_history").removeClass("top-pop");
}

/**
 * Transform an array into an one column table.
 * @param {*} array
 * @param {*} popTableId
 */
function writeTable(array, popTableId) {
  var html = "<table id='" + popTableId + "'><tbody>";
  for (var i = 0; i < array.length; i++) {
    // For each entry
    html += "<tr><td>" + array[i] + "</td></tr>";
  }
  html += "</tbody></table>"; // Finish up
  return html;
}
