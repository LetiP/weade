/** Send compute request to python for the interaction network. */
function computeNet(showNetBtn = false) {
  // send collection table to Python
  node_color = [
    "#e95420",
    "#1f78b4",
    "#a6cee3",
    "#e31a1c",
    "#6a3d9a",
    "#b2df8a",
    "#33a02c",
    "#fb9a99",
    "#fdbf6f",
    "#ff7f00",
    "#cab2d6",
    "#ffff99",
    "#b15928"
  ];

  var data = collection.data();
  var toPython = new Object();
  toPython.numConnections = $("#numConnectNodes").html();
  toPython.collection = new Array();
  for (var i = 0; i < data.length; i++) {
    toPython.collection.push(data[i]);
  }

  if (showNetBtn) {
    // first reset what legend might be there
    $("#anaLegend").empty();

    appended = new Array();
    for (var i = 0; i < data.length; i++) {
      var elem = data[i][2];
      if ($.inArray(elem, appended) < 0) {
        appended.push(elem);
        appendAnaLegend(i, elem, node_color[appended.length - 1]);
      }
    }
    $("#anaLegend input[type=checkbox]").change(function () {
      computeNet();
    });
  }

  toPython.activeInter = findActiveSwitches("#interTypes");
  toPython.activeAnalyses = findActiveSwitches("#anaLegend");
  toPython.goDistances = $("#useGOlengths").prop("checked");
  toPython.goApproxDistances = $("#useApproxGOlengths").prop("checked");

  $.ajax({
    type: "POST",
    url: "/showNetwork",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      if ("error" in response) {
        $("#network").text(response["error"]);
      } else {
        nodes = response["nodes"];
        edges = response["edges"];
        drawNet(nodes, edges);

        vertices = response["nodes"];
        var symbols = new Array();
        for (var i = 0; i < vertices.length; i++) {
          symbols.push({
            Symbol: vertices[i]["label"],
            Gene_ID: vertices[i]["id"]
          });
        }
        // refresh selectize
        if (!(typeof search === "undefined")) {
          search[0].selectize.destroy();
        }
        search = $("#searchSymbol").selectize({
          persist: false,
          maxItems: null,
          valueField: "Gene_ID",
          labelField: "Symbol",
          searchField: ["Symbol", "Gene_ID"],
          options: symbols,
          placeholder: "Search for a gene...",
          onItemAdd: function (value, label) {
            appendNodeInfo(
              value,
              label
                .children()
                .eq(1)
                .html()
            );
            nodesInSel.push(value);
          },
          onChange: function (value) {
            if (value.length != 0) {
              network.selectNodes(value.split(","));
            } else {
              network.unselectAll();
              nodesInSel = new Array();
            }
          },
          onDropdownClose: function () {
            network.selectNodes(nodesInSel);
          },
          render: {
            item: function (item, escape) {
              return (
                "<div>" +
                (item.Gene_ID
                  ? '<span class="Gene_ID">' +
                  escape(item.Gene_ID) +
                  ": </span>"
                  : "") +
                (item.Symbol
                  ? '<span class="Symbol">' + escape(item.Symbol) + "</span>"
                  : "") +
                "</div>"
              );
            },
            option: function (item, escape) {
              var label = item.Symbol || item.Gene_ID;
              var caption = item.Symbol ? item.Gene_ID : null;
              return (
                "<div>" +
                (caption
                  ? '<span class="caption">' + escape(caption) + ": </span>"
                  : "") +
                '<span class="caption">' +
                escape(label) +
                "</span></div>"
              );
            }
          }
        });
        $("#searchSymbol").fadeIn();
      }
    }
  });
}

/**
 * Draw the interaction network.
 * @param {} nodes
 * @param {*} edges
 * @param {*} anaColor
 */
function drawNet(nodes, edges) {
  var stiff = !$("#useLayout").prop("checked");
  var hideLabel = function (values, id, selected, hovering) {
    values.vadjust = 0;
  };
  var selectNode = function (values, id, selected, hovering) {
    values.borderWidth = 2;
    values.shadow = true;
    values.size = 15;
  };
  var selectEdge = function (values, id, selected, hovering) {
    values.inheritsColor = "both";
    values.shadow = true;
    values.width = 2.8;
  };
  // make nodes have no simulation on fixed layout
  for (var i = 0; i < nodes.length; i++) {
    nodes[i].physics = stiff;
  }

  var nodes = new vis.DataSet(nodes);
  var edges = new vis.DataSet(edges);

  // create a network
  var container = document.getElementById("network");
  var data = {
    nodes: nodes,
    edges: edges
  };
  var options = {
    physics: {
      barnesHut: { springConstant: 0.9, damping: 0.6 }
    },
    layout: { randomSeed: 2 },
    nodes: {
      shape: "dot",
      size: 10,
      color: {
        border: "#aa3108",
        background: "#e95420"
      },
      chosen: {
        node: selectNode
      }
    },
    interaction: { hover: true },
    edges: {
      font: { size: 9.5, vadjust: 110000000000000000000 },
      width: 1.3,
      chosen: {
        label: hideLabel,
        edge: selectEdge
      }
    }
  };
  network = new vis.Network(container, data, options);
}

/**
 * Append the analysis legend switches to the corresponding div.
 * @param {*} i
 * @param {*} data
 * @param {*} anaColor
 */
function appendAnaLegend(i, data, anaColor) {
  var html =
    "<div class='switch'>" +
    "<div class='material-switch sm'>" +
    "<input id='ana" +
    i +
    "' class='material-switch-input' type='checkbox' checked='checked' value='" +
    data +
    "{color:}" +
    anaColor +
    "'/>" +
    "<label for='ana" +
    i +
    "' class='label-primary' style='background-color: " +
    anaColor +
    "'></label>" +
    "</div>" +
    "<label for='ana" +
    i +
    "' class='text-label'>" +
    data +
    "</label>" +
    "</div>";
  $("#anaLegend").append(html);
}

var nodesInSel = new Array();
function appendNodeInfo(node, symbol) {
  var alreadyShown = new Array();
  $(".node-info-tab")
    .children()
    .each(function () {
      alreadyShown.push($(this).attr("id"));
    });
  if ($.inArray(node, alreadyShown) < 0) {
    var data = collection.data();
    var goBP;
    var nodeInfo;
    for (var i = 0; i < data.length; i++) {
      if (data[i][0] === node) {
        nodeInfo = data[i];
        break;
      }
    }
    var toPython = new Object();
    if (typeof nodeInfo === "undefined") {
      // additional node
      toPython.color = "#9da0a5";
      toPython.analysis = "additional node";
    } else {
      var color = $.inArray(nodeInfo[2], appended); // analysis color
      toPython.color = node_color[color];
      toPython.analysis = nodeInfo[2];
    }
    toPython.symbol = symbol;
    toPython.node = node;
    $.ajax({
      type: "POST",
      url: "/getNodeInfo",
      data: JSON.stringify(toPython, null, "\t"),
      contentType: "application/json;charset=UTF-8",
      success: function (response) {
        var response = JSON.parse(response);
        if ($("#nodeInfoTab").children().length == 0) {
          $("#nodeInfoTab").addClass("col-md-3");
          $("#networkTab").removeClass("col-md-12");
          $("#networkTab").addClass("col-md-9");
        }
        $("#nodeInfoTab").append(response["nodeInfo"]);
        $("#nodeInfoTab").fadeIn("fast");
        $("#nodeInfoTab").sortable({
          axis: "y",
          revert: true,
          scroll: false,
          placeholder: "sortable-placeholder",
          cursor: "move"
        });
      }
    });
  }
}

$(document).on("click", ".showListOnto", function () {
  if (
    $(this)
      .children()
      .hasClass("fa-chevron-down")
  ) {
    $(this)
      .next()
      .fadeIn("fast");
    $(this)
      .children()
      .removeClass("fa-chevron-down");
    $(this)
      .children()
      .addClass("fa-chevron-up");
  } else {
    $(this)
      .next()
      .fadeOut("fast");
    $(this)
      .children()
      .removeClass("fa-chevron-up");
    $(this)
      .children()
      .addClass("fa-chevron-down");
  }
});

$(document).on("click", ".node-info-close", function () {
  $(this)
    .parent()
    .parent()
    .parent()
    .fadeOut("fast", function () {
      $(this).remove();
      if ($("#nodeInfoTab").children().length == 0) {
        $("#nodeInfoTab").removeClass("col-md-3");
        $("#nodeInfoTab").fadeOut("fast");
        $("#networkTab").removeClass("col-md-9");
        $("#networkTab").addClass("col-md-12");
      }
    });
});
