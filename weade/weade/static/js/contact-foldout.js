$(document).ready(function () {
  $("body").on("click", ".fa-chevron-down.footerToggle", function () {
    $(".contact").slideDown();
    $(this).removeClass("fa-chevron-down");
    $(this).addClass("fa-chevron-up");
  });
  $("body").on("click", ".fa-chevron-up.footerToggle", function () {
    $(".contact").slideUp();
    $(this).removeClass("fa-chevron-up");
    $(this).addClass("fa-chevron-down");
  });
});