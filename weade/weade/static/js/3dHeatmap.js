"use strict";

var graph = null;

function make3dHeatmap() {
  if ($("#3dtf").is(":checked")) {
    var inputType = "tf";
  } else {
    var inputType = "dr";
  }
  var toPython = new Object();
  toPython.inputType = inputType;
  if ($("#iso").is(":checked")) {
    var iso = "iso";
  } else {
    var iso = "all";
  }
  toPython.iso = iso;
  toPython.checkedClust = new Array();
  var clustCheckboxes = $(".material-switch-input.clust:checkbox:checked");
  for (var i = 0; i < clustCheckboxes.length; i++) {
    toPython.checkedClust.push(clustCheckboxes[i].value);
  }

  toPython.cutoff = $('#heatCutoff').val();

  if ($("#n").is(":checked")) {
    var scaling = "none";
  } else if ($("#r").is(":checked")) {
    var scaling = "rows";
  } else {
    var scaling = "columns";
  }
  toPython.scaling = scaling;
  $.ajax({
    type: "POST",
    url: "/reload3dHeatmap",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8",
    success: function (response) {
      var response = JSON.parse(response);
      // Draw 3dheatmap
      var plotData = response['plot'];
      if (typeof plotData === 'string') {
        $("#heatmap").html(plotData);
      } else {
        draw3dHeatMap(plotData, inputType);
      }
      $("#heatmap_message").remove();
    }
  });
}

// Called when the Visualization API is loaded.
function draw3dHeatMap(pltData, heightType) {
  // Create and populate a data table.
  var data = new vis.DataSet();

  for (var x = 0; x < pltData.length; x++) {
    for (var y = 0; y < pltData[x].length; y++) {
      var z = pltData[x][y][0];
      var p = pltData[x][y][1];
      var tip = pltData[x][y][2];
      data.add({
        x: x,
        y: y,
        z: z,
        style: p,
        extra: tip
      });
    }
  }

  if (heightType == 'tf') {
    heightType = 'Term frequency'
  } else {
    heightType = 'Delta rank'
  }

  // specify options
  var options = {
    width: '600px',
    height: '600px',
    style: 'bar-color',
    showPerspective: true,
    showGrid: true,
    showShadow: true,
    xBarWidth: 0.5,
    yBarWidth: 0.5,
    xStep: 1,
    yStep: 1,

    // Option tooltip can be true, false, or a function returning a string with HTML contents
    tooltip: function (point) {
      // parameter point contains properties x, y, z, and data
      // data is the original object passed to the point constructor
      return heightType + ': <b>' + point.z + '</b><br>p-value: <b>' + point.data.extra[2] + '</b><br>Analysis: <b>' + point.data.extra[0] + '</b><br>Category: <b>' + point.data.extra[1] + '</b>';
    },

    xValueLabel: function (i) {
      return pltData[i][0][2][1]
    },

    yValueLabel: function (i) {
      return pltData[0][i][2][0]
    },

    // Tooltip default styling can be overridden
    tooltipStyle: {
      content: {
        background: 'rgba(255, 255, 255, 0.7)',
        padding: '10px',
        borderRadius: '10px'
      },
      line: {
        borderLeft: '1px dotted rgba(0, 0, 0, 0.5)'
      },
      dot: {
        border: '5px solid rgba(0, 0, 0, 0.5)'
      }
    },

    keepAspectRatio: true,
    verticalRatio: 0.5
  };

  var camera = graph ? graph.getCameraPosition() : null;

  // create our graph
  var container = document.getElementById('heatmap');
  graph = new vis.Graph3d(container, data, options);

  if (camera) graph.setCameraPosition(camera); // restore camera position
}