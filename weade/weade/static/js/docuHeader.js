$(document).ready(function () {
  window.onscroll = function () {
    debounce(scrollFunction(), 250);
  }
});

function scrollFunction() {
  var header = document.getElementById("docuHeader");
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    header.className = "documentation header h-small";
  } else {
    header.className = header.className.replace("h-small", "");
  }
}

// from https://davidwalsh.name/javascript-debounce-function
function debounce(func, wait, immediate) {
  "use strict";
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) {
      func.apply(context, args);
    }
  };
}