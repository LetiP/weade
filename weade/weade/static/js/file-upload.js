var numFiles = 0;
var fileNames = new Array();
var numSubmitted = 1;
var numDone = 0;
var errorr = false;

$(function($) {
  $("#fg").fileupload({
    url: "/upload",
    dataType: "json",
    add: function(e, data) {
      data.submit();
    },
    sequentialUploads: true,
    submit: function(e, data) {
      numFiles = data.originalFiles.length;
      errorr = false;
      // check if file limit number exceeded. If not, then show and hide divs.
      if (numFiles > 4) {
        errorr = true;
        if (numSubmitted == 2) {
          swal({
            title: "Error!",
            text: "Sorry, a maximum of 4 files are allowed.",
            type: "error",
            confirmButtonClass: "btn-default btn-primary",
            confirmButtonText: "OK"
          });
        }
      }
      if (!errorr) {
        // check if allowed mymetipe text(/plain)
        for (var i = 0; i < numFiles; i++) {
          // sometimes, windows applies a mime type of "application/vnd.ms-excel" to csv files.
          var mimeType = data.originalFiles[i].type.split("/")[0];
          if (data.originalFiles[i].type == "application/vnd.ms-excel") {
            mimeType = "text";
          }
          if (mimeType != "text") {
            errorr = true;
            if (numSubmitted == 1) {
              swal({
                title: "Error!",
                text:
                  'Sorry, only text files are allowed. You have some "' +
                  mimeType +
                  '" type among your files.',
                type: "error",
                confirmButtonClass: "btn-default btn-primary",
                confirmButtonText: "OK"
              });
            }
            break;
          }
        }
      }
      // reset the uploaded files on the Python side, because new ones are coming.
      if (!errorr) {
        if (numSubmitted == 1) {
          resetUpload();
          // hide old Venn.
          $("#venntabdiv").fadeOut("fast");
        }
      }

      if (numSubmitted == numFiles) {
        numSubmitted = 1;
      } else {
        numSubmitted += 1;
      }
    },
    progressall: function(e, data) {
      if (!errorr) {
        // progress bar
        $("#fg_progress").show();
        var progress = parseInt((data.loaded / data.total) * 100, 10);
        $("#fg_progress .progress-bar ").css("width", progress + "%");
        $("#fg_progress .progress-bar").text(progress + "%");
        if (numFiles >= 2) {
          $("#browse-info").attr("placeholder", numFiles + " files");
        } else {
          $("#browse-info").attr("placeholder", numFiles + " file");
        }
      }
    },
    done: function(e, data) {
      // check columns and format of the files on the Python side
      if (!errorr & (data.result["uploadStatus"] == "error")) {
        errorr = true;
        swal({
          title: "Error!",
          text:
            "One or more files are in the wrong format. Please read the documentation.",
          type: "error",
          confirmButtonClass: "btn-default btn-primary",
          confirmButtonText: "OK"
        });
        // hide previous texts and bars
        $("#browse-info").attr("placeholder", "No file selected");
        $("#fg_progress").hide();
        $("#filenames").hide();
      }

      if (!errorr & (data.result["uploadStatus"] == "warning")) {
        swal({
          title: "Are you sure?",
          text:
            "One or more files have more columns than requested. Took the first column and ignored the rest.",
          type: "warning",
          confirmButtonClass: "btn-default btn-primary",
          confirmButtonText: "OK"
        });
      }

      if (!errorr) {
        numDone += 1;
        // display the input fields for changing filenames
        if (numDone == numFiles) {
          $("#fg_progress .progress-bar").text("Upload completed");
          $("#setsName").val("");
          $("#filenames").show();
          $("#filenames").html("");
          // clear stored filenames from previous upload
          fileNames = new Array();
          // create renaming fields
          for (var i = 1; i <= numFiles; i++) {
            var html =
              '<div class="form-group shiny-input-container">' +
              '<label for="name_' +
              i +
              '">Name for <em><span style="font-weight:normal"> ' +
              data.originalFiles[i - 1].name +
              "</span></em></label>" +
              '<input id="name_' +
              i +
              '" type="text" class="form-control shiny-bound-input shinyjs-resettable" value="" data-shinyjs-resettable-id="name_' +
              i +
              '1" data-shinyjs-resettable-type="Text" data-shinyjs-resettable-value="">' +
              "</div>";

            $(html)
              .hide()
              .appendTo("#filenames")
              .slideDown();
            fileNames.push(data.originalFiles[i - 1].name);
          }

          numDone = 0;
          showVenn(numFiles, fileNames);
        }
      }
    }
  });
});

function showVenn(numFiles, fileNames) {
  // show the Venn diagram.
  $.ajax({
    type: "GET",
    url: "/showSets",
    success: function(response) {
      var response = JSON.parse(response);
      numbers = response["numbers"];
      if (numFiles == 1) {
        $("#bgfg").hide();
      }
      setUpCanvas(numFiles, fileNames, numbers);
      $("#bgfg .material-switch .label-primary").css({
        "background-color": canvasArea.curColor
      });
      $("#venntabdiv").fadeIn("fast");
      $("#VennNoDataMsg").fadeOut("fast");
      if (numFiles > 1) {
        $("#bgfg").fadeIn("fast");
      }
      // jump to Venn tab
      $("[href=\\#tab-1097-1]").tab("show");

      // unbind previously added keyups
      $("#filenames :text").unbind("keyup");
      // catch change in input fields.
      var modFileNames = fileNames.slice();
      $("#filenames :text").keyup(function() {
        inputId = $(this).attr("id");
        whichInput = inputId[inputId.length - 1] - 1;
        var modValue = $(this).val();
        if (modValue.length != 0) {
          modFileNames[whichInput] = modValue;
        } else {
          modFileNames[whichInput] = fileNames[whichInput];
        }
        redrawFileNames(modFileNames);
      });
      // reset the annotation buttons
      $("#swbg").prop("checked", false);
      $("#swfg").prop("checked", true);
    }
  });
}

function resetUpload() {
  $.ajax({
    type: "GET",
    url: "/resetUpload",
    async: false,
    contentType: "application/json;charset=UTF-8"
  });
}
