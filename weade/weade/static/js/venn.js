if (window.devicePixelRatio) {
  var dPR = window.devicePixelRatio;
} else {
  var dPR = 1;
}
var canvasArea = {
  init: function(numSets, fileNames, numbers) {
    this.canvas = document.getElementById("canvas");
    this.ctx = this.canvas.getContext("2d");
    if (typeof cw == "undefined") {
      cw = $(this.canvas).attr("width");
      ch = $(this.canvas).attr("height");
      this.ctx.scale(dPR, dPR);
      $(this.canvas).attr("width", cw * dPR);
      $(this.canvas).attr("height", ch * dPR);
      $(this.canvas).css("width", cw);
      $(this.canvas).css("height", ch);
    }
    this.canvas1 = document.createElement("canvas");
    this.canvas1.height = this.canvas.height;
    this.canvas1.width = this.canvas.width;
    this.ctx1 = this.canvas1.getContext("2d");
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height); // clear Canvas from previous drawings.
    this.ctx.webkitImageSmoothingEnabled = true; // enable anti-aliasing
    this.ctx1.webkitImageSmoothingEnabled = true;
    this.numCircles = numSets; // how many circles there are
    this.fileNames = fileNames; // submitted filenames that have to be rendered
    this.numbers = numbers; // the numbers on every canvas tile
    this.circles = new Array();
    this.circleColors = ["black", "#e95420", "#1e9600", "#0011ff"];
    this.foreground = new Array(); // annotated fg
    this.background = new Array();
    this.curColor = "#B1F3B1"; // current color
    this.pattern = prepareStripes(); // prepare the stripes for intersected annos
    this.ctx.font = dPR * 14 - 1 + "px sans-serif";
    this.ctx.textAlign = "center";
    $(this.canvas).unbind("click"); // unbind clicks from previous datasets on canvas
    // 1-fold Venn
    if (this.numCircles == 1) {
      this.circle1 = {
        x: this.canvas.width / 2,
        y: this.canvas.height / 2,
        r:
          Math.min(this.canvas.height, this.canvas.width) / 2 -
          40 * dPR
      };
      this.circles.push(this.circle1);
    } else if (this.numCircles == 2) {
      // two-fold Venn
      var radius =
        Math.min(this.canvas.height, this.canvas.width) / 3 +
        20 * dPR;
      this.circle1 = {
        x: this.canvas.width / 3,
        y: this.canvas.height / 2,
        r: radius
      };
      this.circle2 = {
        x: this.canvas.width / 3 * 2,
        y: this.canvas.height / 2,
        r: radius
      };
      this.circles.push(this.circle1);
      this.circles.push(this.circle2);
      this.clicked = [false, false];
    } else if (this.numCircles == 3) {
      // three-fold Venn
      var radius =
        Math.min(this.canvas.height, this.canvas.width) / 3 -
        18 * dPR;
      this.circle1 = {
        x: this.canvas.width / 3 + 10 * dPR,
        y: this.canvas.height / 3 + 5 * dPR,
        r: radius
      };
      this.circle2 = {
        x: this.canvas.width / 3 * 2 - 10 * dPR,
        y: this.canvas.height / 3 + 5 * dPR,
        r: radius
      };
      this.circle3 = {
        x: this.canvas.width / 2,
        y: this.canvas.height / 3 * 2 - 5 * dPR,
        r: radius
      };
      this.circles.push(this.circle1);
      this.circles.push(this.circle2);
      this.circles.push(this.circle3);
      this.clicked = [false, false, false];
    } else if (this.numCircles == 4) {
      // four-fold Venn
      var radius =
          Math.min(this.canvas.height, this.canvas.width) / 2 -
          20 * dPR,
        w = radius,
        h = radius / 2;
      this.circle1 = {
        x: this.canvas.width / 2,
        y: this.canvas.height / 2,
        w: w + 20 * dPR,
        h: h,
        angle: 30
      };
      this.circle2 = {
        x: this.canvas.width / 2,
        y: this.canvas.height / 2,
        w: w + 20 * dPR,
        h: h,
        angle: -30
      };
      this.circle3 = {
        x: this.canvas.width / 3,
        y: this.canvas.height / 3 * 2,
        w: w,
        h: h + 10 * dPR,
        angle: 30
      };
      this.circle4 = {
        x: this.canvas.width / 3 * 2,
        y: this.canvas.height / 3 * 2,
        w: w,
        h: h + 10 * dPR,
        angle: -30
      };
      this.circles.push(this.circle1);
      this.circles.push(this.circle2);
      this.circles.push(this.circle3);
      this.circles.push(this.circle4);
      this.clicked = [false, false, false, false];
    }

    // draw the diagram
    drawBorders();
    // draw the datasets names
    drawFileNames();
    // draw the corresponding numbers
    drawNumbers(this.numbers);

    // catch mouse clicks and interpret them
    if (this.numCircles > 1) {
      $(this.canvas).click(function(event) {
        var pos = getCursorPosition(this, event);
        for (var i = 0; i < canvasArea.numCircles; i++) {
          if (canvasArea.numCircles < 4) {
            if (inCircle(pos.x, pos.y, canvasArea.circles[i])) {
              canvasArea.clicked[i] = true;
            }
          } else {
            if (inEllipse(pos.x, pos.y, canvasArea.circles[i])) {
              canvasArea.clicked[i] = true;
            }
          }
        }
        interpretCircleClick();
        drawBorders();
      });
    }
  }
};

$(document).ready(function() {
  // fore- background color switches
  $("#bgfg .material-switch .material-switch-input").click(function() {
    var checkedState = $(this).prop("checked");
    if (!checkedState) {
      // ignore click, because it was true and we want to set it to false.
      $(this).prop("checked", true);
    } else {
      var switchers = $(this)
        .parent(".material-switch")
        .parent(".switch")
        .parent("#bgfg")
        .children(".switch")
        .children(".material-switch");
      var allSliders = switchers.children(".slider");

      switchers
        .children(".material-switch-input:checked")
        .prop("checked", false);

      if (canvasArea.curColor != $(this).val()) {
        allSliders.css({
          "background-color": canvasArea.curColor
        });
      }

      $(this).prop("checked", checkedState);
      canvasArea.curColor = $(this).val();
      canvasArea.pattern = prepareStripes();
      // draw intersections
      redrawIntersections();
      drawBorders();

      $(this)
        .parent(".material-switch")
        .children(".label-primary")
        .css({
          "background-color": canvasArea.curColor
        });
    }
  });
});

/**
 * Initialise the canvas to draw the Venn diagram upon.
 * @param {*} numSets Number of circles, called sets.
 * @param {*} fileNames Names of the uploaded files.
 * @param {*} numbers Numbers to fill in the Venn tiles.
 */
function setUpCanvas(numSets, fileNames, numbers) {
  canvasArea.init(numSets, fileNames, numbers);
}

/**
 * Redraw the filenames when there is another input for them.
 * @param {*} fileNames
 */
function redrawFileNames(fileNames) {
  canvasArea.fileNames = fileNames;
  canvasArea.ctx.clearRect(
    0,
    0,
    canvasArea.canvas.width,
    canvasArea.canvas.height
  );

  // draw selections
  redrawSelections();
  redrawIntersections();
  drawBorders();
  drawFileNames();
  drawNumbers(this.numbers);
}

/**
 * Redraw Selections of Background/Foreground when clearing canvas
 * for renaming the uploaded files.
 */
function redrawSelections() {
  for (var i = 0; i < canvasArea.foreground.length; i++) {
    couleur = "#B1F3B1";
    if (canvasArea.numCircles == 2) {
      select2fold(canvasArea.foreground[i]);
    } else if (canvasArea.numCircles == 3) {
      select3fold(canvasArea.foreground[i]);
    } else if (canvasArea.numCircles == 4) {
      select4fold(canvasArea.foreground[i]);
    }
  }
  for (var i = 0; i < canvasArea.background.length; i++) {
    couleur = "#ff9999";
    if (canvasArea.numCircles == 2) {
      select2fold(canvasArea.background[i]);
    } else if (canvasArea.numCircles == 3) {
      select3fold(canvasArea.background[i]);
    } else if (canvasArea.numCircles == 4) {
      select4fold(canvasArea.background[i]);
    }
  }
}

/**
 * Draw the green-red intersection in stripes
 */
function redrawIntersections() {
  if (
    (canvasArea.background.length != 0) &
    (canvasArea.foreground.length != 0)
  ) {
    pattern = true;
    inter = intersect(canvasArea.background, canvasArea.foreground);
    for (var i = 0; i < inter.length; i++) {
      if (canvasArea.numCircles == 2) {
        select2fold(inter[i]);
      } else if (canvasArea.numCircles == 3) {
        select3fold(inter[i]);
      } else if (canvasArea.numCircles == 4) {
        select4fold(inter[i]);
      }
    }
    pattern = false;
  }
}

/**
 * Draw the intersections of up to 4 circles/ellipses.
 * @param {*} a
 * @param {*} b
 * @param {*} c
 * @param {*} d
 * @param {*} notA
 * @param {*} notB
 * @param {*} notC
 * @param {*} color Intersection color.
 * @param {*} pattern Set true if want stripes.
 */
function drawIntersect(a, b, c, d, notA, notB, notC, color, pattern) {
  canvasArea.ctx1.clearRect(
    0,
    0,
    canvasArea.canvas1.width,
    canvasArea.canvas1.height
  );
  canvasArea.ctx1.save();

  // a
  if (canvasArea.numCircles < 4) {
    drawCircle(canvasArea.ctx1, a, color, true, pattern);
    if (b) {
      canvasArea.ctx1.globalCompositeOperation = "source-in";
      drawCircle(canvasArea.ctx1, b, color, true, pattern);
    }
    if (c) {
      drawCircle(canvasArea.ctx1, c, color, true, pattern);
    }
    if (d) {
      drawCircle(canvasArea.ctx1, d, color, true, pattern);
    }
    // notC
    canvasArea.ctx1.globalCompositeOperation = "destination-out";
    if (notA) {
      drawCircle(canvasArea.ctx1, notA, color, true, pattern);
    }
    if (notB) {
      drawCircle(canvasArea.ctx1, notB, color, true, pattern);
    }
    if (notC) {
      drawCircle(canvasArea.ctx1, notC, color, true, pattern);
    }
  } else {
    drawEllipse(canvasArea.ctx1, a, color, true, pattern);
    if (b) {
      canvasArea.ctx1.globalCompositeOperation = "source-in";
      drawEllipse(canvasArea.ctx1, b, color, true, pattern);
    }
    if (c) {
      drawEllipse(canvasArea.ctx1, c, color, true, pattern);
    }
    if (d) {
      drawEllipse(canvasArea.ctx1, d, color, true, pattern);
    }
    // notC
    canvasArea.ctx1.globalCompositeOperation = "destination-out";
    if (notA) {
      drawEllipse(canvasArea.ctx1, notA, color, true, pattern);
    }
    if (notB) {
      drawEllipse(canvasArea.ctx1, notB, color, true, pattern);
    }
    if (notC) {
      drawEllipse(canvasArea.ctx1, notC, color, true, pattern);
    }
  }

  canvasArea.ctx1.restore();
  canvasArea.ctx.drawImage(canvasArea.canvas1, 0, 0);
}

/**
 * Draw a single circle.
 * @param {*} ctx Canvas context.
 * @param {*} circle Circle objects with center and radius.
 * @param {*} color
 * @param {*} fill True if filled, false if only the border.
 * @param {*} pattern True if striped pattern.
 */
function drawCircle(ctx, circle, color, fill = false, pattern = false) {
  ctx.beginPath();
  ctx.arc(circle.x, circle.y, circle.r, 0, Math.PI * 2);
  ctx.closePath();
  if (!fill) {
    ctx.lineWidth = 2.5;
    ctx.strokeStyle = "white";
    ctx.stroke();
    ctx.lineWidth = 2;
    ctx.strokeStyle = color;
    ctx.stroke();
  } else {
    ctx.fillStyle = color;
    ctx.fill();
    if (pattern) {
      ctx.fillStyle = drawStripes(ctx, canvasArea.pattern);
      ctx.fill();
    }
  }
}

/**
 * Draw an ellipse.
 * @param {*} ctx Canvas context.
 * @param {*} ellipse Ellipse objects with center and radius.
 * @param {*} color
 * @param {*} fill True if filled, false if only the border.
 * @param {*} pattern True if striped pattern.
 */
function drawEllipse(ctx, ellipse, color, fill = false, pattern = false) {
  var w = ellipse.w * 2,
    h = ellipse.h * 2,
    x = ellipse.x - w / 2,
    y = ellipse.y - h / 2;
  var kappa = 0.5522848,
    ox = w / 2 * kappa, // control point offset horizontal
    oy = h / 2 * kappa, // control point offset vertical
    xe = x + w, // x-end
    ye = y + h, // y-end
    xm = x + w / 2, // x-middle
    ym = y + h / 2; // y-middle

  // rotate the ellipse
  ctx.save();
  ctx.beginPath();
  ctx.translate(xm, ym);
  ctx.rotate(ellipse.angle * Math.PI / 180);

  // transform to the translated coords.
  x = -w / 2;
  y = -h / 2;
  xe = x + w;
  ye = y + h;
  xm = x + w / 2;
  ym = y + h / 2;

  ctx.moveTo(x, ym);
  ctx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
  ctx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
  ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
  ctx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
  ctx.restore();
  if (!fill) {
    ctx.lineWidth = 2.5;
    ctx.strokeStyle = "white";
    ctx.stroke();
    ctx.lineWidth = 2;
    ctx.strokeStyle = color;
    ctx.stroke();
  } else {
    ctx.fillStyle = color;
    ctx.fill();
    if (pattern) {
      ctx.fillStyle = drawStripes(ctx, canvasArea.pattern);
      ctx.fill();
    }
  }
}

/**
 * Draw all circles in the diagram at once.
 */
function drawBorders() {
  // draw the diagram
  for (var i = 0; i < canvasArea.numCircles; i++) {
    if (canvasArea.numCircles < 4) {
      drawCircle(
        canvasArea.ctx,
        canvasArea.circles[i],
        canvasArea.circleColors[i]
      );
    } else {
      drawEllipse(
        canvasArea.ctx,
        canvasArea.circles[i],
        canvasArea.circleColors[i]
      );
    }
  }
}

/**
 * Draw all the filenames at once.
 */
function drawFileNames() {
  var textX = 0;
  var textY = 0;
  for (var i = 0; i < canvasArea.numCircles; i++) {
    canvasArea.ctx.fillStyle = canvasArea.circleColors[i];
    var curCirc = canvasArea.circles[i];
    textX = curCirc.x;
    if (canvasArea.numCircles <= 2) {
      textY = 31 * dPR;
    } else if (canvasArea.numCircles == 3) {
      if (i <= 1) {
        textY = 15 * dPR;
      } else {
        textY = canvasArea.canvas.height - 5 * dPR;
      }
    } else if (canvasArea.numCircles == 4) {
      if (i == 0) {
        textX = canvasArea.canvas.width / 3;
        textY = 35 * dPR;
      } else if (i == 1) {
        textX = canvasArea.canvas.width / 3 * 2;
        textY = 35 * dPR;
      } else if (i == 2) {
        textX = 0;
        textY = 60 * dPR;
        canvasArea.ctx.textAlign = "start";
      } else {
        textX = canvasArea.canvas.width - 3 * dPR;
        textY = 60 * dPR;
        canvasArea.ctx.textAlign = "end";
      }
    }
    canvasArea.ctx.fillText(canvasArea.fileNames[i], textX, textY);
    canvasArea.ctx.textAlign = "center";
  }
}

/**
 * Draw all the numbers on the tiles.
 */
function drawNumbers(nums) {
  var textX = 0;
  var textY = 0;
  canvasArea.ctx.fillStyle = "black";

  for (var key in nums) {
    if (canvasArea.numCircles == 1) {
      textY = canvasArea.canvas.height / 2;
      textX = canvasArea.canvas.width / 2;
    }
    // 2-fold
    if (canvasArea.numCircles == 2) {
      textY = canvasArea.canvas.height / 2;
      if (key == 12) {
        textX = canvasArea.canvas.width / 2;
      } else if (key == 1) {
        textX = 100 * dPR;
      } else {
        textX = canvasArea.canvas.width - 100 * dPR;
      }
    }
    // 3-fold
    if (canvasArea.numCircles == 3) {
      if (key == 123) {
        textX = canvasArea.canvas.width / 2;
        textY = canvasArea.canvas.height / 2 - 15 * dPR;
      } else if (key == 12) {
        textX = canvasArea.canvas.width / 2;
        textY = canvasArea.canvas.height / 4 + 10 * dPR;
      } else if (key == 23) {
        textX = canvasArea.canvas.width / 3 * 2 - 20 * dPR;
        textY = canvasArea.canvas.height / 2 + 15 * dPR;
      } else if (key == 13) {
        textX = canvasArea.canvas.width / 3 + 20 * dPR;
        textY = canvasArea.canvas.height / 2 + 15 * dPR;
      } else if (key == 1) {
        textX = canvasArea.canvas.width / 4;
        textY = canvasArea.canvas.height / 3;
      } else if (key == 2) {
        textX = canvasArea.canvas.width / 4 * 3;
        textY = canvasArea.canvas.height / 3;
      } else if (key == 3) {
        textX = canvasArea.canvas.width / 2;
        textY = canvasArea.canvas.height / 4 * 3;
      }
    }
    // 4-fold
    if (canvasArea.numCircles == 4) {
      if (key == 1234) {
        textX = canvasArea.canvas.width / 2;
        textY = canvasArea.canvas.height / 3 * 2;
      } else if (key == 123) {
        textX = canvasArea.canvas.width / 3;
        textY = canvasArea.canvas.height / 2;
      } else if (key == 124) {
        textX = canvasArea.canvas.width / 3 * 2;
        textY = canvasArea.canvas.height / 2;
      } else if (key == 234) {
        textX = canvasArea.canvas.width / 2 - 50 * dPR;
        textY = canvasArea.canvas.height / 4 * 3;
      } else if (key == 134) {
        textX = canvasArea.canvas.width / 2 + 50 * dPR;
        textY = canvasArea.canvas.height / 4 * 3;
      } else if (key == 12) {
        textX = canvasArea.canvas.width / 2;
        textY = canvasArea.canvas.height / 3 + 20 * dPR;
      } else if (key == 34) {
        textX = canvasArea.canvas.width / 2;
        textY = canvasArea.canvas.height / 8 * 7;
      } else if (key == 23) {
        textX = canvasArea.canvas.width / 4;
        textY = canvasArea.canvas.height / 3 * 2;
      } else if (key == 14) {
        textX = canvasArea.canvas.width / 4 * 3;
        textY = canvasArea.canvas.height / 3 * 2;
      } else if (key == 13) {
        textX = canvasArea.canvas.width / 4 - 15 * dPR;
        textY = canvasArea.canvas.height / 3 + 40 * dPR;
      } else if (key == 24) {
        textX = canvasArea.canvas.width / 4 * 3 + 15 * dPR;
        textY = canvasArea.canvas.height / 3 + 40 * dPR;
      } else if (key == 1) {
        textX = canvasArea.canvas.width / 3 - 20 * dPR;
        textY = canvasArea.canvas.height / 3 - 10 * dPR;
      } else if (key == 2) {
        textX = canvasArea.canvas.width / 3 * 2 + 20 * dPR;
        textY = canvasArea.canvas.height / 3 - 10 * dPR;
      } else if (key == 3) {
        textX = 60;
        textY = canvasArea.canvas.height / 2;
      } else if (key == 4) {
        textX = canvasArea.canvas.width - 60 * dPR;
        textY = canvasArea.canvas.height / 2;
      }
    }
    canvasArea.ctx.fillText(nums[key], textX, textY);
  }
}

/**
 * Prepare the stripe pattern for drawing intersections.
 */
function prepareStripes() {
  var pattern = document.createElement("canvas");
  pattern.width = 40;
  pattern.height = 40;
  var pctx = pattern.getContext("2d");

  var x0 = 50;
  var x1 = -10;
  var y0 = -10;
  var y1 = 50;
  var offset = 40;

  pctx.strokeStyle = canvasArea.curColor;
  pctx.lineWidth = 18;
  pctx.beginPath();
  pctx.moveTo(x0, y0);
  pctx.lineTo(x1, y1);
  pctx.moveTo(x0 - offset, y0);
  pctx.lineTo(x1 - offset, y1);
  pctx.moveTo(x0 + offset, y0);
  pctx.lineTo(x1 + offset, y1);
  pctx.stroke();

  var redOffset = 20;
  if (canvasArea.curColor == "#B1F3B1") {
    pctx.strokeStyle = "#ff9999";
  } else {
    pctx.strokeStyle = "#B1F3B1";
  }
  pctx.lineWidth = Math.SQRT2 * 25 / 2 - 7;
  pctx.beginPath();
  pctx.moveTo(x0 - redOffset, y0);
  pctx.lineTo(x1 - redOffset, y1);
  pctx.moveTo(x0 + redOffset, y0);
  pctx.lineTo(x1 + redOffset, y1);
  pctx.stroke();

  return pattern;
}

/**
 * Create a pattern.
 * @param {*} ctx
 * @param {*} pattern
 */
function drawStripes(ctx, pattern) {
  return ctx.createPattern(pattern, "repeat");
}

/**
 * Get cursor position on canvas.
 * @param {*} canvas
 * @param {*} event
 */
function getCursorPosition(canvas, event) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top
  };
}

/**
 * Check if coordinates are in circle or not.
 * @param {*} x X-coord.
 * @param {*} y Y-coord.
 * @param {*} circle Circle object with center and radius.
 */
function inCircle(x, y, circle) {
  x *= dPR; // compensate for css to attr height missmatch
  y *= dPR;
  return (
    Math.sqrt(
      (x - circle.x) * (x - circle.x) + (y - circle.y) * (y - circle.y)
    ) < circle.r
  );
}

/**
 * Check if ellipse are in (rotated) ellipse or not.
 * @param {*} x X-coord.
 * @param {*} y Y-coord.
 * @param {*} ellipse Ellipse object with small and big axis.
 */
function inEllipse(x, y, ellipse) {
  var angle = ellipse.angle * Math.PI / 180;
  x *= dPR; // compensate for css to attr height missmatch
  y *= dPR;
  return (
    (Math.cos(angle) * (x - ellipse.x) + Math.sin(angle) * (y - ellipse.y)) *
      (Math.cos(angle) * (x - ellipse.x) + Math.sin(angle) * (y - ellipse.y)) /
      (ellipse.w * ellipse.w) +
      (Math.sin(angle) * (x - ellipse.x) - Math.cos(angle) * (y - ellipse.y)) *
        (Math.sin(angle) * (x - ellipse.x) -
          Math.cos(angle) * (y - ellipse.y)) /
        (ellipse.h * ellipse.h) <
    1
  );
}

/**
 * Count how many 'what' are in an array.
 * @param {*} array
 * @param {*} what
 */
function countInArray(array, what) {
  var count = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] === what) {
      count++;
    }
  }
  return count;
}

/**
 * Remove value from array.
 * @param {*} array
 * @param {*} value
 */
function removeElem(array, value) {
  var index = array.indexOf(value);
  if (index > -1) {
    array.splice(index, 1);
  }
}

/**
 * Intersection of two arrays.
 * @param {*} a
 * @param {*} b
 */
function intersect(a, b) {
  var t;
  if (b.length > a.length) (t = b), (b = a), (a = t); // indexOf to loop over shorter
  return a.filter(function(e) {
    return b.indexOf(e) > -1;
  });
}

/**
 * Draw corresponding selection colors on a 2-way Venn on tile of position 'pos'.
 * @param {*} pos
 */
function select2fold(pos) {
  if (pos == 12) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle2,
      null,
      null,
      null,
      null,
      null,
      couleur,
      pattern
    );
  }
  if (pos == 1) {
    drawIntersect(
      canvasArea.circle1,
      null,
      null,
      null,
      canvasArea.circle2,
      null,
      null,
      couleur,
      pattern
    );
  }
  if (pos == 2) {
    drawIntersect(
      canvasArea.circle2,
      null,
      null,
      null,
      canvasArea.circle1,
      null,
      null,
      couleur,
      pattern
    );
  }
  canvasArea.clicked = [false, false];
  redrawNumber(pos);
}
/**
 * Draw corresponding selection colors on a 3-way Venn on tile of position 'pos'.
 * @param {*} pos
 */
function select3fold(pos) {
  if (pos == 123) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle2,
      canvasArea.circle3,
      null,
      null,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 12) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle2,
      null,
      null,
      canvasArea.circle3,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 23) {
    drawIntersect(
      canvasArea.circle2,
      canvasArea.circle3,
      null,
      null,
      canvasArea.circle1,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 13) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle3,
      null,
      null,
      canvasArea.circle2,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 1) {
    drawIntersect(
      canvasArea.circle1,
      null,
      null,
      null,
      canvasArea.circle2,
      canvasArea.circle3,
      null,
      couleur,
      pattern
    );
  } else if (pos == 2) {
    drawIntersect(
      canvasArea.circle2,
      null,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle3,
      null,
      couleur,
      pattern
    );
  } else if (pos == 3) {
    drawIntersect(
      canvasArea.circle3,
      null,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle2,
      null,
      couleur,
      pattern
    );
  }
  canvasArea.clicked = [false, false, false];
  redrawNumber(pos);
}
/**
 * Draw corresponding selection colors on a 4-way Venn on tile of position 'pos'.
 * @param {*} pos
 */
function select4fold(pos) {
  if (pos == 1234) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle2,
      canvasArea.circle3,
      canvasArea.circle4,
      null,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 123) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle2,
      canvasArea.circle3,
      null,
      canvasArea.circle4,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 234) {
    drawIntersect(
      canvasArea.circle2,
      canvasArea.circle3,
      canvasArea.circle4,
      null,
      canvasArea.circle1,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 134) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle3,
      canvasArea.circle4,
      null,
      canvasArea.circle2,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 124) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle2,
      canvasArea.circle4,
      null,
      canvasArea.circle3,
      null,
      null,
      couleur,
      pattern
    );
  } else if (pos == 12) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle2,
      null,
      null,
      canvasArea.circle3,
      canvasArea.circle4,
      null,
      couleur,
      pattern
    );
  } else if (pos == 23) {
    drawIntersect(
      canvasArea.circle2,
      canvasArea.circle3,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle4,
      null,
      couleur,
      pattern
    );
  } else if (pos == 34) {
    drawIntersect(
      canvasArea.circle3,
      canvasArea.circle4,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle2,
      null,
      couleur,
      pattern
    );
  } else if (pos == 13) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle3,
      null,
      null,
      canvasArea.circle2,
      canvasArea.circle4,
      null,
      couleur,
      pattern
    );
  } else if (pos == 14) {
    drawIntersect(
      canvasArea.circle1,
      canvasArea.circle4,
      null,
      null,
      canvasArea.circle2,
      canvasArea.circle3,
      null,
      couleur,
      pattern
    );
  } else if (pos == 24) {
    drawIntersect(
      canvasArea.circle2,
      canvasArea.circle4,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle3,
      null,
      couleur,
      pattern
    );
  } else if (pos == 1) {
    drawIntersect(
      canvasArea.circle1,
      null,
      null,
      null,
      canvasArea.circle2,
      canvasArea.circle3,
      canvasArea.circle4,
      couleur,
      pattern
    );
  } else if (pos == 2) {
    drawIntersect(
      canvasArea.circle2,
      null,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle3,
      canvasArea.circle4,
      couleur,
      pattern
    );
  } else if (pos == 3) {
    drawIntersect(
      canvasArea.circle3,
      null,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle2,
      canvasArea.circle4,
      couleur,
      pattern
    );
  } else if (pos == 4) {
    drawIntersect(
      canvasArea.circle4,
      null,
      null,
      null,
      canvasArea.circle1,
      canvasArea.circle2,
      canvasArea.circle3,
      couleur,
      pattern
    );
  }
  canvasArea.clicked = [false, false, false, false];
  redrawNumber(pos);
}

/**
 * Interpret the click on a circle and ask correspondingly.
 */
function interpretCircleClick() {
  if (countInArray(canvasArea.clicked, true) == 0) {
    return;
  }
  if (canvasArea.numCircles == 2) {
    // 2-fold
    if (canvasArea.clicked.length == 2) {
      if (countInArray(canvasArea.clicked, true) == 2) {
        pos = 12;
      } else if (canvasArea.clicked[0]) {
        pos = 1;
      } else if (canvasArea.clicked[1]) {
        pos = 2;
      }
    }
    colorPatterns();
    select2fold(pos);
  } else if (canvasArea.numCircles == 3) {
    // 3-fold
    if (canvasArea.clicked.length == 3) {
      if (countInArray(canvasArea.clicked, true) == 3) {
        pos = 123;
      } else if (canvasArea.clicked[0] & canvasArea.clicked[1]) {
        pos = 12;
      } else if (canvasArea.clicked[1] & canvasArea.clicked[2]) {
        pos = 23;
      } else if (canvasArea.clicked[0] & canvasArea.clicked[2]) {
        pos = 13;
      } else if (canvasArea.clicked[0]) {
        pos = 1;
      } else if (canvasArea.clicked[1]) {
        pos = 2;
      } else if (canvasArea.clicked[2]) {
        pos = 3;
      }
    }
    colorPatterns();
    select3fold(pos);
  } else if (canvasArea.numCircles == 4) {
    // 4-fold
    if (canvasArea.clicked.length == 4) {
      if (countInArray(canvasArea.clicked, true) == 4) {
        pos = 1234;
      } else if (
        canvasArea.clicked[0] &
        canvasArea.clicked[1] &
        canvasArea.clicked[2]
      ) {
        pos = 123;
      } else if (
        canvasArea.clicked[1] &
        canvasArea.clicked[2] &
        canvasArea.clicked[3]
      ) {
        pos = 234;
      } else if (
        canvasArea.clicked[0] &
        canvasArea.clicked[2] &
        canvasArea.clicked[3]
      ) {
        pos = 134;
      } else if (
        canvasArea.clicked[0] &
        canvasArea.clicked[1] &
        canvasArea.clicked[3]
      ) {
        pos = 124;
      } else if (canvasArea.clicked[0] & canvasArea.clicked[1]) {
        pos = 12;
      } else if (canvasArea.clicked[1] & canvasArea.clicked[2]) {
        pos = 23;
      } else if (canvasArea.clicked[2] & canvasArea.clicked[3]) {
        pos = 34;
      } else if (canvasArea.clicked[0] & canvasArea.clicked[2]) {
        pos = 13;
      } else if (canvasArea.clicked[0] & canvasArea.clicked[3]) {
        pos = 14;
      } else if (canvasArea.clicked[1] & canvasArea.clicked[3]) {
        pos = 24;
      } else if (canvasArea.clicked[0]) {
        pos = 1;
      } else if (canvasArea.clicked[1]) {
        pos = 2;
      } else if (canvasArea.clicked[2]) {
        pos = 3;
      } else if (canvasArea.clicked[3]) {
        pos = 4;
      }
    }
    colorPatterns();
    select4fold(pos);
  }
}

/**
 * Redraw the numbers on position 'pos'.
 * @param {*} pos
 */
function redrawNumber(pos) {
  var numToRedraw = new Object();
  numToRedraw[pos] = canvasArea.numbers[pos];
  drawNumbers(numToRedraw);
}

/**
 * Decides whether to select or deselect an area on pos.
 * @param  {String} pos Position of area. Could be 12, 1, 2, etc.
 */
function colorPatterns() {
  pattern = false;
  // if annotating foreground
  var fg = countInArray(canvasArea.foreground, pos) != 0;
  var bg = countInArray(canvasArea.background, pos) != 0;
  if (canvasArea.curColor == "#B1F3B1") {
    if (!fg) {
      if (bg) {
        couleur = "#ff9999";
        pattern = true;
      } else {
        couleur = canvasArea.curColor;
      }
      canvasArea.foreground.push(pos);
    } else {
      pattern = false;
      if (!bg) {
        couleur = "white";
      } else {
        couleur = "#ff9999";
      }
      removeElem(canvasArea.foreground, pos);
    }
  } else {
    if (!bg) {
      if (fg) {
        couleur = "#ff9999";
        pattern = true;
      } else {
        couleur = canvasArea.curColor;
      }
      canvasArea.background.push(pos);
    } else {
      pattern = false;
      if (!fg) {
        couleur = "white";
      } else {
        couleur = "#B1F3B1";
      }
      removeElem(canvasArea.background, pos);
    }
  }
}
