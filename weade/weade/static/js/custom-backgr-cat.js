var bgErr = false;

function customBgCat(
  browseId,
  pythonRoute,
  pythonRouteName,
  progressBar,
  clearBtn,
  info,
  seli
) {
  if (pythonRoute == "/customCats") {
    var titleHere = "Custom category name.";
    var textHere = "Please give your custom category a name:";
  } else {
    var titleHere = "Custom background name.";
    var textHere = "Please name your custom background:";
  }

  $(browseId).fileupload({
    url: pythonRoute,
    dataType: "json",
    add: function (e, data) {
      var filename = data.files[0].name;
      $(info).attr("placeholder", filename);
      data.submit();
    },
    submit: function (e, data) {
      var mimeType = data.originalFiles[0].type.split("/")[0];
      if (mimeType != "text") {
        bgErr = true;
        swal({
          title: "Error!",
          text:
            'Sorry, only text files are allowed. You have some "' +
            mimeType +
            '" type among your files.',
          type: "error",
          confirmButtonClass: "btn-default btn-primary",
          confirmButtonText: "OK"
        });
      }
    },
    progressall: function (e, data) {
      if (!bgErr) {
        // progress bar
        $(progressBar).show();
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $(progressBar + " .progress-bar ").css("width", progress + "%");
        $(progressBar + " .progress-bar").text(progress + "%");
      }
    },
    done: function (e, data) {
      var response = data.result;
      if ("error" in response) {
        swal({
          title: "File error!",
          text: response["error"],
          type: "error"
        });
        $(progressBar).fadeOut();
      } else {
        swal(
          {
            title: titleHere,
            text: textHere,
            type: "input",
            closeOnConfirm: false,
            allowEscapeKey: false,
            inputPlaceholder: "Write something"
          },
          function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
              swal.showInputError("You need to write something!");
              return false;
            }
            $(info).attr("placeholder", inputValue);

            var sel = seli[0].selectize;
            sel.addOption({ item: inputValue });
            sel.setValue(inputValue, true);

            // send species name to Python
            var toPython = new Object();
            toPython.chosenSpecies = inputValue;
            $.ajax({
              type: "POST",
              url: pythonRouteName,
              data: JSON.stringify(toPython, null, "\t"),
              contentType: "application/json;charset=UTF-8"
            });
            swal.close();
          }
        );

        $(clearBtn).fadeIn();
        if (pythonRoute == "/customCats") {
          updateCatSetTable(data.result);
        }
      }
    }
  });
}

function applyCatsEdit() {
  // send the datatable as list of rows to python
  var toPython = new Object();
  toPython.tableData = new Array();
  catSetTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
    toPython.tableData.push(this.data());
  });

  $.ajax({
    type: "POST",
    url: "/applyCatsEdit",
    data: JSON.stringify(toPython, null, "\t"),
    contentType: "application/json;charset=UTF-8"
  });
  swal(
    {
      title: "Custom category name.",
      text: "Please name your custom categories:",
      type: "input",
      closeOnConfirm: false,
      allowEscapeKey: false,
      inputPlaceholder: "Write something"
    },
    function (inputValue) {
      if (inputValue === false) return false;
      if (inputValue === "") {
        swal.showInputError("You need to write something!");
        return false;
      }

      var sel = catSel[0].selectize;
      sel.addOption({ item: inputValue });
      sel.setValue(inputValue, true);

      // send species name to Python
      var toPython = new Object();
      toPython.chosenSpecies = inputValue;
      $.ajax({
        type: "POST",
        url: "/customCatsName",
        data: JSON.stringify(toPython, null, "\t"),
        contentType: "application/json;charset=UTF-8"
      });
      swal.close();
      $("#apply_cats_edit").prop("disabled", true);
    }
  );
}
