""" User data handling in session object. """

import pandas as pd
import weade.prep_associations as preps
import weade.godb as godb
from flask import json


def initSession(session):
    """ Initialise the session object with some default values. """
    session['files'] = {}
    session['set_tiles'] = {}
    session['n_files'] = 0
    session['reg_types'] = ['is_a', 'part_of']  # default value
    session['input_type'] = 'sig'
    session['chosen_species'] = None
    session['species_link'] = None
    session['catname'] = 'General'
    session['cat_go_terms'] = None
    session['category_set'] = None
    session['genes'] = None
    session['complete_backgr'] = None
    session['direct_goterms'] = None
    session['current_analysis_name'] = None
    session['last_plot'] = None
    session['last_res'] = None
    session['last_genes'] = None
    session['history'] = {}
    session['history_iso'] = []
    session['taxids'] = {"Drosophila melanogaster (FBgn)": 7227,
                         "Danio rerio (ZFIN)": 7955,
                         "Mus musculus (ENSGMUS)": 10090,
                         "Arabidopsis thaliana(TAIR)": 3702,
                         "Caenorhabditis elegans (WB)": 6239,
                         "Homo sapiens (HGNC)": 9606,
                         "Homo sapiens (ENSG)": 9606}
    session['dropped_symbols'] = pd.DataFrame(columns=['Symbol'])
    session['custom_cats'] = {}
    session['custom_species'] = {}
    session['user'] = None


def sessionToJSON(session):
    sess = {}
    for key in session['history']:
        sess[str(key)] = session['history'][key].to_json()
    return json.dumps(sess)

def openExample_as_df(session, path):
    """ Open the example file. """
    df = pd.read_csv(path,
                     names=['Gene_ID', 'measure']).dropna(axis=1, how='all')
    return append_file(session, df)

def openIoFiles_as_df(session, fileStorageObj):
    """ Convert the Io Stream from JS to a pandas dataframe. """
    df = pd.read_csv(fileStorageObj.stream,
                     names=['Gene_ID', 'measure']).dropna(axis=1, how='all')
    return append_file(session, df)


def openBgFiles_as_df(session, fileStorageObj):
    """ Convert the background Io Stream from JS to a pandas dataframe. """
    df = pd.read_csv(fileStorageObj.stream, sep='\t',
                     names=['Gene_ID', 'GO_ID', 'Symbol']).dropna(axis=1, how='all')

    if len(list(df.columns.values)) != 3:
        df['Symbol'] = df['Gene_ID']

    offsprings = godb.get_offspring(relations=session['reg_types'])
    try:
        df_compl = pd.merge(df, offsprings, left_on='GO_ID', right_on='Offspring')[
            ['Gene_ID', 'Parent']].drop_duplicates()
    except KeyError:
        return 'error', 'Not enough columns in the file. Please upload a suitable one.'
    if df_compl.empty:
        return 'error', 'The file has a wrong format. Please revise it or upload a suitable one.'

    df_compl = df_compl.rename(columns={'Parent': 'GO_ID'})
    df_compl = pd.concat([df_compl, df])

    session['direct_goterms'] = df
    session['complete_backgr'] = df_compl
    return 'success', 'none'


def append_file(session, df):
    """ Add a new file to the data uploaded by user. """
    status = check_input_type(session, df)
    if status in ['success', 'warning']:
        session['n_files'] += 1
        # session['files'][session['n_files']] = df.drop_duplicates()
        temp = session['files']
        temp[session['n_files']] = df.drop_duplicates()
        session['files'] = temp
        return status
    else:
        return status


def check_input_type(session, df):
    """ Check if the input matches the input type. """
    if session['input_type'] is None:
        raise RuntimeError('We have no input type yet.')
    if session['input_type'] == 'sig' and df.shape[1] != 1:
        return 'warning'
    elif session['input_type'] == 'cm' and df.shape[1] != 2:
        return 'error'
    else:
        return 'success'


def reset_data(session):
    """ Reset the data when new data is coming. """
    session['files'] = {}
    session['n_files'] = 0


def convert_symbol_to_id(session):
    files = session['files']
    set_tiles = session['set_tiles']
    reset_data(session)
    symbol_geneid = session['direct_goterms']
    if session['input_type'] == 'sig':
        header = ['Gene_ID']
    else:
        header = ['Gene_ID', 'measure']
    for df in files.values():
        df = df.rename(columns={'Gene_ID': 'Symbol'})
        # convert symbol to fbgn..
        res = pd.merge(df, symbol_geneid, how='inner', on='Symbol')
        if res.empty:
            return 'error'
        # find out what we dropped
        df = df[~df['Symbol'].isin(res['Symbol'])].dropna()
        session['dropped_symbols'] = pd.concat(
            [session['dropped_symbols'], df])
        append_file(session, res[header])
    for key, df in set_tiles.items():
        if df.empty:
            continue
        df = df.rename(columns={'Gene_ID': 'Symbol'})
        res = pd.merge(df, symbol_geneid, how='inner', on='Symbol')
        if res.empty:
            return 'error'
        session['set_tiles'][key] = res[header].drop_duplicates()
    return None
