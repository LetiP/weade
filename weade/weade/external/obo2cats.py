""" Convert obo file to category file """

import os
import argparse

def parse_obo(infile):
    terms = []
    is_term = False
    curr_id = None
    with open(infile, 'r') as file:
        for line in file:
            line = line.rstrip('\n')
            if len(line) > 0:
                if line[0] == "[":
                    if line == "[Term]":
                        is_term = True
                    else:
                        is_term = False
                elif line[0:3] == "id:" and is_term:
                    curr_id = line.split(" ")[1]
                elif line[0:5] == "name:" and curr_id is not None and is_term:
                    terms.append([curr_id, line.split("name: ")[1]])
    return terms
        

def write_sql_format(terms, name, i, j):
    catset = "({},'{}',1)".format(i, name)
    catterms_l = ["({},'{}','{}','{}',{})".format(j+x, terms[x][0], terms[x][1], terms[x][1], i) for x in range(len(terms))]
    catterms = ",".join(catterms_l)
    return [catset, catterms]

def write_tsv_format(terms):
    catterms_l = ["{}\t{}\t{}".format(x[0], x[1], x[1]) for x in terms]
    return "\n".join(catterms_l)

def main():
    """ parses arguments and starts conversion"""
    parser = argparse.ArgumentParser(description="Converts obo file to category file.")
    parser.add_argument("in_path", help="Path to the obo file.")
    parser.add_argument("out_path", help="Path to the output file.")
    parser.add_argument("-s", "--sql", action="store_true",
                        help="Output for insertion in sql dump.")
    parser.add_argument("-i", "--sql_set_index", type=int, default=1,
                        help="Index for sql insertion")
    parser.add_argument("-j", "--sql_term_index", type=int, default=1,
                        help="Index for terms in sql insertion.")
    parser.add_argument("-n", "--sql_name", type=str, default="GOslim",
                        help="Name for sql insertion")
    args = parser.parse_args()
    infile = os.path.abspath(args.in_path)
    outfile = os.path.abspath(args.out_path)
    terms = parse_obo(infile)
    if args.sql:
        sql = True
        sql_index = args.sql_set_index
        sql_term_index = args.sql_term_index
        sql_name = args.sql_name
        sql_out = write_sql_format(terms, sql_name, sql_index, sql_term_index)
        print(sql_out[0])
        print(sql_out[1])
    with open(outfile, 'w') as out:
        out.write(write_tsv_format(terms))

if __name__ == '__main__':
    main()