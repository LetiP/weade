''' General functions. '''

import pandas as pd
import numpy as np
import concurrent.futures
from statsmodels.sandbox.stats.multicomp import multipletests
from scipy.stats import fisher_exact, mannwhitneyu, rankdata
from weade import tables_and_plotting as plt

import time


def prepare_sets(files, session):
    """ Subsets the datasets and counts for each area tile. """
    set_tiles = dict()

    if len(files) == 1:
        set_tiles[1] = files[1]
    elif len(files) == 2:
        A = files[1]
        B = files[2]
        # A intersect B
        set_tiles[12] = pd.merge(A, B, how='inner')
        # A minus B
        A_B = pd.merge(A, B, how='outer', indicator=True)
        set_tiles[1] = A_B[A_B['_merge'] == 'left_only']
        # B minus A
        set_tiles[2] = A_B[A_B['_merge'] == 'right_only']
    elif len(files) == 3:
        A = files[1]
        B = files[2]
        C = files[3]
        AB = pd.merge(A, B, how='inner')
        BC = pd.merge(B, C, how='inner')
        AC = pd.merge(A, C, how='inner')
        # A intersect B intersect C
        set_tiles[123] = pd.merge(AB, BC, how='inner')
        # AB minus C
        AB_C = pd.merge(AB, C, how='outer', indicator=True)
        set_tiles[12] = AB_C[AB_C['_merge'] == 'left_only'][['Gene_ID']]
        # BC minus A
        BC_A = pd.merge(BC, A, how='outer', indicator=True)
        set_tiles[23] = BC_A[BC_A['_merge'] == 'left_only'][['Gene_ID']]
        # AC minus B
        AC_B = pd.merge(AC, B, how='outer', indicator=True)
        set_tiles[13] = AC_B[AC_B['_merge'] == 'left_only'][['Gene_ID']]
        # A minus B minus C
        A_B = pd.merge(A, B, how='outer', indicator=True)
        A_B = A_B[A_B['_merge'] == 'left_only'][['Gene_ID']]
        A_B_C = pd.merge(A_B, C, how='outer', indicator=True)
        set_tiles[1] = A_B_C[A_B_C['_merge'] == 'left_only'][['Gene_ID']]
        # B minus A minus C
        B_A = pd.merge(B, A, how='outer', indicator=True)
        B_A = B_A[B_A['_merge'] == 'left_only'][['Gene_ID']]
        B_A_C = pd.merge(B_A, C, how='outer', indicator=True)
        set_tiles[2] = B_A_C[B_A_C['_merge'] == 'left_only'][['Gene_ID']]
        # C minus A minus B
        C_A = pd.merge(C, A, how='outer', indicator=True)
        C_A = C_A[C_A['_merge'] == 'left_only'][['Gene_ID']]
        C_A_B = pd.merge(C_A, B, how='outer', indicator=True)
        set_tiles[3] = C_A_B[C_A_B['_merge'] == 'left_only'][['Gene_ID']]
    elif len(files) == 4:
        A = files[1]
        B = files[2]
        C = files[3]
        D = files[4]
        AB = pd.merge(A, B, how='inner')
        BC = pd.merge(B, C, how='inner')
        CD = pd.merge(C, D, how='inner')
        AC = pd.merge(A, C, how='inner')
        AD = pd.merge(A, D, how='inner')
        BD = pd.merge(B, D, how='inner')

        ABC = pd.merge(AB, BC, how='inner')
        BCD = pd.merge(BC, CD, how='inner')
        ACD = pd.merge(AC, AD, how='inner')
        ABD = pd.merge(AB, AD, how='inner')
        # ABCD
        set_tiles[1234] = pd.merge(ABC, BCD, how='inner')
        # ABC - D
        ABC_D = pd.merge(ABC, set_tiles[1234], how='outer', indicator=True)
        set_tiles[123] = ABC_D[ABC_D['_merge'] == 'left_only'][['Gene_ID']]
        BCD_A = pd.merge(BCD, set_tiles[1234], how='outer', indicator=True)
        set_tiles[234] = BCD_A[BCD_A['_merge'] == 'left_only'][['Gene_ID']]
        ACD_B = pd.merge(ACD, set_tiles[1234], how='outer', indicator=True)
        set_tiles[134] = ACD_B[ACD_B['_merge'] == 'left_only'][['Gene_ID']]
        ABD_C = pd.merge(ABD, set_tiles[1234], how='outer', indicator=True)
        set_tiles[124] = ABD_C[ABD_C['_merge'] == 'left_only'][['Gene_ID']]
        # AB - CD
        CuD = pd.merge(C, D, how='outer')
        AuB = pd.merge(A, B, how='outer')
        AuD = pd.merge(A, D, how='outer')
        BuD = pd.merge(B, D, how='outer')
        BuC = pd.merge(B, C, how='outer')
        AuC = pd.merge(A, C, how='outer')
        AB_CD = pd.merge(AB, CuD, how='outer', indicator=True)
        set_tiles[12] = AB_CD[AB_CD['_merge'] == 'left_only'][['Gene_ID']]
        CD_AB = pd.merge(CD, AuB, how='outer', indicator=True)
        set_tiles[34] = CD_AB[CD_AB['_merge'] == 'left_only'][['Gene_ID']]
        BC_AD = pd.merge(BC, AuD, how='outer', indicator=True)
        set_tiles[23] = BC_AD[BC_AD['_merge'] == 'left_only'][['Gene_ID']]
        AD_BC = pd.merge(AD, BuC, how='outer', indicator=True)
        set_tiles[14] = AD_BC[AD_BC['_merge'] == 'left_only'][['Gene_ID']]
        AC_BD = pd.merge(AC, BuD, how='outer', indicator=True)
        set_tiles[13] = AC_BD[AC_BD['_merge'] == 'left_only'][['Gene_ID']]
        BD_AC = pd.merge(BD, AuC, how='outer', indicator=True)
        set_tiles[24] = BD_AC[BD_AC['_merge'] == 'left_only'][['Gene_ID']]
        # A
        BuCuD = pd.merge(BuD, C, how='outer')
        A_ = pd.merge(A, BuCuD, how='outer', indicator=True)
        set_tiles[1] = A_[A_['_merge'] == 'left_only'][['Gene_ID']]
        AuCuD = pd.merge(AuD, C, how='outer')
        B_ = pd.merge(B, AuCuD, how='outer', indicator=True)
        set_tiles[2] = B_[B_['_merge'] == 'left_only'][['Gene_ID']]
        AuBuD = pd.merge(AuD, B, how='outer')
        C_ = pd.merge(C, AuBuD, how='outer', indicator=True)
        set_tiles[3] = C_[C_['_merge'] == 'left_only'][['Gene_ID']]
        AuBuC = pd.merge(AuB, C, how='outer')
        D_ = pd.merge(D, AuBuC, how='outer', indicator=True)
        set_tiles[4] = D_[D_['_merge'] == 'left_only'][['Gene_ID']]

    session['set_tiles'] = set_tiles


def show_numbers(session):
    """ Prepare numbers to send to JS. """
    set_nums = dict()
    tiles = session['set_tiles']
    for key in tiles:
        set_nums[key] = tiles[key].shape[0]

    return set_nums


def get_fg_genes(session, selected_fg):
    """ Get the foreground genes the user selected. """
    genes = None

    files = session['files']
    if not selected_fg:  # empty list, no selection
        for key in files:
            if genes is not None:
                genes = pd.merge(genes, files[key], how='outer')
            else:
                genes = files[key]
    else:
        for sel in selected_fg:
            if genes is not None:
                genes = pd.merge(genes, session['set_tiles'][sel], how='outer')
            else:
                genes = session['set_tiles'][sel]
    session['user_genes'] = genes

    genes = genes[genes['Gene_ID'].isin(
        session['direct_goterms']['Gene_ID'])].drop_duplicates()

    return genes


def get_bg(session, fg, selected_bg):
    """
    Get the background the user selected or the whole universe, if
    none selected.
    """
    genes = None
    if selected_bg:
        for sel in selected_bg:
            if genes is not None:
                genes = pd.merge(genes, session['set_tiles'][sel], how='outer')
            else:
                genes = session['set_tiles'][sel]
        # add fg to bg
        genes = pd.merge(genes, fg, how='outer')
        # filter universe genes out that are not in selected background
        genes = pd.merge(
            genes, session['complete_backgr'], how='inner', on=['Gene_ID'])
    else:
        genes = session['complete_backgr']
    return genes


def run_ana_cats(session, genes, background, cat_go_terms, alternative):
    ''' Run the statistical tests on one category. '''
    t1 = time.time()
    n_genes_in_sample = genes.shape[0]
    if session['input_type'] == 'sig':
        # for calculation of D-value
        n_genes_anno = pd.unique(background['Gene_ID']).shape[0]

    cat_go_terms = cat_go_terms[['GO_ID', 'Category']]
    # filter background for genes already in categories
    background = pd.merge(background[['GO_ID', 'Gene_ID']], cat_go_terms,
                          on='GO_ID').drop_duplicates()

    if session['input_type'] == 'sig':
        is_fisher = True
        test_res = pd.DataFrame(columns=['Category', 'p-Value', 'n. Sample',
                                         'n. Sample Category', 'n. Category'])
        # for C -value
        for cat in pd.unique(cat_go_terms['Category']):
            # get terms for specified category
            go_terms = cat_go_terms[cat_go_terms['Category'] == cat][[
                'GO_ID']].drop_duplicates()
            p_val, n_sample_cat, n_cat = calc_fisher_cat(
                genes, go_terms, background,
                n_genes_anno, n_genes_in_sample, alternative)
            test_res = test_res.append({'Category': cat,
                                        'p-Value': p_val,
                                        'n. Sample': n_genes_in_sample,
                                        'n. Sample Category': n_sample_cat,
                                        'n. Category': n_cat}, ignore_index=True)

        print(time.time() - t1, 'Ran Fisher tests')
        # Benjamini/Hochberg p-val correction
        test_res['p-Value'] = multipletests(
            test_res['p-Value'], method='fdr_bh')[1]

    else:
        is_fisher = False
        genes = genes.assign(rank=pd.Series(
            rankdata(genes['measure'])).values)
        test_res = pd.DataFrame(columns=['Category', 'p-Value', 'delta Rank', 'n. Sample',
                                         'n. Sample Category', 'n. Category'])

        for cat in pd.unique(cat_go_terms['Category']):
            # get terms for specified category
            go_terms = cat_go_terms[cat_go_terms['Category'] == cat][[
                'GO_ID']].drop_duplicates()
            p_val, delta_rank, n_sample_cat, n_cat = calc_mwu_cat(
                genes, go_terms, background)
            test_res = test_res.append({'Category': cat,
                                        'p-Value': p_val,
                                        'delta Rank': delta_rank,
                                        'n. Sample': n_genes_in_sample,
                                        'n. Sample Category': n_sample_cat,
                                        'n. Category': n_cat}, ignore_index=True)
        print(time.time() - t1, 'Ran MWU tests')
    test_res['p-Value'] = test_res['p-Value'].astype('float64')
    return test_res, is_fisher


def make_cats_res(session, test_res, is_fisher):
    ''' Render the category result plot and table. '''
    # make Enrichment results plot
    plot = plt.enrichment_res_plot(
        test_res, fisher=is_fisher, analysis_name=session['current_analysis_name'])
    session["last_plot"] = plot
    # make Enrichment results table
    enrich_table = plt.df_to_table(test_res, 'enrichTable')
    session["last_res"] = enrich_table
    return plot, enrich_table


def calc_fisher_cat(genes, cat_go_terms, background, n_genes_anno, n_genes_in_sample, alt):
    """ Perform the Fisher test on a category. """
    # get all genes annotated with a specific category
    genes_in_cat = pd.merge(background, cat_go_terms, on='GO_ID')[
        ['Gene_ID']].drop_duplicates()

    # num.genes.sample.category
    A = pd.merge(genes_in_cat, genes, how='inner').shape[0]
    # num.genes.in.category -A
    B = genes_in_cat.shape[0] - A
    # num.genes.in.sample -A
    C = n_genes_in_sample - A
    # num.genes.annotated - num.genes.in.sample - B
    D = n_genes_anno - C + A - B

    if A > 0 and B > 0:
        res = fisher_exact(np.array([[A, B], [C, D]]), alternative=alt)
        return res[1], A, A + B  # p-value
    else:
        return 1, 0, 0


def calc_mwu_cat(genes, cat_go_terms, background):
    """ Performs Mann-Whitney U-test on a multi-term category. """
    # get all genes annotated with a specific category
    genes_in_cat = pd.merge(background, cat_go_terms, on='GO_ID')[
        ['Gene_ID']].drop_duplicates()

    # genes in sample and with term (intersection)
    sample_term = pd.merge(genes, genes_in_cat, how='inner', on='Gene_ID')
    # genes in sample but not with term (difference)
    sample_noterm = pd.merge(genes, genes_in_cat, how='outer', indicator=True)
    sample_noterm = sample_noterm[sample_noterm['_merge']
                                  == 'left_only'][['Gene_ID', 'measure', 'rank']]

    n1 = sample_term.shape[0]
    if n1 == 0:
        return 1., 0, 0, genes_in_cat.shape[0]
    
    res = mannwhitneyu(sample_term['measure'],
                       sample_noterm['measure'], alternative='two-sided')

    delta_rank = np.sum(sample_term['rank']) / n1 - \
        np.sum(sample_noterm['rank']) / sample_noterm.shape[0]
    return res[1], delta_rank, n1, genes_in_cat.shape[0]
        


def calc_fisher_term(term, genes, background, n_genes_anno, n_genes_in_sample, alt):
    """ Perform the Fisher test on a single GO-term. """
    # get all genes annotated with a specific term
    genes_with_term = background[background['GO_ID'] == term][[
        'Gene_ID']].drop_duplicates()

    # num.genes.sample.category
    A = pd.merge(genes_with_term, genes, how='inner').shape[0]
    # num.genes.in.category -A
    B = genes_with_term.shape[0] - A
    # num.genes.in.sample -A
    C = n_genes_in_sample - A
    # num.genes.annotated - num.genes.in.sample - B
    D = n_genes_anno - C + A - B

    res = fisher_exact(np.array([[A, B], [C, D]]), alternative=alt)
    return res[1], A, A + B  # p-value


def calc_mwu_term(term, genes, cat_go_terms, background):
    """ Performs Mann-Whitney U-test on a single GO-term. """
    # get all genes annotated with a specific term
    genes_with_term = background[background['GO_ID'] == term][[
        'Gene_ID']].drop_duplicates()

    # genes in sample and with term (intersection)
    sample_term = pd.merge(genes, genes_with_term, how='inner')
    # genes in sample but not with term (difference)
    sample_noterm = pd.merge(genes, genes_with_term,
                             how='outer', indicator=True)
    sample_noterm = sample_noterm[sample_noterm['_merge']
                                  == 'left_only'][['Gene_ID', 'measure', 'rank']]

    res = mannwhitneyu(sample_term['measure'],
                       sample_noterm['measure'], alternative='two-sided')

    n1 = sample_term.shape[0]
    delta_rank = np.sum(sample_term['rank']) / n1 - \
        np.sum(sample_noterm['rank']) / sample_noterm.shape[0]

    return res[1], delta_rank, n1, genes_with_term.shape[0]


def calc_fisher_term_multip(go_terms, genes, background, n_genes_anno, n_genes_in_sample, alt):
    """ Perform the Fisher test on a single GO-term while using Multiprocessing. """
    test_res = []
    for term in go_terms:
        # get all genes annotated with a specific term
        genes_with_term = background[background['GO_ID'] == term][[
            'Gene_ID']].drop_duplicates()

        # num.genes.sample.category
        A = pd.merge(genes_with_term, genes, how='inner').shape[0]
        # num.genes.in.category -A
        B = genes_with_term.shape[0] - A
        # num.genes.in.sample -A
        C = n_genes_in_sample - A
        # num.genes.annotated - num.genes.in.sample - B
        D = n_genes_anno - C + A - B

        res = fisher_exact(np.array([[A, B], [C, D]]), alternative=alt)
        test_res.append((term, res[1], A, A + B))
    return test_res


def calc_mwu_term_multip(go_terms, genes, cat_go_terms, background):
    """ Performs Mann-Whitney U-test on single GO-term while using Multiprocessing. """
    test_res = []
    for term in go_terms:
        # get all genes annotated with a specific term
        genes_with_term = background[background['GO_ID'] == term][[
            'Gene_ID']].drop_duplicates()

        # genes in sample and with term (intersection)
        sample_term = pd.merge(genes, genes_with_term, how='inner')
        # genes in sample but not with term (difference)
        sample_noterm = pd.merge(
            genes, genes_with_term, how='outer', indicator=True)
        sample_noterm = sample_noterm[sample_noterm['_merge']
                                      == 'left_only'][['Gene_ID', 'measure', 'rank']]

        res = mannwhitneyu(sample_term['measure'],
                           sample_noterm['measure'], alternative='two-sided')

        n1 = sample_term.shape[0]
        delta_rank = np.sum(sample_term['rank']) / n1 - \
            np.sum(sample_noterm['rank']) / sample_noterm.shape[0]

        test_res.append((term, res[1], delta_rank,
                         n1, genes_with_term.shape[0]))

    return test_res


def run_fisher_single_process(go_terms, genes, background, n_genes_anno, n_genes_in_sample, alternative):
    """ Bring the data into right format to perform the fisher test single process. """
    test_res = pd.DataFrame(columns=['Term', 'p-Value', 'n. Sample',
                                     'n. Sample Term', 'n. Term'])
    for term in go_terms:
        p_val, n_sample_cat, n_cat = calc_fisher_term(term, genes, background,
                                                      n_genes_anno, n_genes_in_sample, alternative)
        test_res = test_res.append({'Term': term,
                                    'p-Value': p_val,
                                    'n. Sample': n_genes_in_sample,
                                    'n. Sample Term': n_sample_cat,
                                    'n. Term': n_cat}, ignore_index=True)
    return test_res


def run_mwu_single_process(genes, go_terms, background, n_genes_in_sample):
    """ Bring the data into right format to perform the MWU test single process. """
    genes = genes.assign(rank=pd.Series(rankdata(genes['measure'])).values)
    test_res = pd.DataFrame(columns=['Term', 'p-Value', 'delta Rank', 'n. Sample',
                                     'n. Sample Term', 'n. Term'])
    for term in go_terms:
        p_val, delta_rank, n_sample_cat, n_cat = calc_mwu_term(
            term, genes, go_terms, background)
        test_res = test_res.append({'Term': term,
                                    'p-Value': p_val,
                                    'delta Rank': delta_rank,
                                    'n. Sample': n_genes_in_sample,
                                    'n. Sample Term': n_sample_cat,
                                    'n. Term': n_cat}, ignore_index=True)
    return test_res


def run_fisher_multi_process(go_terms, genes, background, n_genes_anno, n_genes_in_sample, alternative, n_worker):
    """ Bring the data into right format to perform the fisher test while using multiprocessing. """
    test_res = pd.DataFrame(columns=['GO_ID', 'p-Value', 'n. Sample',
                                     'n. Sample Term', 'n. Term'])
    n = go_terms.shape[0] // n_worker

    ExecutorType = concurrent.futures.ProcessPoolExecutor
    with ExecutorType() as executor:
        jobs = []
        for i in range(n_worker):
            if i < n_worker - 1:
                terms_list = go_terms[i * n:(i + 1) * n]
            else:
                terms_list = go_terms[i * n:]
            jobs.append(executor.submit(calc_fisher_term_multip, terms_list, genes,
                                        background, n_genes_anno, n_genes_in_sample, alternative))
        for job in concurrent.futures.as_completed(jobs):
            res = job.result()
            for elem in res:
                term, p_val, n_sample_cat, n_cat = elem
                test_res = test_res.append({'GO_ID': term,
                                            'p-Value': p_val,
                                            'n. Sample': n_genes_in_sample,
                                            'n. Sample Term': n_sample_cat,
                                            'n. Term': n_cat}, ignore_index=True)
    return test_res


def run_mwu_multi_process(genes, go_terms, background, n_genes_in_sample, n_worker):
    """ Bring the data into right format to perform the MWU test while using multiprocessing. """
    genes = genes.assign(rank=pd.Series(rankdata(genes['measure'])).values)
    test_res = pd.DataFrame(columns=['GO_ID', 'p-Value', 'delta Rank', 'n. Sample',
                                     'n. Sample Term', 'n. Term'])
    n = go_terms.shape[0] // n_worker

    ExecutorType = concurrent.futures.ProcessPoolExecutor
    with ExecutorType() as executor:
        jobs = []
        for i in range(n_worker):
            if i < n_worker - 1:
                terms_list = go_terms[i * n:(i + 1) * n]
            else:
                terms_list = go_terms[i * n:]
            jobs.append(executor.submit(calc_mwu_term_multip,
                                        terms_list, genes, go_terms, background))
        for job in concurrent.futures.as_completed(jobs):
            res = job.result()
            for elem in res:
                term, p_val, delta_rank, n_sample_cat, n_cat = elem
                test_res = test_res.append({'GO_ID': term,
                                            'p-Value': p_val,
                                            'delta Rank': delta_rank,
                                            'n. Sample': n_genes_in_sample,
                                            'n. Sample Term': n_sample_cat,
                                            'n. Term': n_cat}, ignore_index=True)
    return test_res


def calc_pathway_enrichment(pairs, pw_data, species, genes_sample, n_genes_anno):
    """ Calculate enrichment of genes belonging to pathways. """
    pw_names = pw_data[['pathway', 'name', 'link']].drop_duplicates()
    pw_links = pw_names.set_index('pathway').to_dict()['link']
    pw_names = pw_names.set_index('pathway').to_dict()['name']
    t1 = time.time()
    n_genes_sample = genes_sample.shape[0]
    calculated_pws = {}
    pair_results = []
    n_sig = []
    for _, pair in pairs[['interactor_a', 'interactor_b']].iterrows():
        gene1 = pair[0]
        gene2 = pair[1]
        pw_g1 = pw_data[pw_data.gene == gene1]
        pw_g2 = pw_data[pw_data.gene == gene2]
        shared_pw = pd.merge(pw_g1, pw_g2, how='inner', on='pathway').pathway.drop_duplicates()
        pw_results = []
        for pw in shared_pw:
            if pw in calculated_pws:
                pw_results.append([pw, calculated_pws[pw]])
            else:
                genes_pw = pw_data[pw_data.pathway == pw].gene.drop_duplicates()
                n_genes_pw = genes_pw.shape[0]
                A = genes_pw[genes_pw.isin(genes_sample.Gene_ID)].shape[0]
                B = n_genes_pw - A
                C = n_genes_sample - A
                D = n_genes_anno - C + A - B
                if A > 0 and B > 0:
                    res = fisher_exact(np.array([[A, B], [C, D]]), alternative='greater')
                    pw_results.append([pw, res[1]])
                    calculated_pws[pw] = res[1]
                else:
                    pw_results.append([pw, 1])
                    calculated_pws[pw] = 1
        if len(pw_results) == 0:
            pair_results.append("No information on pathways found.")
            n_sig.append(0)
        else:
            df = pd.DataFrame(np.array(pw_results), columns=['pathway', 'p'])
            df.p = df.p.astype('float64')
            sig = df[df.p <= 0.05]
            sig = sig.sort_values(by='p')
            n_sig.append(sig.shape[0])
            pair_results.append(", ".join('<a href="' + pw_links[x] + '" target="_blank">' + pw_names[x] +
                                ' (' + str(sig.p.iat[idx]) + ')</a>' for idx, x in enumerate(sig.pathway)))
    pairs['n_enriched'] = n_sig
    pairs['enriched_pathways'] = pair_results
    print('calculated pathway enrichment in: ', time.time() - t1)
    return pairs
