"""
The flask application package.
"""

# import os
# from os import mkdir, chmod
# from os.path import exists
# import sys
from flask import Flask
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
# from weade.api.v1_0.api import api as api_v1_0

app = Flask(__name__)

app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# if sys.path[0] == "/var/www/weade/weade":
#     app.config['SESSION_TYPE'] = 'redis'
#     # app.config['SESSION_TYPE'] = 'filesystem'
#     # flask_dir = os.path.join(sys.path[0], 'flask_session/')
#     # app.config['SESSION_FILE_DIR'] = flask_dir
#     # app.config['SESSION_FILE_MODE'] = int('0700', 8)
# else:
#     app.config['SESSION_TYPE'] = 'filesystem'
#     flask_dir = os.path.join(sys.path[0], 'flask_session/')
#     app.config['SESSION_FILE_DIR'] = flask_dir
# app.config['SESSION_PERMANENT'] = False

Session(app)


import weade.views
# app.register_blueprint(api_v1_0, url_prefix='/api/v1.0')
