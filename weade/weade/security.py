import base64
import hmac
from os import urandom
from werkzeug.security import safe_str_cmp
from scrypt import hash as scrypt_hash
from weade.mysqlfun import MysqlData

def debase64(byte_str):
    """ Decode base64 encoded bytes/strings """

    if isinstance(byte_str, str):
        byte_str = bytes(byte_str, 'utf-8')
    return base64.b64decode(byte_str)

def enbase64(byte_str):
    """ Encode bytes/strings to base64 """
    if isinstance(byte_str, str):
        byte_str = bytes(byte_str, 'utf-8')
    return base64.b64encode(byte_str)

def generate_random_salt(byte_size=64):
    """ Generate random salt to use with generate_password_hash() """
    return enbase64(urandom(byte_size))

def generate_password_hash(password, salt, N=1 << 14, r=8, p=1, buflen=64):
    """
    Generate password hash given the password string and salt.
    Args:
        ``password``: Password string.
        ``salt`` : Random base64 encoded string.
    Optional args:
        ``N`` : the CPU cost, must be a power of 2 greater than 1, defaults to 1 << 14.
        ``r`` : the memory cost, defaults to 8.
        ``p`` : the parallelization parameter, defaults to 1.
    The parameters r, p, and buflen must satisfy r * p < 2^30 and buflen <= (2^32 - 1) * 32.
    """

    pw_hash = scrypt_hash(password, salt, N, r, p, buflen)
    return enbase64(pw_hash)

def check_password_hash(password, password_hash, salt, N=1 << 14, r=8, p=1, buflen=64):
    """ Given a password, hash, salt this function verifies the password is equal to hash/salt. """
    candidate_hash = generate_password_hash(password, salt, N, r, p, buflen)
    return safe_str_cmp(password_hash, candidate_hash)

def user_exists(u_name):
    """ True if user name is already in use """
    sql = MysqlData()
    query = "SELECT * FROM tbl_user WHERE user_name = %s"
    sql.runQuery_udata(query, (u_name,))
    return len(sql.data) != 0

def email_exists(email):
    """ True if email already in use """
    sql = MysqlData()
    query = "SELECT * FROM tbl_user WHERE user_email = %s"
    sql.runQuery_udata(query, (email,))
    return len(sql.data) != 0

def register_user(u_name, pw, email):
    """ add user to data base """
    # encrypt the password
    salt = generate_random_salt()
    pw_hash = generate_password_hash(pw, salt)
    sql = MysqlData()
    ret = sql.runProc('sp_regUser', [u_name, pw_hash, salt, email])
    return ret == 0

def login_user(u_name, pw):
    """ Verify password """
    sql = MysqlData()
    query = "SELECT user_password, user_salt FROM tbl_user WHERE user_name = %s"
    sql.runQuery_udata(query, (u_name,))
    if len(sql.data) != 0:
        pw_hash, salt = sql.data[0]
        return check_password_hash(pw, pw_hash, salt)
    else:
        return False

def save_session(u_name, json_data):
    """ save session object to data base """
    sql = MysqlData()
    query = "SELECT user_id FROM tbl_user WHERE user_name = %s"
    sql.runQuery_udata(query, (u_name,), close=False)
    if len(sql.data) == 0:
        return False
    u_id = sql.data[0][0]
    ret = sql.runProc('sp_saveSession', [u_id, json_data])
    print(ret)
    return ret == 0
