"""
Routes and views for the flask application.
"""

import os, sys
from datetime import datetime
from flask import render_template, json, request, flash, redirect, session, url_for
from markupsafe import Markup
import pandas as pd
from scipy.stats import rankdata
import numpy as np
import time
import uuid

from weade import app
import weade.prep_associations as preps
import weade.misc as misc
import weade.functions as fct
import weade.session_data as sess
from weade.history import History_item
from weade import tables_and_plotting as plt
# from weade.security import register_user, login_user, user_exists, save_session
from scipy.stats import fisher_exact


@app.route('/showRoutes')
def showRoutes():
    import urllib
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.parse.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
        output.append(line)

    for line in sorted(output):
        print(line)
    return '', 204


@app.route('/')
@app.route('/home')
def home():
    """ Renders the home page."""
    return render_template(
        'index.html',
        title='WEADE - Workflow for Enrichment Analysis and Data Exploration',
        year=datetime.now().year
    )


@app.route('/documentation')
def documentation():
    """ Renders the documentation page."""
    return render_template(
        'documentation.html',
        title='WEADE - Documentation',
        year=datetime.now().year
    )

@app.route('/termsOfUse')
def termsOfUse():
    """ Renders the terms of use page."""
    return render_template(
        'terms.html',
        title='WEADE - Terms of use',
        year=datetime.now().year
    )

@app.route('/weade')
def weade():
    """ Load main app page. """
    return render_template(
        'main.html',
        title='WEADE - Workflow for Enrichment Analysis and Data Exploration',
        year=datetime.now().year
    )


@app.route('/getCategories', methods=['GET'])
def getCategories():
    """ Get available categories to fill dropdown list. """
    t1 = time.time()
    sess.initSession(session)
    print("initSession", time.time() - t1)
    # load categories from mysql
    session["cat_go_terms"], session['category_set'] = preps.retrieve_categories(
        session['catname'])
    print("prepcats", time.time() - t1)
    # load species from mysql
    cat_choices = preps.list_available_categories()

    # prepare category set table
    cat_set_table = plt.df_to_table(session['category_set'], 'catSetTable')
    return json.dumps({'catChoices': cat_choices, 'catSetTable': cat_set_table})


@app.route('/useCategory', methods=['POST'])
def useCategory():
    """ Get the offsprings for the chosen categories. """
    chosen_category = request.json['chosenCategory']
    reg_types = request.json['regTypes']

    if chosen_category != session['catname'] or reg_types != session['reg_types']:
        if chosen_category in session['custom_cats'].keys():
            session["cat_go_terms"], session['category_set'] = session['custom_cats'][chosen_category]
        else:
            session["cat_go_terms"], session['category_set'] = preps.retrieve_categories(
                chosen_category, reg_types=reg_types)
        # prepare category set table
        cat_set_table = plt.df_to_table(session['category_set'], 'catSetTable')
        session['reg_types'] = reg_types
        session['catname'] = chosen_category
        return json.dumps({'catSetTable': cat_set_table})
    else:
        return json.dumps({'result': 'blaa'})


@app.route('/getSpecies', methods=['GET'])
def getSpecies():
    """ Get available species to fill the dropdown list. """
    species = preps.list_available_species()

    return json.dumps({'species': species})


@app.route('/useExampleData', methods=['POST'])
def useExampleData():
    """ Use example file provided. """
    upload_status = sess.openExample_as_df(
        session, os.path.join(sys.path[0], 'weade/ga/dmel_gro_FBgn.txt'))
    upload_status = sess.openExample_as_df(
        session, os.path.join(sys.path[0], 'weade/ga/dmel_repro_FBgn.txt'))

    return json.dumps({'uploadStatus': upload_status})


@app.route('/upload', methods=['POST'])
def upload():
    """ Handle uploaded files by user. """
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    up_file = request.files['file']
    # if user does not select file, browser also
    # submit a empty part without filename
    if up_file.filename == '':
        flash('No selected file')
        return redirect(request.url)

    upload_status = sess.openIoFiles_as_df(session, up_file)
    return json.dumps({'uploadStatus': upload_status})


@app.route('/resetUpload', methods=['GET'])
def resetUpload():
    """
    Reset the uploaded data from the user to nothing when
    they uploads new files.
    """
    sess.reset_data(session)
    return json.dumps({'result': 'blaa'})


@app.route('/customBackground', methods=['POST'])
def customBackground():
    """ Upload custom association file. """
    up_file = request.files['bg']
    if up_file and up_file.mimetype.split('/')[0] == 'text':
        upload_status, message = sess.openBgFiles_as_df(session, up_file)
        return json.dumps({upload_status: message})
    else:
        return json.dumps({'error': 'Not a text mimetype.'})


@app.route('/customSpeciesName', methods=['POST'])
def customSpeciesName():
    """ Set the custom species name. """
    session['chosen_species'] = request.json['chosenSpecies']

    temp = session['custom_species']
    temp[session['chosen_species']] = [session['complete_backgr'],
                                       session['direct_goterms'], session['species_link']]
    session['custom_species'] = temp
    return '', 204


@app.route('/customCats', methods=['POST'])
def customCats():
    """ Upload custom categories file. """
    up_file = request.files['userCatUpload']
    if up_file and up_file.mimetype.split('/')[0] == 'text':
        session['catname'] = 'tbd'
        # check if file has enough columns
        try:
            df = pd.read_csv(up_file.stream, sep='\t',
                             names=['GO_ID', 'Ontology', 'Category']).dropna(axis=1, how='all')
            df = df[['GO_ID', 'Category']]
        except KeyError:
            return json.dumps({'error': 'Not enough columns in the file. Please upload a suitable one.'})

        session["cat_go_terms"], session['category_set'] = preps.retrieve_categories(
            df, session['reg_types'])

        # check if resulting dataframes are empty
        if session["cat_go_terms"].empty or session['category_set'].empty:
            return json.dumps({'error': 'The file has a wrong format. Please revise it or upload a suitable one.'})

        # prepare category set table
        cat_set_table = plt.df_to_table(session['category_set'], 'catSetTable')
        return json.dumps({'catSetTable': cat_set_table})
    else:
        return json.dumps({'error': 'Not a text mimetype.'})


@app.route('/customCatsName', methods=['POST'])
def customCatsName():
    """ Set the custom category name. """
    session['catname'] = request.json['chosenSpecies']

    temp = session['custom_cats']
    temp[session['catname']] = [
        session["cat_go_terms"], session['category_set']]
    session['custom_cats'] = temp
    return '', 204


@app.route('/applyCatsEdit', methods=['POST'])
def applyCatsEdit():
    """ Upload custom categories file. """

    df = request.json['tableData']
    df = pd.DataFrame(df, columns=['GO_ID', 'Ontology', 'Name', 'Category'])
    session["cat_go_terms"], session['category_set'] = preps.retrieve_categories(
        df[['GO_ID', 'Category']], session['reg_types'])

    return '', 204


@app.route('/getDroppedInfo', methods=['POST'])
def getDroppedInfo():
    ''' Search for matches to show in dropdown in dropped symbols table. '''
    typed = request.json['typed']

    series = session['direct_goterms']['Symbol']
    mask = series.str.contains(typed, case=False)
    suggestion = series[mask].head(15).tolist()

    return json.dumps({'suggestion': suggestion})


@app.route('/addDroppedRecovered', methods=['POST'])
def addDroppedRecovered():
    ''' Lost and found. '''
    value = request.json['value']

    symbol_geneid = session['direct_goterms']
    symbol = symbol_geneid[symbol_geneid['Symbol'] == value][['Gene_ID']]

    if session['input_type'] == 'cm':
        symbol['measure'] = request.json['measure']
    temp = session['files']
    temp[1] = pd.concat([temp[1], symbol])
    session['files'] = temp

    genes = fct.get_fg_genes(session, session['selected_fg'])
    session['background'] = fct.get_bg(session, genes, session['selected_bg'])
    session['genes'] = genes
    return '', 204


@app.route('/getTableEditInfos', methods=['POST'])
def getTableEditInfos():
    """ Get infos to fill in dropdown. """
    typed = request.json['typed']
    clicked_col = request.json['clickedColumn']
    suggestion = preps.search_matches(typed, clicked_col)

    return json.dumps({'suggestion': suggestion})


@app.route('/getCorrespondingGoidNameOnto', methods=['POST'])
def getCorrespondingGoidNameOnto():
    """ Get the corresponding goid/name and onto. """
    value = request.json['value']
    clicked_col = request.json['clickedColumn']
    compl_value, onto = preps.search_value(value, clicked_col)

    return json.dumps({'suggestion': compl_value, 'onto': onto})


@app.route('/inputType', methods=['POST'])
def inputType():
    """
    Get the Input Type from switcher checkboxes and check if this
    matches the files from the user.
    """
    session['input_type'] = request.json['inputType']
    return '', 204


@app.route('/showSets')
def showSets():
    """
    Display Venn Diagram (done mostly in JS). Here: calculate elements of each
    surface in Venn.
    """
    fct.prepare_sets(session['files'], session)
    numbers = fct.show_numbers(session)

    return json.dumps({'numbers': numbers})


@app.route('/applySets', methods=['POST'])
def applySets():
    """ Calculate foreground and background from selection. """
    selected_fg = request.json['foreground']
    selected_bg = request.json['background']
    # get the species association
    session['current_analysis_name'] = str(request.json['analysisName'])

    # handle using gene symbol
    use_gene_symbol = request.json['useGeneSymbol']
    if use_gene_symbol:
        status = sess.convert_symbol_to_id(session)
        if status == 'error':
            return json.dumps({'error': 'Are you sure? You seem to be using identifiers, not gene symbols.'})
        # for recovering dropped symbols
        session['selected_fg'] = selected_fg
        session['selected_bg'] = selected_bg
    else:
        # empty
        session['dropped_symbols'] = pd.DataFrame(columns=['Symbol'])
    genes = fct.get_fg_genes(session, selected_fg)
    if genes.empty:
        return json.dumps({'error': 'Are you sure? You seem to be using gene symbols instead of identifiers.'})
    session['background'] = fct.get_bg(session, genes, selected_bg)
    session['genes'] = genes
    return '', 204


@app.route('/whereAreMyGenes', methods=['GET'])
def whereAreMyGenes():
    """ Compute genes that are not found again by user. """
    genes = session['user_genes']
    genes = genes[~genes['Gene_ID'].isin(
        session['complete_backgr']['Gene_ID'])].dropna()
    if not genes.empty:
        user_genes_table = plt.df_to_table(genes, 'whereGenesTable')
    else:
        user_genes_table = 'empty'

    dropped = session['dropped_symbols']
    if not dropped.empty:
        dropped_symbols_table = plt.df_to_table(dropped, 'droppedSymbolsTable')
    else:
        dropped_symbols_table = 'empty'

    return json.dumps({'whereGenes': user_genes_table, 'droppedSymbols': dropped_symbols_table})


@app.route('/readSpecies', methods=['POST'])
def readSpecies():
    """ Read the selected species while the user looks somewhere else."""
    chosen_species = request.json['chosenSpecies']
    # if chosen_species != session['chosen_species']:
    session['chosen_species'] = chosen_species
    if chosen_species in session['custom_species'].keys():
        session['complete_backgr'], session['direct_goterms'], session['species_link'] = session['custom_species'][chosen_species]
    else:
        session['complete_backgr'], session['direct_goterms'], session['species_link'] = preps.read_species(
            chosen_species)
    return json.dumps({'speciesLink': session['species_link']})


@app.route('/startAnalysisCategories', methods=['POST'])
def startAnalysisCategories():
    """ Run enrichment analysis on categories. """

    # first filter for the ontology
    onto = request.json['checkedOnto']
    alternative = request.json['alternative']
    if len(onto) != 3:
        cat_go_terms = session["category_set"][session["category_set"]['Ontology'].isin(
            onto)]
    else:
        cat_go_terms = session["category_set"]

    genes = session['genes']
    background = session['background']
    session['cat_go_terms_in_onto'] = cat_go_terms
    session['alternative'] = alternative
    session['onto'] = onto

    test_res, is_fisher = fct.run_ana_cats(
        session, genes, background, cat_go_terms, alternative)

    item = History_item(genes,
                        background,
                        session['chosen_species'],
                        onto,
                        session['input_type'],
                        session['current_analysis_name'],
                        session['alternative'],
                        test_res
                        )
    history = session['history']
    history[uuid.uuid1()] = item
    session['history'] = history

    # make Enrichment results plot
    plot, enrich_table = fct.make_cats_res(session, test_res, is_fisher)

    return json.dumps({'plot': plot, 'enrich_table': enrich_table})


@app.route('/startAnalysisTerms', methods=['POST'])
def startAnalysisTerms():
    """ Run enrichment analysis on individual terms. """
    # get selected category where to zoom in.
    cat = request.json['clickedCat']
    onto = session['onto']

    if len(onto) != 3:
        cat_go_terms = session["cat_go_terms"][session["cat_go_terms"]['Ontology'].isin(
            onto)]
    else:
        cat_go_terms = session["cat_go_terms"]

    genes = session['genes']
    background = session['background']
    session['cat_go_terms_in_onto'] = cat_go_terms
    input_type = session['input_type']
    alternative = session['alternative']

    t1 = time.time()
    # for calculation of D-value
    n_genes_anno = pd.unique(background['Gene_ID']).shape[0]
    print(n_genes_anno)
    # for C -value
    n_genes_in_sample = genes.shape[0]
    # get terms for specified category
    cat_go_terms = cat_go_terms[cat_go_terms['Category'] == cat][[
        'GO_ID', 'Name']]
    go_terms = pd.unique(cat_go_terms['GO_ID'])

    useMultiProcessing = True
    n_worker = 2

    # filter the go_terms
    bgfg = pd.merge(background, genes, how='inner')['GO_ID']
    go_terms = np.array(list(set(go_terms).intersection(set(bgfg.values))))
    # filter the background in order to reduce the huge amount of data
    background = background[background['GO_ID'].isin(go_terms)]  # TO CHANGE
    print(pd.unique(background['Gene_ID']).shape[0])
    if useMultiProcessing:
        if input_type == 'sig':
            test_res = fct.run_fisher_multi_process(
                go_terms, genes, background, n_genes_anno,
                n_genes_in_sample, alternative, n_worker)
        else:
            test_res = fct.run_mwu_multi_process(
                genes, go_terms, background, n_genes_in_sample, n_worker)
    else:
        if input_type == 'sig':
            test_res = fct.run_fisher_single_process(
                go_terms, genes, background, n_genes_anno,
                n_genes_in_sample, alternative)
        else:
            test_res = fct.run_mwu_single_process(
                genes, go_terms, background, n_genes_in_sample)
    print(time.time() - t1, 'Analysis on Terms')

    # get the name of the GO_term into the table
    test_res = pd.merge(cat_go_terms.drop_duplicates(), test_res,
                        how='inner', on='GO_ID')
    # make Enrichment results table
    enrich_table = plt.df_to_table(test_res, 'enrichTable')

    return json.dumps({'enrich_table': enrich_table})


@app.route('/getPrelimGenesTable', methods=['GET'])
def getPrelimGenesTable():
    """Render genes table before analysis"""
    genes = session['genes']
    background = session['direct_goterms']
    genes = pd.merge(genes, background, how='inner')
    genes = genes[['Gene_ID', 'Symbol']].drop_duplicates()
    genes_table = plt.df_to_table(genes, 'pGenesTable')
    return json.dumps({'genes_table': genes_table})


@app.route('/getGenesTable', methods=['POST'])
def getGenesTable():
    """ Render the genes table while user looks at plot. """
    cat_or_term = request.json['where']

    genes = session['genes']
    background = session['complete_backgr']
    cat_go_terms = session['cat_go_terms_in_onto']

    genes_per_x = pd.merge(genes, background, how='inner')
    genes_per_x = pd.merge(genes_per_x, cat_go_terms)[
        ['Gene_ID', 'Symbol', cat_or_term, 'GO_ID']].drop_duplicates()

    # bring duplicates of genes into same row
    genes_per_x = genes_per_x.groupby(['Gene_ID', 'Symbol'], as_index=False).agg(
        lambda x: ', '.join(x.unique())).reindex(columns=genes_per_x.columns)
    genes_table = plt.df_to_table(genes_per_x, 'genesTable', cat_or_term)

    if cat_or_term == 'Category':
        session["last_genes"] = genes_table

    return json.dumps({'genes_table': genes_table})


@app.route('/showOntoTree', methods=['POST'])
def showOntoTree():
    """ Compute the ontology tree. """
    go_id = request.json['goid']
    nodes, edges = preps.build_onto_tree(go_id, session)
    return json.dumps({'nodes': nodes, 'edges': edges})


@app.route('/showNetwork', methods=['POST'])
def showNetwork():
    """ Compute the interaction graph."""
    connectivity = int(request.json['numConnections']) + 2
    active_inter = request.json['activeInter']
    use_go_dist = request.json['goDistances']
    use_approx_go_dist = request.json['goApproxDistances']
    active_ana = {}
    for elem in request.json['activeAnalyses']:
        name, color = elem.split('{color:}')
        active_ana[name] = color
    # unique genes
    genes = request.json['collection']
    genes = pd.DataFrame(genes, columns=['Gene_ID', 'Symbol', 'Analysis'])
    genes = genes[genes['Analysis'].isin(active_ana.keys())]
    # duplicates merge
    genes = genes.groupby(['Gene_ID', 'Symbol'], as_index=False).agg(
        lambda x: ', '.join(x)).reindex(columns=genes.columns)

    species = session['chosen_species']
    try:
        taxid = session['taxids'][species]
    except KeyError:
        return json.dumps({'error': 'Sorry, interactions are not available for species {}.'.format(species)})
    interactions, symbol_geneid = preps.load_interactions(species, taxid)
    # only selected interactions
    interactions = interactions[interactions['inter_type'].isin(
        active_inter)]

    compl_G = preps.build_inter_graph(interactions)

    gene_ids = genes['Gene_ID'].tolist()
    # pylint: disable-msg=E1133
    # pylint complains that Graph is not iterable
    sub_nodes = [x for x in compl_G.vs if x['name'] in gene_ids]
    conn_nodes = preps.shortest_path(compl_G, sub_nodes, connectivity)
    vert = list(set([item for sublist in conn_nodes for item in sublist]))
    if use_go_dist or use_approx_go_dist:
        edge_weights = preps.compute_go_dist(
            vert, conn_nodes, session, use_approx_go_dist)
    else:
        edge_weights = None
    nodes, edges = preps.visualise_graph(
        interactions, symbol_geneid, vert, edge_weights, genes, active_ana, species, taxid)

    return json.dumps({'nodes': nodes, 'edges': edges})


@app.route('/getGenesForTerm', methods=['POST'])
def getGenesForTerm():
    ''' Map genes to the corresponding terms in order to add to collection. '''
    goids = request.json['goids']
    gene_to_term = session['background']
    genes = session['genes']

    gene_to_term = gene_to_term.loc[gene_to_term['GO_ID'].isin(goids)]
    gene_to_term = pd.merge(gene_to_term, genes, on='Gene_ID')

    return json.dumps({'genes': gene_to_term['Gene_ID'].tolist(),
                       'symbols': gene_to_term['Symbol'].tolist()})


@app.route('/getNodeInfo', methods=['POST'])
def getNodeInfo():
    ''' Get the information for node in interaction network to display in side node info. '''
    node = request.json['node']
    gene_to_term = session['direct_goterms']
    gene_to_term = gene_to_term.loc[gene_to_term['Gene_ID'] == node][[
        'GO_ID']]

    gene_to_term = pd.merge(
        gene_to_term, session['cat_go_terms'], how='inner').drop_duplicates('GO_ID')
    go_bp = gene_to_term[gene_to_term['Ontology']
                         == 'BP'][['GO_ID', 'Name']].values.tolist()
    go_cc = gene_to_term[gene_to_term['Ontology']
                         == 'CC'][['GO_ID', 'Name']].values.tolist()
    go_mf = gene_to_term[gene_to_term['Ontology']
                         == 'MF'][['GO_ID', 'Name']].values.tolist()
    mu = render_template(
        'nodeInfo.html',
        symbol=request.json['symbol'],
        species_link=session['species_link']+node,
        node=node,
        analysis_color=request.json['color'],
        analysis_name=request.json['analysis'],
        bp_terms=go_bp,
        cc_terms=go_cc,
        mf_terms=go_mf
    )
    return json.dumps({'nodeInfo': mu})


@app.route('/backToCats', methods=['POST'])
def backToCats():
    """ Returns the last saved category results. """
    return json.dumps({'plot': session['last_plot'],
                       'enrich_table': session['last_res'],
                       'genes_table': session['last_genes'],
                       'analysis_name': session['current_analysis_name']})


@app.route('/reloadHistory', methods=['POST'])
def reloadHistory():
    """ Loads names of history items. """
    history = session['history']
    df = pd.DataFrame(
        columns=["uuid", "Analysis name", "Organism", "Analysis type"])
    a_names = {"sig": "SEA", "cm": "GSEA"}
    for i, key in enumerate(history):
        df.loc[i] = [key, history[key].name,
                     history[key].species,
                     a_names[history[key].input_type]]
    hist_table = plt.df_to_table(df, "historyTable")
    return json.dumps({'history_table': hist_table})


@app.route('/findPairWiseInter', methods=['POST'])
def findPairWiseInter():
    """ Find pairwise interactions. """
    to_compare = [uuid.UUID(x) for x in request.json['toCompare']]

    interactions = preps.computePairWiseInter(to_compare, session)
    # generate table from dataframe
    inter_table = plt.df_to_table(interactions, 'interactionsTable')
    return json.dumps({'interactions': inter_table})


@app.route('/deleteFromHistory', methods=['POST'])
def deleteFromHistory():
    ''' Deletes from history by list of uuids. '''
    history = session['history']
    to_delete = request.json['toDelete']
    for key in to_delete:
        del history[uuid.UUID(key)]
    session['history'] = history
    return '', 204


@app.route('/reloadHeatmap', methods=['POST'])
def reloadHeatmap():
    """ Loads history data and creates heatmap. """
    measure = request.json['inputType']
    clust = request.json['checkedClust']
    use_iso = request.json['iso']
    z_score = request.json['scaling']
    _history = session['history']
    p_cutoff = None
    if use_iso == "iso":
        _history = {k: _history[k] for k in session['history_iso']}
    # filter history for applicable analyses
    history = {}
    if measure == "tf":
        # term frequency only for fisher test
        for _, key in enumerate(_history):
            if _history[key].input_type == "sig":
                history[key] = _history[key]
    elif measure == "dr":
        # delta rank only for mwu
        for _, key in enumerate(_history):
            if _history[key].input_type == "cm":
                history[key] = _history[key]
    else:
        # p value - keep all entries
        history = _history
        p_cutoff = int(request.json['cutoff'])
    if len(history) < 2:
        mu = "<p style='color:#e95420;font-weight:bold;padding:30px 0;'>"
        mu += "Not enough analyses of this type to display heat map.</p>"
        mu = Markup(mu)
        return json.dumps({"plot": mu})
    results = []
    names = []
    for _, key in enumerate(history):
        results.append(history[key].result)
        names.append(history[key].name)
    hm = plt.makeHeatmap(names, results, measure=measure,
                         clustering=clust, z_score=z_score, cutoff=p_cutoff)
    return json.dumps({"plot": hm})


@app.route('/reload3dHeatmap', methods=['POST'])
def reload3dHeatmap():
    """ Loads history data and creates heatmap. """
    measure = request.json['inputType']
    clust = request.json['checkedClust']
    use_iso = request.json['iso']
    z_score = request.json['scaling']
    _history = session['history']
    p_cutoff = int(request.json['cutoff'])
    if use_iso == "iso":
        _history = {k: _history[k] for k in session['history_iso']}
    # filter history for applicable analyses
    history = {}
    if measure == "tf":
        # term frequency only for fisher test
        for _, key in enumerate(_history):
            if _history[key].input_type == "sig":
                history[key] = _history[key]
    else:
        # delta rank only for mwu
        for _, key in enumerate(_history):
            if _history[key].input_type == "cm":
                history[key] = _history[key]

    if len(history) < 2:
        mu = "<p style='color:#e95420;font-weight:bold;padding:30px 0;'>"
        mu += "Not enough analyses of this type to display heat map.</p>"
        mu = Markup(mu)
        return json.dumps({"plot": mu})
    results = []
    names = []
    for _, key in enumerate(history):
        results.append(history[key].result)
        names.append(history[key].name)
    hm = plt.make3dHeatmap(names, results, measure,
                           clustering=clust, z_score=z_score, cutoff=p_cutoff)
    return json.dumps({"plot": hm})


@app.route('/getNamesForHeatmapSorting', methods=['POST'])
def getNamesForHeatmapSorting():
    """ Retrieves names of all relevant heat map entries for sorting by user """
    measure = request.json['inputType']
    use_iso = request.json['iso']
    _history = session['history']
    ids = []
    names = []
    species = []
    if use_iso == "iso":
        _history = {k: _history[k] for k in session['history_iso']}
    # filter history for applicable analyses

    if measure == "tf":
        # term frequency only for fisher test
        for _, key in enumerate(_history):
            if _history[key].input_type == "sig":
                ids.append(key)
                names.append(_history[key].name)
                species.append(_history[key].species[0])
    elif measure == "dr":
        # delta rank only for mwu
        for _, key in enumerate(_history):
            if _history[key].input_type == "cm":
                ids.append(key)
                names.append(_history[key].name)
                species.append(_history[key].species[0])
    else:
        for _, key in enumerate(_history):
            ids.append(key)
            names.append(_history[key].name)
            species.append(_history[key].species[0])

    mu = render_template('heatSortingOverlay.html',
                         n=len(ids),
                         ids=ids,
                         names=names,
                         species=species)
    return json.dumps({"overlay": mu})


@app.route('/sortHeatmap/<iso>', methods=['POST'])
@app.route('/sortHeatmap', methods=['POST'])
def sortHeatmap(iso='all'):
    """ Brings history entries in user specified order """
    selected = [uuid.UUID(x) for x in request.json['sorting']]
    if iso == 'all':
        _history = session['history']
        history = {k: _history[k] for k in selected}
        session['history'] = history
    else:
        session['history_iso'] = selected
    return '', 204


@app.route('/isolateInHeatmap', methods=['POST'])
def isolateInHeatmap():
    """ Isolates selection for heat map. """
    selected = [uuid.UUID(x) for x in request.json['selected']]
    session['history_iso'] = selected
    return '', 204


@app.route('/rerunFromHistory', methods=['POST'])
def rerunFromHistory():
    """ Loads analysis data from history to display the results again. """
    selected = request.json['toRerun']

    history = session['history']
    analysis = history[uuid.UUID(selected)]
    session['genes'] = analysis.foreground
    session['background'] = analysis.background
    # change species if necessary
    if session['chosen_species'] != analysis.species:
        if analysis.species in session['custom_species'].keys():
            session['complete_backgr'], session['direct_goterms'], session['species_link'] = session['custom_species'][analysis.species]
        else:
            session['complete_backgr'], session['direct_goterms'], session['species_link'] = preps.read_species(
                analysis.species)
    session['chosen_species'] = analysis.species
    session['current_analysis_name'] = analysis.name
    session['input_type'] = analysis.input_type
    session['alternative'] = analysis.alternative

    onto = analysis.ontologies
    if len(onto) != 3:
        cat_go_terms = session["cat_go_terms"][session["cat_go_terms"]['Ontology'].isin(
            onto)]
    else:
        cat_go_terms = session["cat_go_terms"]

    test_res, is_fisher = fct.run_ana_cats(session, analysis.foreground,
                                           analysis.background, cat_go_terms, analysis.alternative)
    # make Enrichment results plot
    plot, enrich_table = fct.make_cats_res(session, test_res, is_fisher)

    return json.dumps({'plot': plot, 'enrich_table': enrich_table, 'speciesLink': session['species_link']})


# @app.route('/registerUser', methods=['POST'])
# def registerUser():
#     """ sets up a new user in sql with password and login """
#     u_name = request.json['name']
#     u_mail = request.json['mail']
#     u_pass = request.json['pass']
#     user = None

#     if user_exists(u_name):
#         res = 'error'
#         msg = 'Username already taken.'
#     else:
#         if register_user(u_name, u_pass, u_mail):
#             res = 'success'
#             msg = 'Successfully registered user.'
#             session['user'] = u_name
#             user = u_name
#         else:
#             res = 'error'
#             msg = 'An error occurred.'

#     return json.dumps({'result': res, 'message': msg, 'user': user})


# @app.route('/loginUser', methods=['POST'])
# def loginUser():
#     u_name = request.json['name']
#     u_pass = request.json['pass']
#     user = None
#     if login_user(u_name, u_pass):
#         res = 'success'
#         msg = 'Successfully logged in.'
#         session['user'] = u_name
#         user = u_name
#     else:
#         res = 'error'
#         msg = 'Username or password incorrect.'
#     return json.dumps({'result': res, 'message': msg, 'user': user})


# @app.route('/logoutUser', methods=['POST'])
# def logoutUser():
#     session['user'] = None
#     return '', 204


# @app.route('/saveSession', methods=['POST'])
# def saveSession():
#     if session['user'] is not None:
#         json_data = sess.sessionToJSON(session)
#         save_session(session['user'], json_data)
#     return '', 204
