{% extends 'layout-static.html' %} {% block content %}

<div class="documentation header" id="docuHeader">
  <a href="{{ url_for('home') }}">
    <img src="/static/img/brand-logo@2x.png" alt="WEADE - Workflow for Enrichment Analysis and Data Exploration">
  </a>
  <h1><a href="{{ url_for('documentation') }}">Documentation</a></h1>
  <div class="nav-container">
    <ul style="overflow: hidden; list-style: none;">
      <li><a href="{{ url_for('documentation') }}#SEA">SEA</a></li>
      <li><a href="{{ url_for('documentation') }}#GSEA">GSEA</a></li>
      <li><a href="{{ url_for('documentation') }}#SideTabs">Side Tabs</a></li>
      <li><a href="{{ url_for('documentation') }}#MainTabs">Main Tabs</a></li>
    </ul>
  </div>
</div>
<div class="documentation" style="margin-top: 250px;">
  <h2 id="Start">Getting Started</h2>
  <p>
    If this is your first time using this app, consider taking the guided interactive tour by clicking the orange button in the
    top right. If you need a hint about what a specific setting or button does, click the question mark in the top right
    corner, this will display content-aware hints.
  </p>
  <p>
    If that does not answer all of your questions, you can find a complete documentation below.
  </p>
  <p>You can address any further questions about WEADE to Nils Trost, nils.trost@stud.uni-heidelberg.de</p>
  <h2>FAQ</h2>
  <h4>How is my uploaded data stored? Can anyone access it?</h4>
  <p>
    Your data is only stored during the session, it is not accessible for anyone else but you for the analyses. Please refer
    to our
    <a href="{{ url_for('termsOfUse') }}">Terms of Use</a> for more details.
  </p>
  <h4>The app is taking a long time loading an analysis. Will it ever finish?</h4>
  <p>
    If the application shows a loading screen for more than roughly 5 minutes, something most certainly went wrong. Try reloading
    the page and running the analysis again. If the issue keeps happening, contact Nils Trost, nils.trost@stud.uni-heidelberg.de.
  </p>
  <h2 id="Analysis">The Analysis</h2>
  <h3 id="SEA">Singular Enrichment Analysis (SEA)</h3>
  <p>
    The tool uses the Fisher's exact test to calculate the enrichment of a category or a term from a list of genes. The resulting
    p-Values are adjusted for multiple hypothesis testing with the Benjamini-Hochberg method when testing on categories.
    Additionally, the term-frequency or odds ratio is calculated.
  </p>
  <h3 id="GSEA">Gene Set Enrichment Analysis (GSEA)</h3>
  <p>
    The tool performs a Mann-Whitney U-test on a set of genes with an associated continuous measure. The resulting p-Values are
    adjusted for multiple hypothesis testing with the Benjamini-Hochberg method when testing on categories. This way, no
    threshold has to be set, all genes are considered and weighted according to the provided measure. Additionally, the delta-rank
    of each category or term is calculated.
  </p>
  <h2 id="UI">The User Interface</h2>
  <h3 id="SideTabs">The Side Tabs</h3>
  <p>The side tabs contain the options that you can set to perform the analyses.</p>
  <h4>Categories</h4>
  <img src="/static/img/docuScreens/Side-cat1.png" alt="Category tab general options.">
  <p>In the
    <strong>Categories</strong> tab, you can select the predefined category sets for the analysis. You may click on the
    <strong>Advanced options</strong> or the
    <strong>Relationship types</strong> button to reveal the following options:</p>
  <img src="/static/img/docuScreens/Side-cat2.png" alt="Category tab advanced options">
  <p>
    <ol>
      <li>
        Here you can upload your own set of categories. The file should be in the following format (no header):
        <br>
        <code>
          GO id&nbsp;&nbsp;&nbsp;&nbsp;GO term&nbsp;&nbsp;&nbsp;&nbsp;Name for category
        </code>
        <br> It is possible to directly load a basket export from QuickGO as a category set.
      </li>
      <li>
        Here you can select the GO edge types that should be included when creating the graph. The default is
        <strong>is a</strong> and
        <strong>part of</strong>.
      </li>
    </ol>
  </p>
  <p>
    <strong>Changing the set of categories will reset the heat map and history, as it makes the analyses incomparable.</strong>
  </p>
  <h4>Files</h4>
  <img src="/static/img/docuScreens/Side-files1.png" alt="Files tab general options">
  <p>
    <ol>
      <li>
        Already supported species are displayed in a drop down menu. The identifier that is used in the annotation of genes to GO
        terms is found in parenthesis after the species name.
      </li>
      <li>
        Here you can select the type of your input files, this will determine the type of test that is run on your data.
      </li>
      <li>
        Here, you can upload your files. You can upload up to four files at a time when performing an SEA, or one at a time when
        performing a GSEA. The following format is used for candidate gene lists (SEA), one gene per row:
        <br>
        <code>...<br>
          Gene1<br>
          Gene2<br>
          Gene3<br>...
        </code>
        <br> The following format is used for gene sets with an associated continuous measure (GSEA), two comma separated columns:
        <br>
        <code>...<br>
          Gene1,-0.2<br>
          Gene2,2.3<br>
          Gene3,1.4<br>...
        </code>
        <br>The files should not include any header.
      </li>
      <li>To test the tool, you can also use example data by clicking this button.</li>
      <li>
        It is generally safer to use the gene identifier that is indicated in the species selection. However, it is also possible
        to use the gene symbol (called external_gene_name in biomart and ensembl) by clicking this button. Conversion might
        be ambiguous, be sure to check the
        <strong>Where are my genes?</strong> button in the
        <strong>Genes</strong> tab.
      </li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Side-files2.png" alt="Files tab advanced options">
  <p>
    Clicking the
    <strong>Advanced options</strong> button below the species selection shows the upload area for custom annotation files. With
    this, you can use an alternate identifier for species that are present or run the analysis on other species. The file
    format for this is as follows:
    <br>
    <code>...<br>
      FBgn0041711&nbsp;&nbsp;&nbsp;&nbsp;GO:0005576 <br>
      FBgn0041711&nbsp;&nbsp;&nbsp;&nbsp;GO:0048067 <br>
      FBgn0041711&nbsp;&nbsp;&nbsp;&nbsp;GO:0016853 <br>
      FBgn0041711&nbsp;&nbsp;&nbsp;&nbsp;GO:0042438 <br>
      FBgn0042110&nbsp;&nbsp;&nbsp;&nbsp;GO:0003674 <br>
      FBgn0042110&nbsp;&nbsp;&nbsp;&nbsp;GO:0003824 <br>
      FBgn0042110&nbsp;&nbsp;&nbsp;&nbsp;GO:0016740 <br>
      FBgn0042110&nbsp;&nbsp;&nbsp;&nbsp;GO:0016772 <br>
      FBgn0037149&nbsp;&nbsp;&nbsp;&nbsp;GO:0005575 <br>
      FBgn0037149&nbsp;&nbsp;&nbsp;&nbsp;GO:0003674 <br>
      FBgn0037149&nbsp;&nbsp;&nbsp;&nbsp;GO:0008150 <br>
      FBgn0039007&nbsp;&nbsp;&nbsp;&nbsp;GO:0007165 <br>
      FBgn0039007&nbsp;&nbsp;&nbsp;&nbsp;GO:0007154 <br>
      FBgn0039007&nbsp;&nbsp;&nbsp;&nbsp;GO:0007186 <br>
      FBgn0039007&nbsp;&nbsp;&nbsp;&nbsp;GO:0008150 <br>...
    </code>
    <br> The file should be a tab separated text file with two columns. The first column should contain the gene identifiers
    and the second column the corresponding GO term. If a gene identifier is associated with more than one GO term, put each
    association in a separate line.
  </p>
  <p>
    You may also add a third column containing gene symbols for display instead of the gene identifiers.
  </p>
  <img src="/static/img/docuScreens/Side-files3.png" alt="Files tab set names">
  <p>
    After uploading input files, you can enter names for the sets. These names will be displayed in the Venn diagram. This optional
    but helpful, if the file names are not clear.
  </p>
  <h4>Run</h4>
  <img src="/static/img/docuScreens/Side-run1.png" alt="Run tab">
  <p>
    <ol>
      <li>
        Here you can choose which of the three ontologies (
        <strong>Biological Processes</strong>,
        <strong>Cellular Components</strong> and
        <strong>Molecular Functions</strong>) should be included in the enrichment analysis.
      </li>
      <li>
        This option is only available when performing an SEA. This allows you to test for over-representation of term or categories
        (
        <strong>enrichment</strong>) or under-representation (
        <strong>depletion</strong>).
      </li>
      <li>
        This button starts the analysis. It is only available after you have uploaded files in the
        <strong>Files</strong> tab and applied the sets for analysis in the
        <strong>Venn</strong> tab.
      </li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Side-run2.png" alt="Run tab after analysis">
  <p>
    <ol>
      <li>This button appears after you have performed an analysis on a specific category. Clicking it takes you back to the
        results of all categories.</li>
      <li>This field shows the name of the last analysis. The results in the main tabs refer to this analysis.</li>
    </ol>
  </p>

  <h3 id="MainTabs">The Main Tabs</h3>
  <p>
    The main tabs display the set selection Venn diagram as well as the results of the analyses.
  </p>
  <h4>Set selection</h4>
  <img src="/static/img/docuScreens/Main-venn.png" alt="Set selection tab">
  <p>
    <ol>
      <li>
        After uploading data sets, a Venn diagram will be generated displaying the overlaps of the genes in the different data sets.
        You can then select which subset of genes should form the foreground and the background by selecting segments in
        the Venn diagram. When no background is selected, WEADE tests against the entire genome (all annotated genes with
        GO terms). This is different from selecting all segments, as this includes only the uploaded genes. Selecting no
        foreground will perform the analysis with all uploaded genes. This is the same as selecting all segments.
      </li>
      <li>
        Here you need to enter a name for the analysis. This name will be shown in the
        <strong>Current analysis</strong> field, in the
        <strong>history</strong>, the
        <strong>collection</strong> and in the
        <strong>heat map</strong>.
      </li>
      <li>After having selected a fore- and background and chosen a name for the analysis, you can apply the sets with this button.
        This enables the start analysis button.</li>
    </ol>
  </p>
  <h4>Plot</h4>
  <img src="/static/img/docuScreens/Main-plot.png" alt="Plot tab">
  <p>
    The plot tab contains a plot of the enrichment results. If a list of significant genes was used for the analysis (SEA), the
    length of the bars is the odds ratio (term-frequency). If the analysis was performed on a list of genes with a continuos
    measure (GSEA), the length of the bars corresponds to the delta rank of the MWU test. The color of the bars represents
    the p-Value.
  </p>
  <h4>Enrichment Results</h4>
  <img src="/static/img/docuScreens/Main-res1.png" alt="Enrichment results tab overview">
  <p>
    The enrichment results tab shows the results in a tabular format. The first column contains the names of the categories,
    the second contains the p-values of the enrichment of those categories. The last three columns contain the numbers of
    genes in the sample, in the sample and in the category, and in the background and in the category, respectively, for
    SEA and the delta-rank for GSEA. The table can be sorted and filtered with the search and the results can be downloaded
    in several formats. Clicking on one of the categories shows the following options:
  </p>
  <img src="/static/img/docuScreens/Main-res2.png" alt="Enrichment results tab highlighted category">
  <p>
    <ol>
      <li>
        This button allows you to perform the analysis on the GO terms that are annotated to the selected category. This shows their
        contribution to the enrichment result.
      </li>
      <li>
        This button takes you to the genes tab but filters the genes to include only the genes annotated to this category.
      </li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Main-res3.png" alt="Enrichment results tab terms">
  <p>
    When you click on the
    <strong>Run analysis on this category</strong> button of a selected category, the enrichment results table will change to now
    include the terms that contribute to the enrichment of the category. You can use the
    <strong>Back to categories</strong> button in the
    <strong>Run</strong> tab to go back to the previous view of all categories. Selecting a term in this table reveals the following
    options:
  </p>
  <img src="/static/img/docuScreens/Main-res4.png" alt="Enrichment results tab highlighted term">
  <p>
    <ol>
      <li>
        This button moves all genes annotated to the term of your sample to the collection. You will be prompted to name your collection.
        This name will be prepended with the name of the current analysis as shown in the
        <strong>Current analysis</strong> field in the
        <strong>Run</strong> tab.
      </li>
      <li>
        This button takes you to the genes tab but filters the genes to include only the genes annotated to this term.
      </li>
      <li>
        This button opens an overlay that shows the selected term in its context of the Gene Ontology tree.
      </li>
      <li>
        This button takes you QuickGO, where you can find more information about the selected GO term.
      </li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Main-onto.png" alt="Term in ontology context">
  <p>
    <ol>
      <li>Click this button to close the overlay.</li>
      <li>The different edge types of the Gene ontology and their colours in the graph.</li>
      <li>This graph shows the selected term on the bottom and its relation to the root node of the ontology at the top.</li>
    </ol>
  </p>
  <h4>Genes</h4>
  <img src="/static/img/docuScreens/Main-genes1.png" alt="Genes tab overview">
  <p>
    The gene tab contains a table with all genes and the category or term to which they were annotated. This table can also be
    sorted and filtered and its content be downloaded in several formats. Selecting one gene in this table shows the following
    information:
  </p>
  <img src="/static/img/docuScreens/Main-genes2.png" alt="Genes tab highlighted gene">
  <p>
    <ol>
      <li>This button takes you to a corresponding species data base with information on the gene.</li>
      <li>
        Here, a list of all categories or terms to which the gene is annotated is displayed. This includes direct annotations as
        well as those inferred through the hierarchical structure of the Gene Ontology.
      </li>
      <li>
        This button adds the selected gene to the collection. You will be prompted to name your collection. This name will be prepended
        with the name of the current analysis as shown in the
        <strong>Current analysis</strong> field in the
        <strong>Run</strong> tab.
      </li>
    </ol>
    Selecting multiple genes (with shift or control/command keys) from the table allows you to add them to the collection together,
    with the button shown in the following image:
  </p>
  <img src="/static/img/docuScreens/Main-genes3.png" alt="Genes tab highlighted genes">
  <h4>Heat Map</h4>
  <img src="/static/img/docuScreens/Main-heat1.png" alt="Heat map 2D">
  <p>
    This tab shows a heat map of previous analyses, if at least two of the same type (SEA/GSEA) have been performed. The options
    for the heat map can be found above.
    <ol>
      <li>Here you can choose between a 3D or a 2D representation of the heat map. Changing this also changes the available options
        slightly.
      </li>
      <li>
        This option controls, if the color of the heat map is used to represent the p-values (only available in 2D), the term frequency
        (for SEA) or the delta-rank (for GSEA). Results from GSEA and SEA can be displayed in the same heat map, if p-value
        is chosen.
      </li>
      <li>
        This option is only available when selecting p-value. Since the p-value can range across several orders of magnitude, it
        can be hard to see differences in either the high p-value ranges or the low. This cut-off allows you to adjust the
        distribution of colours across the p-value range to best fit your data.
      </li>
      <li>
        Here you can select between showing all analyses or those that you isolated in the
        <strong>History</strong> tab.
      </li>
      <li>
        By default, the columns and rows are clustered to minimize differences between neighbouring values. You can turn of the clustering
        for columns, rows, or both. When turning of clustering of columns, you can choose a custom order of the analyses
        by clicking the
        <strong>Reorder analyses</strong> button and rearranging them to your liking.
      </li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Main-heat2.png" alt="Heat map 3D">
  <p>
    This image shows the 3D heat map. You can move the view around by clicking on it and dragging. In the 3D heat map, the colour
    always corresponds to the p-value. You can adjust the colour cut-off in the same way as in the 2D heat map. The height
    of the bars represent either the term-frequency (SEA) or the delta-rank (GSEA). You can display either the absolute values
    of the term-frequency and delta-rank or use scaling with the z-score. This scaling can be done across columns or across
    rows.
  </p>
  <h4>History</h4>
  <img src="/static/img/docuScreens/Main-hist1.png" alt="History tab overview">
  <p>
    The history tab contains a table with all previously performed analyses. It shows the name of each analysis, the organism
    that was used and the type of analysis (SEA/GSEA). Clicking on one of the analyses gives you access to the following
    options:
  </p>
  <img src="/static/img/docuScreens/Main-hist2.png" alt="History tab highlighted one">
  <p>
    <ol>
      <li>This button allows you to return to the results of the selected analysis.</li>
      <li>Clicking this button removes the selected analysis permanently from the history and thereby from the heat map.</li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Main-hist3.png" alt="History tab highlighted multiple">
  <p>
    <ol>
      <li>When you selected two analyses, you can test for potential pairwise interactions between their genes.</li>
      <li>When you selected two or more analyses, you can isolate them in the heat map to display them separately from the other
        analyses.
      </li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Main-pair.png" alt="Pairwise interactions">
  <p>
    Clicking on the
    <strong>Find pairwise interactions</strong> button after selecting two analyses in the history, this overlay is shown. BioGRID
    interaction data is used to find interactions between genes or gene products where one of the interaction partners is
    present in the first analysis and the other in the second analysis. WEADE filters these results for genes that include
    either the GO term "ligand" or "receptor".
    <br>Using Reactome.org data, WEADE tries to determine to which pathway this interaction may belong to. The genes of the
    two data sets are then tested for enrichment in the pathways. Significantly enriched pathways are reported next to the
    interaction.
    <br>To exit from the overlay, click the close button.
  </p>
  <h4>Collection</h4>
  <img src="/static/img/docuScreens/Main-coll.png" alt="Collection tab">
  <p>
    The collection shows a table of genes that have been added to it from the
    <strong>Genes</strong> or the
    <strong>Enrichment results</strong> tab. The gene identifier and the gene symbol are shown for each gene, as well as from where
    the gene has been added to the collection.
    <ol>
      <li>Selecting genes from the table and clicking this button removes them from the collection.</li>
      <li>This button removes all genes from the collection.</li>
      <li>This button opens an overlay with an interaction network of the genes in the collection.</li>
    </ol>
  </p>
  <img src="/static/img/docuScreens/Main-net.png" alt="Interaction network">
  <p>
    The interaction network uses BioGRID data to find interactions between genes and gene products of the selection.
    <ol>
      <li>Here you can choose which types of interactions will be used for creating the graph.</li>
      <li>
        Adding additional connecting nodes can give you a more comprehensive view of the interaction network, including genes that
        are not in your collection.
      </li>
      <li>
        You can search for a specific gene in the network.
      </li>
      <li>
        WEADE can use the semantic similarity of the terms annotated to the genes to cluster the interaction network. Genes which
        share many GO terms will be connected with shorter edges, Genes which share few or no GO terms get longer edges.
        Calculating the distances for large networks can take long, you might want to use approximations of the distances
        in this case.
      </li>
      <li>For larger networks, the interactive and dynamic network representation may be slow. Using a fixed layout can help.</li>
      <li>Here you can choose, genes from which collection will be used to build the graph.</li>
      <li>This button closes the overlay.</li>
      <li>The network view where nodes represent genes or their products and edges their interactions.</li>
      <li>Clicking on a node in the network will add an info box to this panel. Here you find information on the gene and the
        GO terms that are annotated to it.</li>
    </ol>
  </p>
  <h4>Category Set</h4>
  <img src="/static/img/docuScreens/Main-cat.png" alt="Category set tab">
  <p>
    In this table, the GO terms that contribute to each category are listed. From here you can add terms to any of the categories,
    add new categories, and remove terms. You can also show the terms in their context in the Gene Ontology graph. After
    editing the category set, you can click the
    <strong>Apply all changes</strong> button. This creates a new category set in the drop-down menu in the
    <strong>Categories</strong> side tab for the duration of the session. It will automatically be selected for your next analyses.
  </p>
</div>

{% endblock %}