from weade import db


class Species(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    g_id = db.Column(db.String(100), index=True)
    identifier = db.Column(db.String(100), index=True)
    dataset = db.Column(db.String(200))
    link = db.Column(db.String(200))
    taxid = db.Column(db.Integer, index=True)
    p_id = db.Column(db.String(100), index=True)

class Catset(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True)
    owner = db.Column(db.Integer, default=1)
    terms = db.relationship('Catterm', backref='catset', lazy='dynamic')  # type: list[Catterm]


class Catterm(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    goid = db.Column(db.String(120), index=True)
    goterm = db.Column(db.String(240))
    cat = db.Column(db.String(120), index=True)
    catset_id = db.Column(db.Integer, db.ForeignKey('catset.id'))


class Interaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    a = db.Column(db.Integer, index=True)
    b = db.Column(db.Integer, index=True)
    itype = db.Column(db.String(240), index=True)
    taxid_a = db.Column(db.Integer, index=True)
    taxid_b = db.Column(db.Integer, index=True)
    source = db.Column(db.String(120))
    source_id = db.Column(db.String(120))
    pubmed_id = db.Column(db.String(120))


class Pathway(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    gene = db.Column(db.String(120), index=True)
    Rid = db.Column(db.String(120))
    link = db.Column(db.String(240))
    name = db.Column(db.String(240))
    evidence = db.Column(db.String(20))
    species = db.Column(db.String(120), index=True)
