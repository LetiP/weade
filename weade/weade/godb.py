"""
Most of the code taken from the godb package that is always cited
with https://github.com/endrebak/go_db, which in present does no
more exist.
Adjusted in need for parsing more regulation types from the database,
Python 3 and latest pandas version (0.20.0) compatibility.
"""

import logging

from os.path import expanduser, join as path_join, isfile
from subprocess import call

import pandas as pd
from joblib import Memory


CACHE_PATH = expanduser("~/.python_cache")
call("mkdir -p {}".format(CACHE_PATH), shell=True)
OBO_PATH = path_join(CACHE_PATH, "go.obo")
MEMORY = Memory(cachedir=CACHE_PATH)

try:
    from urllib import urlretrieve
except ImportError:
    from urllib.request import urlretrieve


def _download_obo_if_not_exists():
    if not isfile(OBO_PATH):
        logging.info("Downloading Gene Ontology file to " + OBO_PATH)
        urlretrieve("http://geneontology.org/ontology/go.obo",
                    OBO_PATH)
        logging.info("Download finished")

    return OBO_PATH


@MEMORY.cache(verbose=0)
def parse_obo_file(obo_file=""):
    """Parse obo file into dataframe, where each record takes one row.

    The cols "is_a", and "synonym" are untidy; each may contain more than one
    value per row (each entry is separated by a tab)."""

    reg_types = ["is_a", "part_of", "has_part", "regulates",
                 "positively_regulates", "negatively_regulates",
                 "occurs_in"]

    if not obo_file:
        obo_file = _download_obo_if_not_exists()

    obo_records = "".join(open(obo_file).readlines()).split("\n\n[Term]")[1:-1]

    record_dicts = []
    for record in obo_records:
        record_dict = _parse_record(record, reg_types)
        record_dicts.append(record_dict)

    df = pd.DataFrame.from_dict(record_dicts)
    df = _drop_obsolete_records(df)

    df = _clean_dataframe(df).reset_index(drop=True)

    df = df[["id", "namespace", "name", "synonym",
             "def"] + reg_types]
    df.columns = ["GO_ID", "Ontology", "Name", "Synonym",
                  "Definition"] + reg_types
    print(df.shape[0])
    return df


def _clean_dataframe(df):

    # Turn names like biological_process into BP.

    df["namespace"] = df["namespace"].apply(
        lambda s: {"biological_process": "BP",
                   "molecular_function": "MF",
                   "cellular_component": "CC"}[s])

    # Turn definitions like:
    #
    # The production of new individuals that contain some portion of
    # genetic material inherited from one or more parent organisms.
    # [GOC:go_curators, GOC:isa_complete, GOC:jl, ISBN:0198506732]
    #
    # into
    #
    # The production of new individuals that contain some portion of
    # genetic material inherited from one or more parent organisms.

    df["def"] = df["def"].apply(lambda s: s.split('" [')[0])
    df["def"] = df["def"].str.replace('"', '')

    df["synonym"] = df["synonym"].str.replace('"', '')

    return df


def _drop_obsolete_records(df):

    obsolete_rows = df.is_obsolete.dropna().index
    df = df.drop(obsolete_rows)
    df.pop("is_obsolete")

    return df


def _parse_record(record, reg_types, data_to_keep=("id", "name", "namespace", "is_a",
                                                   "def", "synonym", "is_obsolete", "relationship")):

    record_lines = record.split("\n")

    synonyms, is_a_list = [], []
    record_dict, helper = {}, {}

    for line in record_lines:

        if ":" in line:

            key, value = line.split(":", 1)

            if key not in data_to_keep:
                continue

            key, value = key.strip(), value.strip()

            if key == "is_a":
                is_a_list.append(value.split(" !")[0])
            elif key == "synonym":
                synonyms.append(value.split('" ')[0])
            elif key == "relationship":
                relationship_key, relationship_value = value.split(" ", 1)

                for reg_type in reg_types:
                    if relationship_key == reg_type:
                        if reg_type in record_dict.keys():
                            helper[reg_type].append(
                                relationship_value.split(" !")[0])
                        else:
                            helper[reg_type] = [
                                relationship_value.split(" !")[0]]
            else:
                record_dict[key] = value

        record_dict["is_a"] = "\t".join(is_a_list)
        record_dict["synonym"] = ";".join(synonyms)

        for key in set(helper.keys()).difference(set(["synonym"])):
            record_dict[key] = "\t".join(helper[key])

    return record_dict


def _get_all_parent_child_relations(df, ontology=None, relations=["is_a", "part_of", "has_part"]):
    """Create map of children and parent nodes."""

    if ontology is not None:
        children_df = df.loc[df.Ontology == ontology]
    else:
        children_df = df

    relation_dfs = []
    for relation in relations:

        splitted = children_df[relation].str.split('\t', expand=True)

        stacked = pd.concat([children_df['GO_ID'], splitted],
                            axis=1).set_index('GO_ID').stack()

        relation_df = stacked.reset_index().rename(columns={0: relation}).\
            drop('level_1', axis=1)

        empty_col_index = relation_df[relation_df[relation] == ""].index
        relation_df = relation_df.drop(empty_col_index)

        if relation == "part_of":
            # relation = "has_part*"
            relation_df.columns = ["GO_ID", relation]
        elif relation not in ["is_a"]:
            relation_df = relation_df[[relation, "GO_ID"]]
            relation_df.columns = ["GO_ID", relation]

        relation_df.columns = ["Child", "Parent"]

        relation_df["Relation"] = relation

        relation_dfs.append(relation_df)

    relation_df = pd.concat(relation_dfs)

    return relation_df


def _offspring(children_df):
    """Create long-format df mapping parent and offspring."""

    offspring_df = children_df.copy()[["Child", "Parent"]]
    offspring_df.columns = ["Offspring", "Parent"]

    offspring_df = get_offspring_map(offspring_df)

    return offspring_df


@MEMORY.cache(verbose=0)
def get_offspring_map(df):
    """Turn a map of parent to children into map of parent to offspring.

    I.e. it computes the grandchildren, grand-grandchildren and so on and
    returns a long format dataframe of these relations."""

    logging.info("Computing offspring of node.")
    new_df = _compute_transitive_closure(df)

    while not df.equals(new_df):
        df = df.append(new_df).drop_duplicates()
        new_df = _compute_transitive_closure(df)

        df = df.drop_duplicates().sort_index()
        new_df = new_df.drop_duplicates().sort_index()

    return df


def _compute_transitive_closure(df):
    """Computes the transitive closure from a two-col parent/child map."""

    df_temp = df[df["Parent"].isin(df["Offspring"])]
    df2 = df.merge(df_temp, left_on="Offspring", right_on="Parent",
                   suffixes=["_1_gen", "_2_gen"])
    df2 = df2.drop(["Offspring_1_gen", "Parent_2_gen"], axis=1)

    df2.columns = ["Parent", "Offspring"]
    concat_df = pd.concat([df, df2]).drop_duplicates()

    return concat_df


@MEMORY.cache(verbose=0)
def get_children(ontology=None, relations=["is_a", "part_of", "has_part"]):

    df = PARSED_OBO[relations + ["GO_ID", "Ontology"]]

    return _get_all_parent_child_relations(df, ontology, relations).reset_index(drop=True)


@MEMORY.cache(verbose=0)
def get_offspring(ontology=None, relations=["is_a", "part_of", "has_part"]):

    children = get_children(ontology, relations)

    return _offspring(children).reset_index(drop=True)


@MEMORY.cache(verbose=0)
def get_annotations():

    GOTERM = PARSED_OBO[["GO_ID", "Ontology", "Name",
                         "Synonym", "Definition"]]

    return GOTERM


def get_ontology(goid):
    goterm = PARSED_OBO[["GO_ID", "Ontology"]]

    return goterm.loc[goterm['GO_ID'] == goid].Ontology.iloc[0]


PARSED_OBO = parse_obo_file("")
