import mysql.connector


class MysqlData:
    """ Convenience function for accessing MySQL databases. """

    def __init__(self):
        import mysql.connector
        self.cnx = mysql.connector.connect(user="App",
                                           password="goCatApp717",
                                           host="127.0.0.1",
                                           database="db_goapp")
        self.cursor = self.cnx.cursor()

    def runQuery(self, query, n=False, commit=False, close=True):
        self.cursor.execute(query)
        if not n:
            self.data = self.cursor.fetchall()
        else:
            self.data = self.cursor.fetchone()
        if commit:
            self.cnx.commit()
        if close:
            self.cursor.close()
            self.cnx.close()

    def runQuery_udata(self, query, udata, n=False, commit=False, close=True):
        """
        Includes sanitization of user input
        Query should be in format: "SELECT * FROM tbl WHERE name = %s"
        udata is a tuple of the same length as occurrences of %s.
        """
        self.cursor.execute(query, udata)
        if not n:
            self.data = self.cursor.fetchall()
        else:
            self.data = self.cursor.fetchone()
        if commit:
            self.cnx.commit()
        if close:
            self.cursor.close()
            self.cnx.close()

    def runProc(self, procedure, arglist):
        try:
            self.cursor.callproc(procedure, arglist)
            self.data = self.cursor.fetchone()
            if self.data is None:
                self.cnx.commit()
                self.cursor.close()
                self.cnx.close()
                return 0
            else:
                print(self.data)
                return 1
        except Exception as e:
            return e

    def updateTable(self, query, commit=False):
        self.cursor.execute(query)
        if commit:
            self.cnx.commit()
