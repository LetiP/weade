# script for development purposes.
# clear flask_session folder content and run server.

rm flask_session/*

# concatenate all js to one large file
# order is important
# cd weade/static/js/
# cat sweetalert.min.js jquery.ui.widget.js jquery-ui.min.js jquery.iframe-transport.js jquery.fileupload.js file-upload.js custom-backgr-cat.js venn.js plotly-extension.js selectize.js bootstrap.min.js jquery.dataTables.min.js dataTables.bootstrap4.min.js dataTables.scroller.min.js dataTables.select.min.js dataTables.buttons.min.js buttons.flash.min.js jszip.min.js pdfmake.min.js vfs_fonts.js buttons.html5.min.js buttons.print.min.js dataTables.altEditor.free.js network.js ontotree.js popovers.js editTable.js 3dHeatmap.js functions.js plotly-latest.min.js vis.min.js > ./toConcat/all.js
# cd ../../..

python runserver.py