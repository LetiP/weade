"""
This script runs the WEADE application using a development server.
"""

from os import environ
from weade import app

if __name__ == '__main__':
    # HOST = environ.get('SERVER_HOST', 'localhost') # only accept connections from same computer
    HOST = '127.0.0.1' # hosting in local network
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555

    # app.secret_key = "This secret key will be in wsgi on production"

    app.run(HOST, PORT)
