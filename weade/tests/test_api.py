import os
import tempfile
import pytest

from flask import json

from weade import weade


@pytest.fixture
def client():
    weade.app.config['TESTING'] = True
    client = weade.app.test_client()

    yield client


def test_categorySets(client):
    """ test retrieval of available category sets. """
    rv = client.get('/api/v1.0/categorySets')
    assert b'General' in rv.data


def test_category(client):
    """ test retrieval of selected category set. """
    rv = client.get('/api/v1.0/categorySets/General')
    assert b'Stem Cell' in rv.data


def test_species(client):
    """ test retrieval of species list. """
    rv = client.get('/api/v1.0/species')
    assert b'Drosophila' in rv.data


def test_fisher(client):
    """ test SEA. """
    data = []
    with open('weade/ga/dmel_gro_FBgn.txt', 'r') as infile:
        for line in infile:
            data.append(line.rstrip('\n'))
    rv = client.post('api/v1.0/run/fisher', data=json.dumps({
        'categorySet': 'General',
        'species': 'Drosophila melanogaster (FBgn)',
        'ontologies': ['BP', 'CC', 'MF'],
        'alternative': 'greater',
        'dataset': data
    }), content_type='application/json')
    print(rv.data)
    assert b'Growth' in rv.data


def test_mwu(client):
    """ test GSEA. """
    data = []
    with open('weade/ga/mwu_sample.csv', 'r') as infile:
        for line in infile:
            data.append(line.rstrip('\n').split(','))
    rv = client.post('api/v1.0/run/mwu', data=json.dumps({
        'categorySet': 'General',
        'species': 'Drosophila melanogaster (FBgn)',
        'ontologies': ['BP', 'CC', 'MF'],
        'dataset': data
    }), content_type='application/json')
    print(rv.data)
    assert b'Growth' in rv.data


def test_fisher_terms(client):
    """ test SEA on Terms. """
    data = []
    with open('weade/ga/dmel_gro_FBgn.txt', 'r') as infile:
        for line in infile:
            data.append(line.rstrip('\n'))
    rv = client.post('api/v1.0/run/fisher/terms', data=json.dumps({
        'categorySet': 'General',
        'category': 'Growth',
        'species': 'Drosophila melanogaster (FBgn)',
        'ontologies': ['BP', 'CC', 'MF'],
        'alternative': 'greater',
        'dataset': data
    }), content_type='application/json')
    print(rv.data)
    assert b'p-Value' in rv.data


def test_mwu_terms(client):
    """ test GSEA on Terms. """
    data = []
    with open('weade/ga/mwu_sample.csv', 'r') as infile:
        for line in infile:
            data.append(line.rstrip('\n').split(','))
    rv = client.post('api/v1.0/run/mwu/terms', data=json.dumps({
        'categorySet': 'General',
        'category': 'Growth',
        'species': 'Drosophila melanogaster (FBgn)',
        'ontologies': ['BP', 'CC', 'MF'],
        'alternative': 'greater',
        'dataset': data
    }), content_type='application/json')
    print(rv.data)
    assert b'p-Value' in rv.data
