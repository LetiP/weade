import os
import sys
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'this-is-a-SUPER-secret-key'
    SESSION_TYPE = os.environ.get('SESSION_TYPE') or 'filesystem'
    SESSION_FILE_DIR = os.environ.get('SESSION_FILE_DIR') or os.path.join(basedir, 'flask_session/')
    SESSION_PERMANENT = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'sqlite:///{}'.format(os.path.join(basedir, 'goapp_lite.db'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    BASEDIR = basedir
    REMEMBER_COOKIE_SECURE = True
    SESSION_COOKIE_SECURE = True