# README

## Categorized GO Enrichment App
Performs GO Enrichment on predefined categories with biological meaning.

## Deployment

### Using docker-compose:

Clone the repository, then build and start the containers using `docker-compose up -d --build`

The web app is then listening on port 8000. Edit the nginx ports configuration in the `docker-compose.yml` to change the port.

## Team 

* Developed by Letitia Parcalabescu, Nils Trost & Eugen Rempel